[TOC]

## active_scooter_location

| column_name          | data_type                   | related_keys   | remarks   |
|:---------------------|:----------------------------|:---------------|:----------|
| id                   | bigint                      |                |           |
| imei                 | character varying (96.0)    |                |           |
| trip_id              | bigint                      |                |           |
| user_id              | bigint                      |                |           |
| is_no_riding         | smallint                    |                |           |
| is_no_parking        | smallint                    |                |           |
| is_outside_service   | smallint                    |                |           |
| last_no_riding       | bigint                      |                |           |
| last_no_parking      | bigint                      |                |           |
| last_outside_service | bigint                      |                |           |
| created_at           | timestamp without time zone |                |           |
| updated_at           | timestamp without time zone |                |           |
| latitude             | double precision            |                |           |
| longitude            | double precision            |                |           |

## activity

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| related_device_id | bigint                      |                |           |
| type              | character varying (150.0)   |                |           |
| description       | character varying (600.0)   |                |           |
| created_at        | timestamp without time zone |                |           |
| last_active_at    | timestamp without time zone |                |           |

## alarm_status_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| alarm_on      | smallint                    |                |           |
| device_id     | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |

## app_config

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| name          | character varying (150.0)   |                |           |
| data          | character varying (1500.0)  |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## app_version

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| platform      | character varying (150.0)   |                |           |
| version_name  | character varying (150.0)   |                |           |
| version_code  | integer                     |                |           |
| update_log    | character varying (765.0)   |                |           |
| force_update  | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## attractions_feedback

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| rating        | smallint                    |                |           |
| comment       | character varying (900.0)   |                |           |
| created_at    | timestamp without time zone |                |           |

## battery_info_record

| column_name                   | data_type                   | related_keys   | remarks   |
|:------------------------------|:----------------------------|:---------------|:----------|
| id                            | bigint                      |                |           |
| gps_id                        | bigint                      |                |           |
| milli_voltage                 | integer                     |                |           |
| milli_current                 | integer                     |                |           |
| remaining_capacity            | integer                     |                |           |
| full_capacity                 | integer                     |                |           |
| work_cycle                    | integer                     |                |           |
| voltage_1                     | integer                     |                |           |
| voltage_2                     | integer                     |                |           |
| voltage_3                     | integer                     |                |           |
| voltage_4                     | integer                     |                |           |
| voltage_5                     | integer                     |                |           |
| voltage_6                     | integer                     |                |           |
| voltage_7                     | integer                     |                |           |
| voltage_8                     | integer                     |                |           |
| voltage_9                     | integer                     |                |           |
| voltage_10                    | integer                     |                |           |
| battery_number                | character varying (192.0)   |                |           |
| undervoltage_protect          | smallint                    |                |           |
| powerlow_warn                 | smallint                    |                |           |
| overvoltage_protect           | smallint                    |                |           |
| overvoltage_warn              | smallint                    |                |           |
| discharge_overcurrent_protect | smallint                    |                |           |
| charge_overcurrent_protect    | smallint                    |                |           |
| discharging                   | smallint                    |                |           |
| charging                      | smallint                    |                |           |
| discharge_mos                 | smallint                    |                |           |
| charge_mos                    | smallint                    |                |           |
| shortout                      | smallint                    |                |           |
| charger_connected             | smallint                    |                |           |
| charge_undertemp_protect      | smallint                    |                |           |
| charge_undertemp_warn         | smallint                    |                |           |
| charge_overtemp_protect       | smallint                    |                |           |
| charge_overtemp_warn          | smallint                    |                |           |
| discharge_undertemp_protect   | smallint                    |                |           |
| discharge_lowtemp_warn        | smallint                    |                |           |
| discharge_overtemp_protect    | smallint                    |                |           |
| discharge_overtemp_warn       | smallint                    |                |           |
| temperature_1                 | integer                     |                |           |
| temperature_2                 | integer                     |                |           |
| remaining_percentage          | integer                     |                |           |
| bms_hardware_version          | integer                     |                |           |
| bms_software_version          | integer                     |                |           |
| created_at                    | timestamp without time zone |                |           |
| working_status                | smallint                    |                |           |

## battery_inspection

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| id             | integer                     |                |           |
| user_id        | bigint                      |                |           |
| status         | character varying (60.0)    |                |           |
| created_at     | timestamp without time zone |                |           |
| city           | character varying (96.0)    |                |           |
| battery_number | character varying (90.0)    |                |           |

## battery_inventory

| column_name        | data_type                   | related_keys   | remarks   |
|:-------------------|:----------------------------|:---------------|:----------|
| id                 | bigint                      |                |           |
| scooter_id         | bigint                      |                |           |
| created_at         | timestamp without time zone |                |           |
| updated_at         | timestamp without time zone |                |           |
| battery_number     | character varying (192.0)   |                |           |
| remaining_capacity | smallint                    |                |           |
| full_capacity      | smallint                    |                |           |
| work_cycle         | smallint                    |                |           |
| electricity        | double precision            |                |           |
| city               | character varying (60.0)    |                |           |
| status             | character varying (90.0)    |                |           |

## battery_status_record

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| status          | character varying (150.0)   |                |           |
| previous_status | character varying (150.0)   |                |           |
| city            | character varying (96.0)    |                |           |
| created_at      | timestamp without time zone |                |           |
| previous_city   | character varying (96.0)    |                |           |
| battery_no      | character varying (150.0)   |                |           |

## battery_swap

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| battery_swap_time | timestamp without time zone |                |           |
| battery_out_no    | character varying (192.0)   |                |           |
| battery_out_level | numeric                     |                |           |
| battery_in_no     | character varying (192.0)   |                |           |
| battery_in_level  | numeric                     |                |           |
| user_id           | bigint                      |                |           |
| created_at        | timestamp without time zone |                |           |
| city              | character varying (96.0)    |                |           |
| vehicle_type      | character varying (96.0)    |                |           |
| scooter_id        | bigint                      |                |           |

## battery_swap_history

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| user_id         | bigint                      |                |           |
| scooter_id      | bigint                      |                |           |
| created_at      | timestamp without time zone |                |           |
| updated_at      | timestamp without time zone |                |           |
| status          | character varying (60.0)    |                |           |
| current_battery | double precision            |                |           |
| battery_no      | character varying (192.0)   |                |           |

## blacklist

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| ip            | character varying (192.0)   |                |           |
| created_at    | timestamp without time zone |                |           |

## campaign

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| created_at      | timestamp without time zone |                |           |
| updated_at      | timestamp without time zone |                |           |
| created_by      | bigint                      |                |           |
| updated_by      | bigint                      |                |           |
| name            | character varying (765.0)   |                |           |
| city            | character varying (765.0)   |                |           |
| base_fee        | double precision            |                |           |
| unit_fee        | double precision            |                |           |
| vehicle_type    | character varying (96.0)    |                |           |
| deleted         | smallint                    |                |           |
| active          | smallint                    |                |           |
| type            | character varying (48.0)    |                |           |
| start_timestamp | timestamp without time zone |                |           |
| end_timestamp   | timestamp without time zone |                |           |
| start_date      | character varying (48.0)    |                |           |
| end_date        | character varying (48.0)    |                |           |
| start_time      | character varying (48.0)    |                |           |
| end_time        | character varying (48.0)    |                |           |
| weekdays        | character varying (48.0)    |                |           |

## cmd_task

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| cmd_list      | character varying (64512.0) |                |           |
| rule          | character varying (600.0)   |                |           |
| task_type     | character varying (60.0)    |                |           |
| valid_time    | timestamp without time zone |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| deleted       | smallint                    |                |           |

## command

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| name          | character varying (150.0)   |                |           |
| content       | character varying (600.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## command_log

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| command       | character varying (3072.0)  |                |           |
| succeed       | smallint                    |                |           |
| device_id     | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |

## command_response

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| message       | character varying (765.0)   |                |           |
| device_id     | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |

## coupon_task_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | character varying (192.0)   |                |           |
| coupon_id     | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| type          | character varying (96.0)    |                |           |
| remark        | character varying (3072.0)  |                |           |

## deposit

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| amount        | double precision            |                |           |
| charge_id     | character varying (300.0)   |                |           |
| refunded      | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## device_blacklist

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| device_id     | character varying (192.0)   |                |           |
| comment       | character varying (768.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |

## dft_quiz_result

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| quiz_id       | smallint                    |                |           |
| trip_id       | bigint                      |                |           |
| response      | character varying (300.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| user_id       | bigint                      |                |           |

## email_verification

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| email         | character varying (765.0)   |                |           |
| token         | character varying (150.0)   |                |           |
| status        | character varying (60.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| user_id       | bigint                      |                |           |

## feedback

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| subject       | character varying (150.0)   |                |           |
| content       | character varying (65535.0) |                |           |
| status        | character varying (150.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| trip_id       | bigint                      |                |           |
| city          | character varying (60.0)    |                |           |
| user_id       | bigint                      |                |           |

## flyway_schema_history

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| installed_rank | integer                     |                |           |
| version        | character varying (150.0)   |                |           |
| description    | character varying (600.0)   |                |           |
| type           | character varying (60.0)    |                |           |
| script         | character varying (3000.0)  |                |           |
| checksum       | integer                     |                |           |
| installed_by   | character varying (300.0)   |                |           |
| installed_on   | timestamp without time zone |                |           |
| execution_time | integer                     |                |           |
| success        | smallint                    |                |           |

## geofence

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| name            | character varying (450.0)   |                |           |
| created_at      | timestamp without time zone |                |           |
| updated_at      | timestamp without time zone |                |           |
| city            | character varying (60.0)    |                |           |
| type            | character varying (96.0)    |                |           |
| max_speed       | integer                     |                |           |
| zone            | character varying (60.0)    |                |           |
| station_id      | bigint                      |                |           |
| start_time      | character varying (30.0)    |                |           |
| end_time        | character varying (30.0)    |                |           |
| hidden          | smallint                    |                |           |
| weekdays        | character varying (96.0)    |                |           |
| status          | character varying (60.0)    |                |           |
| vehicle_type    | character varying (96.0)    |                |           |
| description     | character varying (65535.0) |                |           |
| start_date      | character varying (60.0)    |                |           |
| end_date        | character varying (60.0)    |                |           |
| sidewalk_action | integer                     |                |           |

## geofence_alert

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| user_id           | bigint                      |                |           |
| status            | character varying (48.0)    |                |           |
| created_at        | timestamp without time zone |                |           |
| updated_at        | timestamp without time zone |                |           |
| imei              | character varying (96.0)    |                |           |
| source            | character varying (30.0)    |                |           |
| geofence_id       | bigint                      |                |           |
| geo_trigger_event | character varying (300.0)   |                |           |
| trip_id           | bigint                      |                |           |

## geofence_position

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| id            | numeric                  |                |           |
| latitude      | numeric                  |                |           |
| longitude     | numeric                  |                |           |
| fence_id      | bigint                   |                |           |
| type          | character varying (48.0) |                |           |

## getui_user_client

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| client_id     | character varying (1536.0)  |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| type          | character varying (48.0)    |                |           |
| user_id       | bigint                      |                |           |

## gps_brake

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| gps_id        | bigint                      |                |           |
| brake         | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |

## gps_inventory

| column_name          | data_type                   | related_keys   | remarks   |
|:---------------------|:----------------------------|:---------------|:----------|
| topple               | smallint                    |                |           |
| remaining_range      | numeric                     |                |           |
| blt_password         | character varying (384.0)   |                |           |
| iot_version          | character varying (150.0)   |                |           |
| dbu_hw               | character varying (30.0)    |                |           |
| local_geofence       | smallint                    |                |           |
| version_list         | character varying (64512.0) |                |           |
| white_noise          | smallint                    |                |           |
| mode                 | smallint                    |                |           |
| local_geo_status     | character varying (64512.0) |                |           |
| hdop                 | double precision            |                |           |
| vin                  | double precision            |                |           |
| imei                 | character varying (300.0)   |                |           |
| created_at           | timestamp without time zone |                |           |
| updated_at           | timestamp without time zone |                |           |
| remaining_battery    | double precision            |                |           |
| latitude             | double precision            |                |           |
| longitude            | double precision            |                |           |
| engine_off           | smallint                    |                |           |
| gps_battery_level    | integer                     |                |           |
| gps_external_power   | smallint                    |                |           |
| status               | character varying (60.0)    |                |           |
| alarm_on             | smallint                    |                |           |
| version              | character varying (765.0)   |                |           |
| voltage              | numeric                     |                |           |
| apn                  | character varying (300.0)   |                |           |
| iccid                | character varying (300.0)   |                |           |
| location_source      | character varying (48.0)    |                |           |
| position_active_time | timestamp without time zone |                |           |
| protocol             | character varying (90.0)    |                |           |
| normal_speed         | smallint                    |                |           |
| sport_speed          | smallint                    |                |           |
| dashboard_version    | character varying (384.0)   |                |           |
| motor_version        | character varying (384.0)   |                |           |
| bluetooth_version    | character varying (384.0)   |                |           |
| helmet_attached      | smallint                    |                |           |
| no_parking_light     | smallint                    |                |           |
| wrench_light         | smallint                    |                |           |
| bt_mac               | character varying (96.0)    |                |           |
| is_connected         | smallint                    |                |           |
| ride_mile            | bigint                      |                |           |
| id                   | bigint                      |                |           |

## gps_region

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| imei          | character varying (105.0)   |                |           |
| country       | character varying (48.0)    |                |           |
| created_at    | timestamp without time zone |                |           |

## gps_self_check

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| gps_id        | bigint                      |                |           |
| status        | character varying (96.0)    |                |           |
| description   | character varying (768.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| gps_protocol  | character varying (24.0)    |                |           |
| temp          | smallint                    |                |           |
| baro          | integer                     |                |           |
| mpu           | smallint                    |                |           |
| dbu_vs        | smallint                    |                |           |
| 4gstat        | smallint                    |                |           |
| imei          | character varying (96.0)    |                |           |
| rssi          | integer                     |                |           |
| mode          | smallint                    |                |           |
| sv_unum       | smallint                    |                |           |
| vbat          | integer                     |                |           |
| vin           | integer                     |                |           |
| v18           | integer                     |                |           |
| v50           | integer                     |                |           |
| v90           | integer                     |                |           |
| scu_vs        | smallint                    |                |           |
| ctle          | smallint                    |                |           |
| bat_vs        | smallint                    |                |           |
| batulck       | smallint                    |                |           |
| batstat       | smallint                    |                |           |
| soc           | smallint                    |                |           |
| cyc           | integer                     |                |           |
| dbstat        | smallint                    |                |           |
| brakefault    | smallint                    |                |           |
| accelfault    | smallint                    |                |           |
| hlu_vs        | smallint                    |                |           |
| iot_sw        | character varying (150.0)   |                |           |
| btu_sw        | character varying (150.0)   |                |           |
| btu_vs        | smallint                    |                |           |
| created_by    | bigint                      |                |           |
| fall          | smallint                    |                |           |
| yaw           | integer                     |                |           |
| roll          | integer                     |                |           |
| pitch         | integer                     |                |           |

## helmet_unlock_record

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| user_id         | bigint                      |                |           |
| trip_id         | bigint                      |                |           |
| helmet_attached | smallint                    |                |           |
| scooter_online  | smallint                    |                |           |
| unlock_channel  | character varying (30.0)    |                |           |
| stage           | character varying (60.0)    |                |           |
| created_at      | timestamp without time zone |                |           |
| updated_at      | timestamp without time zone |                |           |
| result          | smallint                    |                |           |

## ibeacon

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| station_id    | bigint                      |                |           |
| major         | character varying (48.0)    |                |           |
| minor         | character varying (48.0)    |                |           |
| uuid          | character varying (150.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## incident_log

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| city          | character varying (90.0)    |                |           |
| risk_level    | character varying (90.0)    |                |           |
| status        | character varying (90.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |
| created_by    | bigint                      |                |           |
| content       | character varying (65535.0) |                |           |
| incident_time | timestamp without time zone |                |           |
| aware_time    | timestamp without time zone |                |           |

## incident_log_image

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| type          | character varying (90.0)    |                |           |
| image_id      | character varying (150.0)   |                |           |
| log_id        | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |

## incident_mechanic_report

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| report_id         | bigint                      |                |           |
| deck_id           | character varying (300.0)   |                |           |
| comment           | character varying (65535.0) |                |           |
| created_at        | timestamp without time zone |                |           |
| created_by        | bigint                      |                |           |
| created_user      | character varying (768.0)   |                |           |
| created_user_role | character varying (768.0)   |                |           |
| updated_user      | character varying (768.0)   |                |           |
| updated_user_role | character varying (768.0)   |                |           |
| updated_at        | timestamp without time zone |                |           |
| updated_by        | bigint                      |                |           |

## incident_report

| column_name          | data_type                   | related_keys   | remarks   |
|:---------------------|:----------------------------|:---------------|:----------|
| id                   | bigint                      |                |           |
| city                 | character varying (90.0)    |                |           |
| business_risk_level  | character varying (300.0)   |                |           |
| risk_level           | character varying (90.0)    |                |           |
| status               | character varying (90.0)    |                |           |
| type                 | character varying (90.0)    |                |           |
| incident_time        | timestamp without time zone |                |           |
| aware_time           | timestamp without time zone |                |           |
| police               | smallint                    |                |           |
| ticket_url           | character varying (768.0)   |                |           |
| short_description    | character varying (3000.0)  |                |           |
| description          | character varying (65535.0) |                |           |
| remarks              | character varying (768.0)   |                |           |
| user_involved        | character varying (300.0)   |                |           |
| users                | character varying (65535.0) |                |           |
| third_party_involved | character varying (300.0)   |                |           |
| parties              | character varying (65535.0) |                |           |
| vehicle_property     | character varying (65535.0) |                |           |
| location             | character varying (768.0)   |                |           |
| created_user         | character varying (768.0)   |                |           |
| created_user_role    | character varying (768.0)   |                |           |
| updated_user         | character varying (768.0)   |                |           |
| updated_user_role    | character varying (768.0)   |                |           |
| platform             | character varying (300.0)   |                |           |
| version              | integer                     |                |           |
| created_at           | timestamp without time zone |                |           |
| updated_at           | timestamp without time zone |                |           |
| updated_by           | bigint                      |                |           |
| created_by           | bigint                      |                |           |
| latitude             | numeric                     |                |           |
| longitude            | numeric                     |                |           |
| category             | character varying (90.0)    |                |           |
| reason               | character varying (765.0)   |                |           |
| source               | character varying (384.0)   |                |           |
| edm_sent             | smallint                    |                |           |

## incident_report_history

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| report_id         | bigint                      |                |           |
| updated_fields    | character varying (1500.0)  |                |           |
| previous          | character varying (65535.0) |                |           |
| current           | character varying (65535.0) |                |           |
| platform          | character varying (300.0)   |                |           |
| version           | integer                     |                |           |
| created_at        | timestamp without time zone |                |           |
| created_by        | bigint                      |                |           |
| created_user      | character varying (768.0)   |                |           |
| created_user_role | character varying (768.0)   |                |           |
| source            | character varying (384.0)   |                |           |

## incident_report_image

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| type          | character varying (90.0)    |                |           |
| image_id      | character varying (150.0)   |                |           |
| report_id     | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| version       | integer                     |                |           |

## incident_report_snapshot

| column_name          | data_type                   | related_keys   | remarks   |
|:---------------------|:----------------------------|:---------------|:----------|
| id                   | bigint                      |                |           |
| report_id            | bigint                      |                |           |
| city                 | character varying (90.0)    |                |           |
| business_risk_level  | character varying (300.0)   |                |           |
| risk_level           | character varying (90.0)    |                |           |
| status               | character varying (90.0)    |                |           |
| type                 | character varying (90.0)    |                |           |
| incident_time        | timestamp without time zone |                |           |
| aware_time           | timestamp without time zone |                |           |
| police               | smallint                    |                |           |
| ticket_url           | character varying (768.0)   |                |           |
| short_description    | character varying (3000.0)  |                |           |
| description          | character varying (65535.0) |                |           |
| remarks              | character varying (768.0)   |                |           |
| user_involved        | character varying (300.0)   |                |           |
| users                | character varying (65535.0) |                |           |
| third_party_involved | character varying (300.0)   |                |           |
| parties              | character varying (65535.0) |                |           |
| vehicle_property     | character varying (65535.0) |                |           |
| location             | character varying (768.0)   |                |           |
| platform             | character varying (300.0)   |                |           |
| version              | integer                     |                |           |
| created_at           | timestamp without time zone |                |           |
| created_by           | bigint                      |                |           |
| created_user         | character varying (768.0)   |                |           |
| created_user_role    | character varying (768.0)   |                |           |
| reason               | character varying (765.0)   |                |           |
| source               | character varying (384.0)   |                |           |

## incident_report_timeline

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| report_id         | bigint                      |                |           |
| type              | character varying (150.0)   |                |           |
| touch_time        | timestamp without time zone |                |           |
| content           | character varying (65535.0) |                |           |
| updated           | smallint                    |                |           |
| created_at        | timestamp without time zone |                |           |
| created_by        | bigint                      |                |           |
| created_user      | character varying (768.0)   |                |           |
| created_user_role | character varying (768.0)   |                |           |
| local_time        | smallint                    |                |           |
| email_template    | character varying (384.0)   |                |           |
| email_address     | character varying (1500.0)  |                |           |

## issue_subscribe_operator

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |
| operator_id   | bigint                      |                |           |
| issue_id      | bigint                      |                |           |
| mute          | smallint                    |                |           |
| important     | smallint                    |                |           |

## location_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| latitude      | double precision            |                |           |
| longitude     | double precision            |                |           |
| type          | character varying (150.0)   |                |           |
| device_id     | character varying (1536.0)  |                |           |

## lock_response

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| device_id     | bigint                      |                |           |
| card_no       | character varying (300.0)   |                |           |
| locked        | smallint                    |                |           |
| succeed       | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |

## maintenance_work

| column_name   | data_type                 | related_keys   | remarks   |
|:--------------|:--------------------------|:---------------|:----------|
| id            | integer                   |                |           |
| title         | character varying (765.0) |                |           |

## maintenance_work_item

| column_name   | data_type                 | related_keys   | remarks   |
|:--------------|:--------------------------|:---------------|:----------|
| id            | integer                   |                |           |
| title         | character varying (765.0) |                |           |
| group_id      | integer                   |                |           |
| deleted       | smallint                  |                |           |

## maintenance_work_performed

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| maintenance_id    | bigint                      |                |           |
| performed_item_id | integer                     |                |           |
| created_at        | timestamp without time zone |                |           |
| updated_at        | timestamp without time zone |                |           |
| updated_by        | bigint                      |                |           |
| action            | character varying (765.0)   |                |           |
| quantity          | smallint                    |                |           |
| comment           | character varying (6000.0)  |                |           |
| created_by        | bigint                      |                |           |

## mechanic_report_image

| column_name        | data_type                   | related_keys   | remarks   |
|:-------------------|:----------------------------|:---------------|:----------|
| id                 | bigint                      |                |           |
| image_id           | character varying (150.0)   |                |           |
| mechanic_report_id | bigint                      |                |           |
| created_at         | timestamp without time zone |                |           |

## mobile_verification

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| sms_code      | character varying (30.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| country_code  | character varying (48.0)    |                |           |
| remote_ip     | character varying (192.0)   |                |           |
| mobile        | character varying (150.0)   |                |           |

## mqtt_btu

| column_name                | data_type                   | related_keys   | remarks   |
|:---------------------------|:----------------------------|:---------------|:----------|
| id                         | bigint                      |                |           |
| gps_id                     | bigint                      |                |           |
| created_at                 | timestamp without time zone |                |           |
| sn                         | character varying (96.0)    |                |           |
| hw                         | character varying (96.0)    |                |           |
| sw                         | character varying (96.0)    |                |           |
| enabled                    | smallint                    |                |           |
| valid_status               | smallint                    |                |           |
| valid_count                | integer                     |                |           |
| working_status             | smallint                    |                |           |
| receive_success_bytes      | integer                     |                |           |
| receive_failure_bytes      | integer                     |                |           |
| send_success_bytes         | integer                     |                |           |
| send_failure_bytes         | integer                     |                |           |
| receive_send_total_failure | integer                     |                |           |

## mqtt_dbu

| column_name                | data_type                   | related_keys   | remarks   |
|:---------------------------|:----------------------------|:---------------|:----------|
| id                         | bigint                      |                |           |
| gps_id                     | bigint                      |                |           |
| created_at                 | timestamp without time zone |                |           |
| sn                         | character varying (96.0)    |                |           |
| hw                         | character varying (96.0)    |                |           |
| sw                         | character varying (96.0)    |                |           |
| gear                       | smallint                    |                |           |
| working_status             | smallint                    |                |           |
| screen_on                  | smallint                    |                |           |
| headlight_on               | smallint                    |                |           |
| bell_on                    | smallint                    |                |           |
| braking                    | smallint                    |                |           |
| accelerate                 | smallint                    |                |           |
| receive_success_bytes      | integer                     |                |           |
| receive_failure_bytes      | integer                     |                |           |
| send_success_bytes         | integer                     |                |           |
| send_failure_bytes         | integer                     |                |           |
| receive_send_total_failure | integer                     |                |           |

## mqtt_hlu

| column_name                | data_type                   | related_keys   | remarks   |
|:---------------------------|:----------------------------|:---------------|:----------|
| id                         | bigint                      |                |           |
| gps_id                     | bigint                      |                |           |
| created_at                 | timestamp without time zone |                |           |
| sn                         | character varying (96.0)    |                |           |
| hw                         | character varying (96.0)    |                |           |
| sw                         | character varying (96.0)    |                |           |
| working_status             | smallint                    |                |           |
| receive_success_bytes      | integer                     |                |           |
| receive_failure_bytes      | integer                     |                |           |
| send_success_bytes         | integer                     |                |           |
| send_failure_bytes         | integer                     |                |           |
| receive_send_total_failure | integer                     |                |           |
| helmet_locked              | smallint                    |                |           |
| physical_lock              | smallint                    |                |           |

## mqtt_iot

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| gps_id            | bigint                      |                |           |
| created_at        | timestamp without time zone |                |           |
| sn                | character varying (96.0)    |                |           |
| hw                | character varying (96.0)    |                |           |
| sw                | character varying (96.0)    |                |           |
| working_status    | smallint                    |                |           |
| uptime            | integer                     |                |           |
| temperature       | numeric                     |                |           |
| water_leak        | smallint                    |                |           |
| flow              | integer                     |                |           |
| baro              | integer                     |                |           |
| mpu_status        | smallint                    |                |           |
| topple            | smallint                    |                |           |
| total_mile        | integer                     |                |           |
| ride_mile         | integer                     |                |           |
| dangerous_driving | smallint                    |                |           |
| scu_vs            | smallint                    |                |           |
| scu_vc            | smallint                    |                |           |
| bat_vs            | smallint                    |                |           |
| bat_vc            | smallint                    |                |           |
| dbu_vs            | smallint                    |                |           |
| dbu_vc            | smallint                    |                |           |
| hlu_vs            | smallint                    |                |           |
| hlu_vc            | smallint                    |                |           |
| lte_vs            | smallint                    |                |           |
| lte_vc            | smallint                    |                |           |
| low_power_mode    | smallint                    |                |           |
| vbat              | integer                     |                |           |
| vin               | integer                     |                |           |
| v18               | integer                     |                |           |
| v50               | integer                     |                |           |
| v90               | integer                     |                |           |

## mqtt_lte

| column_name                   | data_type                   | related_keys   | remarks   |
|:------------------------------|:----------------------------|:---------------|:----------|
| id                            | bigint                      |                |           |
| gps_id                        | bigint                      |                |           |
| created_at                    | timestamp without time zone |                |           |
| sn                            | character varying (96.0)    |                |           |
| hw                            | character varying (96.0)    |                |           |
| sw                            | character varying (96.0)    |                |           |
| working_status                | smallint                    |                |           |
| imsi                          | character varying (192.0)   |                |           |
| iccid                         | character varying (96.0)    |                |           |
| join_status                   | smallint                    |                |           |
| rssi                          | smallint                    |                |           |
| ber                           | smallint                    |                |           |
| operator                      | character varying (192.0)   |                |           |
| tech                          | character varying (192.0)   |                |           |
| brand                         | character varying (192.0)   |                |           |
| chan                          | character varying (192.0)   |                |           |
| channel                       | character varying (192.0)   |                |           |
| reset_count                   | integer                     |                |           |
| online_count                  | integer                     |                |           |
| join_count                    | integer                     |                |           |
| dial_count                    | integer                     |                |           |
| connection_count              | integer                     |                |           |
| network_receive_success_count | integer                     |                |           |
| network_receive_failure_count | integer                     |                |           |
| network_send_success_count    | integer                     |                |           |
| network_send_failure_count    | integer                     |                |           |
| receive_success_bytes         | integer                     |                |           |
| receive_failure_bytes         | integer                     |                |           |
| send_success_bytes            | integer                     |                |           |
| send_failure_bytes            | integer                     |                |           |
| receive_send_total_failure    | integer                     |                |           |

## mqtt_mpu

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| gps_id        | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| ts            | integer                     |                |           |
| gyro1         | numeric                     |                |           |
| gyro2         | numeric                     |                |           |
| gyro3         | numeric                     |                |           |
| accelerate1   | numeric                     |                |           |
| accelerate2   | numeric                     |                |           |
| accelerate3   | numeric                     |                |           |
| yaw_angle     | numeric                     |                |           |
| pitch_angle   | numeric                     |                |           |
| roll_angle    | numeric                     |                |           |

## mqtt_position

| column_name      | data_type                   | related_keys   | remarks   |
|:-----------------|:----------------------------|:---------------|:----------|
| id               | bigint                      |                |           |
| gps_id           | bigint                      |                |           |
| created_at       | timestamp without time zone |                |           |
| latitude         | numeric                     |                |           |
| longitude        | numeric                     |                |           |
| altitude         | numeric                     |                |           |
| speed            | numeric                     |                |           |
| course           | numeric                     |                |           |
| pdop             | numeric                     |                |           |
| hdop             | numeric                     |                |           |
| vdop             | numeric                     |                |           |
| device_time      | timestamp without time zone |                |           |
| visible_st_num   | smallint                    |                |           |
| using_st_num     | smallint                    |                |           |
| inview_st_sn     | character varying (384.0)   |                |           |
| inview_st_signal | character varying (384.0)   |                |           |
| status           | smallint                    |                |           |
| mode             | smallint                    |                |           |
| work_status      | smallint                    |                |           |

## mqtt_scu

| column_name                | data_type                   | related_keys   | remarks   |
|:---------------------------|:----------------------------|:---------------|:----------|
| id                         | bigint                      |                |           |
| gps_id                     | bigint                      |                |           |
| created_at                 | timestamp without time zone |                |           |
| sn                         | character varying (96.0)    |                |           |
| hw                         | character varying (96.0)    |                |           |
| sw                         | character varying (96.0)    |                |           |
| working_status             | smallint                    |                |           |
| voltage                    | integer                     |                |           |
| current                    | integer                     |                |           |
| speed                      | numeric                     |                |           |
| unlocked                   | smallint                    |                |           |
| current_limit              | integer                     |                |           |
| gear                       | smallint                    |                |           |
| controller_failure         | smallint                    |                |           |
| low_voltage                | smallint                    |                |           |
| over_current               | smallint                    |                |           |
| mhall_failure              | smallint                    |                |           |
| mstall_failure             | smallint                    |                |           |
| mploss_failure             | smallint                    |                |           |
| battery_network_failure    | smallint                    |                |           |
| battery_lock_failure       | smallint                    |                |           |
| battery_lock_unlocked      | smallint                    |                |           |
| receive_success_bytes      | integer                     |                |           |
| receive_failure_bytes      | integer                     |                |           |
| send_success_bytes         | integer                     |                |           |
| send_failure_bytes         | integer                     |                |           |
| receive_send_total_failure | integer                     |                |           |
| speed_limit1               | integer                     |                |           |
| speed_limit2               | integer                     |                |           |

## n3_status

| column_name             | data_type                   | related_keys   | remarks   |
|:------------------------|:----------------------------|:---------------|:----------|
| id                      | bigint                      |                |           |
| electric_hall           | smallint                    |                |           |
| a_phase_current         | smallint                    |                |           |
| b_phase_current         | smallint                    |                |           |
| c_phase_current         | smallint                    |                |           |
| dc_current              | smallint                    |                |           |
| accelerator             | smallint                    |                |           |
| dashboard_communication | smallint                    |                |           |
| bms_communication       | smallint                    |                |           |
| brake                   | smallint                    |                |           |
| iot_communication       | smallint                    |                |           |
| ble_communication       | smallint                    |                |           |
| low_battery             | smallint                    |                |           |
| low_battery_protection  | smallint                    |                |           |
| topple                  | smallint                    |                |           |
| lock_status             | character varying (96.0)    |                |           |
| battery_percentage      | double precision            |                |           |
| device_id               | bigint                      |                |           |
| created_at              | timestamp without time zone |                |           |
| single_range            | double precision            |                |           |
| speed                   | double precision            |                |           |
| voltage                 | double precision            |                |           |
| electricity             | double precision            |                |           |
| helmet_attached         | smallint                    |                |           |

## neuron_file

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| type          | character varying (60.0)    |                |           |
| name          | character varying (765.0)   |                |           |
| url           | character varying (1500.0)  |                |           |
| md5           | character varying (300.0)   |                |           |
| recognized    | smallint                    |                |           |
| confidence    | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| bucket        | character varying (150.0)   |                |           |
| audited       | smallint                    |                |           |
| id            | character varying (300.0)   |                |           |

## neuron_order

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| amount        | numeric                     |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| pass_id       | bigint                      |                |           |
| status        | character varying (96.0)    |                |           |
| updated_by    | bigint                      |                |           |
| version       | integer                     |                |           |
| currency      | character varying (48.0)    |                |           |
| city_base_fee | numeric                     |                |           |
| city_unit_fee | numeric                     |                |           |
| trip_id       | bigint                      |                |           |

## neuron_sync_queue

| column_name         | data_type                   | related_keys   | remarks   |
|:--------------------|:----------------------------|:---------------|:----------|
| id                  | numeric                     |                |           |
| type                | character varying (150.0)   |                |           |
| json_body           | character varying (3000.0)  |                |           |
| succeed             | smallint                    |                |           |
| created_at          | timestamp without time zone |                |           |
| updated_at          | timestamp without time zone |                |           |
| original_created_at | timestamp without time zone |                |           |
| original_id         | bigint                      |                |           |

## neuron_sync_scooter_zego

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | numeric                     |                |           |
| scooter_id      | bigint                      |                |           |
| zego_vehicle_id | bigint                      |                |           |
| created_at      | timestamp without time zone |                |           |

## neuron_transaction

| column_name      | data_type                   | related_keys   | remarks   |
|:-----------------|:----------------------------|:---------------|:----------|
| id               | bigint                      |                |           |
| amount           | numeric                     |                |           |
| currency         | character varying (48.0)    |                |           |
| type             | character varying (150.0)   |                |           |
| created_at       | timestamp without time zone |                |           |
| order_id         | bigint                      |                |           |
| stripe_charge_id | character varying (300.0)   |                |           |
| stripe_refund_id | character varying (300.0)   |                |           |
| my_pass_id       | bigint                      |                |           |
| method           | character varying (192.0)   |                |           |
| automatic        | smallint                    |                |           |
| brand            | character varying (96.0)    |                |           |
| last4            | character varying (12.0)    |                |           |
| platform         | character varying (96.0)    |                |           |
| account          | character varying (96.0)    |                |           |
| user_pass_id     | bigint                      |                |           |
| dispute_status   | character varying (96.0)    |                |           |
| dispute_amount   | double precision            |                |           |
| updated_at       | timestamp without time zone |                |           |
| city             | character varying (96.0)    |                |           |
| client_ip        | character varying (96.0)    |                |           |
| trip_deposit_id  | bigint                      |                |           |
| user_id          | bigint                      |                |           |

## notification

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| content       | character varying (65535.0) |                |           |
| type          | character varying (150.0)   |                |           |
| data          | character varying (65535.0) |                |           |
| created_at    | timestamp without time zone |                |           |

## notification_config

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| title         | character varying (765.0)   |                |           |
| start_time    | character varying (30.0)    |                |           |
| end_time      | character varying (30.0)    |                |           |
| weekdays      | character varying (90.0)    |                |           |
| status        | character varying (60.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |
| message_id    | character varying (150.0)   |                |           |
| city          | character varying (60.0)    |                |           |
| trigger_event | character varying (150.0)   |                |           |
| message_type  | character varying (60.0)    |                |           |

## okai_battery_sn

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| battery       | character varying (360.0)   |                |           |
| sn            | character varying (192.0)   |                |           |
| created_at    | timestamp without time zone |                |           |

## ops_live_location

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| city          | character varying (150.0)   |                |           |
| latitude      | double precision            |                |           |
| longitude     | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| username      | character varying (768.0)   |                |           |
| full_name     | character varying (768.0)   |                |           |
| avatar        | character varying (768.0)   |                |           |

## ops_open_deck

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| user_id           | bigint                      |                |           |
| battery_no        | character varying (192.0)   |                |           |
| remaining_battery | numeric                     |                |           |
| open_deck_time    | timestamp without time zone |                |           |
| open_channel      | character varying (60.0)    |                |           |
| created_at        | timestamp without time zone |                |           |
| status            | character varying (60.0)    |                |           |
| scooter_id        | bigint                      |                |           |

## order_detail

| column_name            | data_type                   | related_keys   | remarks   |
|:-----------------------|:----------------------------|:---------------|:----------|
| id                     | bigint                      |                |           |
| user_id                | bigint                      |                |           |
| trip_minutes           | bigint                      |                |           |
| trip_mileage           | bigint                      |                |           |
| vip                    | smallint                    |                |           |
| pass_minutes           | bigint                      |                |           |
| coupon_amount          | numeric                     |                |           |
| wallet_amount          | numeric                     |                |           |
| credit_amount          | numeric                     |                |           |
| created_at             | timestamp without time zone |                |           |
| user_coupon_id         | bigint                      |                |           |
| convenience_fee        | numeric                     |                |           |
| trip_fare              | numeric                     |                |           |
| pass_amount            | numeric                     |                |           |
| adjust_off             | numeric                     |                |           |
| updated_at             | timestamp without time zone |                |           |
| updated_by             | bigint                      |                |           |
| currency               | character varying (48.0)    |                |           |
| scooter_discount       | numeric                     |                |           |
| is_short_trip          | smallint                    |                |           |
| trip_cap_off           | numeric                     |                |           |
| base_fee               | numeric                     |                |           |
| unit_fee               | numeric                     |                |           |
| user_pass_id           | bigint                      |                |           |
| helmet_replacement_fee | numeric                     |                |           |
| deposit_captured       | numeric                     |                |           |
| order_id               | bigint                      |                |           |

## organization

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| code          | character varying (150.0)   |                |           |
| name          | character varying (765.0)   |                |           |
| type          | character varying (150.0)   |                |           |
| domains       | character varying (1500.0)  |                |           |
| deleted       | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## organization_discount

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | integer                     |                |           |
| org_code      | character varying (765.0)   |                |           |
| discount      | integer                     |                |           |
| pass_days     | integer                     |                |           |
| deleted       | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| pass_id       | bigint                      |                |           |

## organization_email_verification

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| email         | character varying (765.0)   |                |           |
| token         | character varying (150.0)   |                |           |
| status        | character varying (60.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| user_id       | bigint                      |                |           |
| deleted       | smallint                    |                |           |
| org_code      | character varying (150.0)   |                |           |

## organization_invitation

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| org_code      | character varying (765.0)   |                |           |
| code          | character varying (96.0)    |                |           |
| status        | character varying (96.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |

## organization_secret_key

| column_name      | data_type                   | related_keys   | remarks   |
|:-----------------|:----------------------------|:---------------|:----------|
| id               | bigint                      |                |           |
| org_code         | character varying (765.0)   |                |           |
| encrypted_secret | character varying (1536.0)  |                |           |
| status           | character varying (96.0)    |                |           |
| created_at       | timestamp without time zone |                |           |
| updated_at       | timestamp without time zone |                |           |
| created_by       | bigint                      |                |           |
| updated_by       | bigint                      |                |           |

## organization_user

| column_name      | data_type                   | related_keys   | remarks   |
|:-----------------|:----------------------------|:---------------|:----------|
| id               | bigint                      |                |           |
| org_code         | character varying (765.0)   |                |           |
| user_id          | bigint                      |                |           |
| created_at       | timestamp without time zone |                |           |
| updated_at       | timestamp without time zone |                |           |
| deleted          | smallint                    |                |           |
| external_user_id | character varying (384.0)   |                |           |

## ota_status_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| status        | character varying (96.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| task_id       | bigint                      |                |           |
| scooter_id    | bigint                      |                |           |
| created_by    | bigint                      |                |           |
| notes         | character varying (300.0)   |                |           |
| imei          | character varying (96.0)    |                |           |

## ota_task

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| firmware      | character varying (384.0)   |                |           |
| status        | character varying (96.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| type          | character varying (96.0)    |                |           |
| description   | character varying (300.0)   |                |           |
| version       | integer                     |                |           |
| fm_version    | character varying (48.0)    |                |           |
| start_time    | character varying (48.0)    |                |           |
| end_time      | character varying (48.0)    |                |           |
| scooter_id    | bigint                      |                |           |
| protocol      | character varying (90.0)    |                |           |
| voice         | character varying (150.0)   |                |           |
| upgrade_start | timestamp without time zone |                |           |
| upgrade_end   | timestamp without time zone |                |           |
| retry_times   | smallint                    |                |           |
| progress_info | character varying (600.0)   |                |           |
| language      | character varying (48.0)    |                |           |
| enum_protocol | character varying (48.0)    |                |           |

## pass

| column_name              | data_type                   | related_keys   | remarks   |
|:-------------------------|:----------------------------|:---------------|:----------|
| id                       | bigint                      |                |           |
| title                    | character varying (150.0)   |                |           |
| duration                 | bigint                      |                |           |
| free_min                 | bigint                      |                |           |
| price                    | numeric                     |                |           |
| status                   | character varying (60.0)    |                |           |
| created_at               | timestamp without time zone |                |           |
| updated_at               | timestamp without time zone |                |           |
| currency                 | character varying (48.0)    |                |           |
| created_by               | bigint                      |                |           |
| updated_by               | bigint                      |                |           |
| description              | character varying (65535.0) |                |           |
| reference_price          | numeric                     |                |           |
| lang_title               | character varying (6000.0)  |                |           |
| lang_description         | character varying (6000.0)  |                |           |
| cities                   | character varying (1536.0)  |                |           |
| renewable_after_inactive | bigint                      |                |           |
| renewable                | smallint                    |                |           |

## password_reset

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | integer                     |                |           |
| email         | character varying (450.0)   |                |           |
| token         | character varying (300.0)   |                |           |
| status        | character varying (60.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## payment_transaction_mapping

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| id             | bigint                      |                |           |
| type           | character varying (96.0)    |                |           |
| value          | bigint                      |                |           |
| created_at     | timestamp without time zone |                |           |
| transaction_id | bigint                      |                |           |

## promo_code

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| name          | character varying (384.0)   |                |           |
| type          | character varying (192.0)   |                |           |
| value         | double precision            |                |           |
| expired_at    | timestamp without time zone |                |           |
| valid_time    | integer                     |                |           |
| currency      | character varying (192.0)   |                |           |
| remark        | character varying (3072.0)  |                |           |
| parent        | bigint                      |                |           |
| created_by    | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| redeem_limit  | integer                     |                |           |
| valid_at      | timestamp without time zone |                |           |
| scope         | character varying (45.0)    |                |           |
| status        | character varying (60.0)    |                |           |
| coupon_name   | character varying (768.0)   |                |           |

## purchase_intent

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| pass_id           | bigint                      |                |           |
| order_id          | bigint                      |                |           |
| amount            | numeric                     |                |           |
| currency          | character varying (48.0)    |                |           |
| status            | character varying (96.0)    |                |           |
| type              | character varying (96.0)    |                |           |
| payment_intent_id | character varying (384.0)   |                |           |
| created_at        | timestamp without time zone |                |           |
| updated_at        | timestamp without time zone |                |           |
| platform          | character varying (96.0)    |                |           |
| account           | character varying (96.0)    |                |           |
| client_ip         | character varying (96.0)    |                |           |
| trip_deposit_id   | bigint                      |                |           |
| user_id           | bigint                      |                |           |

## qrtz_blob_triggers

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| sched_name    | character varying (360.0)   |                |           |
| trigger_name  | character varying (600.0)   |                |           |
| trigger_group | character varying (600.0)   |                |           |
| blob_data     | character varying (65535.0) |                |           |

## qrtz_calendars

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| sched_name    | character varying (360.0)   |                |           |
| calendar_name | character varying (600.0)   |                |           |
| calendar      | character varying (65535.0) |                |           |

## qrtz_cron_triggers

| column_name     | data_type                 | related_keys   | remarks   |
|:----------------|:--------------------------|:---------------|:----------|
| sched_name      | character varying (360.0) |                |           |
| trigger_name    | character varying (600.0) |                |           |
| trigger_group   | character varying (600.0) |                |           |
| cron_expression | character varying (360.0) |                |           |
| time_zone_id    | character varying (240.0) |                |           |

## qrtz_fired_triggers

| column_name       | data_type                 | related_keys   | remarks   |
|:------------------|:--------------------------|:---------------|:----------|
| sched_name        | character varying (360.0) |                |           |
| entry_id          | character varying (285.0) |                |           |
| trigger_name      | character varying (600.0) |                |           |
| trigger_group     | character varying (600.0) |                |           |
| instance_name     | character varying (600.0) |                |           |
| fired_time        | bigint                    |                |           |
| sched_time        | bigint                    |                |           |
| priority          | integer                   |                |           |
| state             | character varying (48.0)  |                |           |
| job_name          | character varying (600.0) |                |           |
| job_group         | character varying (600.0) |                |           |
| is_nonconcurrent  | character varying (15.0)  |                |           |
| requests_recovery | character varying (15.0)  |                |           |

## qrtz_job_details

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| sched_name        | character varying (360.0)   |                |           |
| job_name          | character varying (600.0)   |                |           |
| job_group         | character varying (600.0)   |                |           |
| description       | character varying (750.0)   |                |           |
| job_class_name    | character varying (750.0)   |                |           |
| is_durable        | character varying (15.0)    |                |           |
| is_nonconcurrent  | character varying (15.0)    |                |           |
| is_update_data    | character varying (15.0)    |                |           |
| requests_recovery | character varying (15.0)    |                |           |
| job_data          | character varying (65535.0) |                |           |

## qrtz_locks

| column_name   | data_type                 | related_keys   | remarks   |
|:--------------|:--------------------------|:---------------|:----------|
| sched_name    | character varying (360.0) |                |           |
| lock_name     | character varying (120.0) |                |           |

## qrtz_paused_trigger_grps

| column_name   | data_type                 | related_keys   | remarks   |
|:--------------|:--------------------------|:---------------|:----------|
| sched_name    | character varying (360.0) |                |           |
| trigger_group | character varying (600.0) |                |           |

## qrtz_scheduler_state

| column_name       | data_type                 | related_keys   | remarks   |
|:------------------|:--------------------------|:---------------|:----------|
| sched_name        | character varying (360.0) |                |           |
| instance_name     | character varying (600.0) |                |           |
| last_checkin_time | bigint                    |                |           |
| checkin_interval  | bigint                    |                |           |

## qrtz_simple_triggers

| column_name     | data_type                 | related_keys   | remarks   |
|:----------------|:--------------------------|:---------------|:----------|
| sched_name      | character varying (360.0) |                |           |
| trigger_name    | character varying (600.0) |                |           |
| trigger_group   | character varying (600.0) |                |           |
| repeat_count    | bigint                    |                |           |
| repeat_interval | bigint                    |                |           |
| times_triggered | bigint                    |                |           |

## qrtz_simprop_triggers

| column_name   | data_type                  | related_keys   | remarks   |
|:--------------|:---------------------------|:---------------|:----------|
| sched_name    | character varying (360.0)  |                |           |
| trigger_name  | character varying (600.0)  |                |           |
| trigger_group | character varying (600.0)  |                |           |
| str_prop_1    | character varying (1536.0) |                |           |
| str_prop_2    | character varying (1536.0) |                |           |
| str_prop_3    | character varying (1536.0) |                |           |
| int_prop_1    | integer                    |                |           |
| int_prop_2    | integer                    |                |           |
| long_prop_1   | bigint                     |                |           |
| long_prop_2   | bigint                     |                |           |
| dec_prop_1    | numeric                    |                |           |
| dec_prop_2    | numeric                    |                |           |
| bool_prop_1   | character varying (3.0)    |                |           |
| bool_prop_2   | character varying (3.0)    |                |           |

## qrtz_triggers

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| sched_name     | character varying (360.0)   |                |           |
| trigger_name   | character varying (600.0)   |                |           |
| trigger_group  | character varying (600.0)   |                |           |
| job_name       | character varying (600.0)   |                |           |
| job_group      | character varying (600.0)   |                |           |
| description    | character varying (750.0)   |                |           |
| next_fire_time | bigint                      |                |           |
| prev_fire_time | bigint                      |                |           |
| priority       | integer                     |                |           |
| trigger_state  | character varying (48.0)    |                |           |
| trigger_type   | character varying (24.0)    |                |           |
| start_time     | bigint                      |                |           |
| end_time       | bigint                      |                |           |
| calendar_name  | character varying (600.0)   |                |           |
| misfire_instr  | smallint                    |                |           |
| job_data       | character varying (65535.0) |                |           |

## rebalance

| column_name      | data_type                   | related_keys   | remarks   |
|:-----------------|:----------------------------|:---------------|:----------|
| id               | bigint                      |                |           |
| created_by       | bigint                      |                |           |
| start_station_id | bigint                      |                |           |
| end_station_id   | bigint                      |                |           |
| created_at       | timestamp without time zone |                |           |
| updated_at       | timestamp without time zone |                |           |
| status           | character varying (48.0)    |                |           |
| updated_by       | bigint                      |                |           |
| user_gps_time    | timestamp without time zone |                |           |
| from_scan        | smallint                    |                |           |
| start_latitude   | numeric                     |                |           |
| start_longitude  | numeric                     |                |           |
| finish_latitude  | numeric                     |                |           |
| finish_longitude | numeric                     |                |           |
| city             | character varying (96.0)    |                |           |
| vehicle_type     | character varying (96.0)    |                |           |
| start_zone       | character varying (150.0)   |                |           |
| end_zone         | character varying (150.0)   |                |           |
| scooter_id       | bigint                      |                |           |

## receipt

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| type          | character varying (60.0)    |                |           |
| source        | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| serial_num    | character varying (60.0)    |                |           |
| sent_num      | integer                     |                |           |

## rtk_station

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| imei           | character varying (96.0)    |                |           |
| city           | character varying (96.0)    |                |           |
| rtk_data       | character varying (65535.0) |                |           |
| latitude       | numeric                     |                |           |
| longitude      | numeric                     |                |           |
| created_at     | timestamp without time zone |                |           |
| updated_at     | timestamp without time zone |                |           |
| role           | character varying (30.0)    |                |           |
| switch_role_at | timestamp without time zone |                |           |

## sagemaker_training_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| type          | character varying (96.0)    |                |           |
| user_id       | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| raw           | character varying (64512.0) |                |           |
| feature       | character varying (1536.0)  |                |           |
| result        | character varying (1536.0)  |                |           |

## scan_record

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| id                | bigint                      |                |           |
| user_id           | bigint                      |                |           |
| scooter_id        | bigint                      |                |           |
| station_id        | bigint                      |                |           |
| scooter_status    | character varying (150.0)   |                |           |
| user_latitude     | double precision            |                |           |
| user_longitude    | double precision            |                |           |
| remaining_battery | double precision            |                |           |
| user_gps_time     | timestamp without time zone |                |           |
| error_code        | character varying (96.0)    |                |           |
| trip_id           | bigint                      |                |           |
| updated_at        | timestamp without time zone |                |           |
| app_version       | character varying (96.0)    |                |           |
| platform          | character varying (96.0)    |                |           |
| device_id         | character varying (384.0)   |                |           |
| imei              | character varying (96.0)    |                |           |
| city              | character varying (96.0)    |                |           |
| vehicle_type      | character varying (96.0)    |                |           |
| vin               | double precision            |                |           |
| created_at        | timestamp without time zone |                |           |

## scooter_assemble_record

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| id             | bigint                      |                |           |
| user_id        | bigint                      |                |           |
| scooter_id     | bigint                      |                |           |
| imei           | character varying (105.0)   |                |           |
| deck_id        | character varying (90.0)    |                |           |
| mac            | character varying (90.0)    |                |           |
| scooter_status | character varying (60.0)    |                |           |
| type           | character varying (60.0)    |                |           |
| created_at     | timestamp without time zone |                |           |
| licence        | character varying (105.0)   |                |           |
| opt_lock_no    | character varying (105.0)   |                |           |
| sticker        | character varying (90.0)    |                |           |
| qr_code        | character varying (48.0)    |                |           |

## scooter_check_record

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| id             | bigint                      |                |           |
| status         | character varying (60.0)    |                |           |
| content        | character varying (65535.0) |                |           |
| maintenance_id | bigint                      |                |           |
| type           | character varying (60.0)    |                |           |
| created_at     | timestamp without time zone |                |           |
| updated_at     | timestamp without time zone |                |           |
| created_by     | bigint                      |                |           |
| updated_by     | bigint                      |                |           |
| city           | character varying (96.0)    |                |           |
| device_id      | bigint                      |                |           |

## scooter_city_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| city          | character varying (60.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| zone          | character varying (60.0)    |                |           |
| pre_city      | character varying (60.0)    |                |           |
| pre_zone      | character varying (90.0)    |                |           |
| scooter_id    | bigint                      |                |           |

## scooter_cmd_exec

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| imei            | character varying (300.0)   |                |           |
| task_id         | bigint                      |                |           |
| task_type       | character varying (60.0)    |                |           |
| status          | character varying (30.0)    |                |           |
| cmd_record_list | character varying (64512.0) |                |           |
| created_at      | timestamp without time zone |                |           |
| updated_at      | timestamp without time zone |                |           |
| deleted         | smallint                    |                |           |

## scooter_extend_info

| column_name                  | data_type                   | related_keys   | remarks   |
|:-----------------------------|:----------------------------|:---------------|:----------|
| id                           | bigint                      |                |           |
| scooter_id                   | bigint                      |                |           |
| latest_trip_start_time       | timestamp without time zone |                |           |
| latest_trip_end_time         | timestamp without time zone |                |           |
| created_at                   | timestamp without time zone |                |           |
| updated_at                   | timestamp without time zone |                |           |
| latest_rebalance_finish_time | timestamp without time zone |                |           |

## scooter_inventory

| column_name        | data_type                   | related_keys   | remarks   |
|:-------------------|:----------------------------|:---------------|:----------|
| id                 | bigint                      |                |           |
| imei               | character varying (300.0)   |                |           |
| station_id         | bigint                      |                |           |
| qr_code            | character varying (48.0)    |                |           |
| deck_id            | character varying (60.0)    |                |           |
| mac                | character varying (60.0)    |                |           |
| ic_card            | character varying (765.0)   |                |           |
| status             | character varying (60.0)    |                |           |
| city               | character varying (60.0)    |                |           |
| created_at         | timestamp without time zone |                |           |
| updated_at         | timestamp without time zone |                |           |
| version            | integer                     |                |           |
| deleted            | smallint                    |                |           |
| image_id           | character varying (192.0)   |                |           |
| generation         | character varying (48.0)    |                |           |
| zone               | character varying (60.0)    |                |           |
| licence            | character varying (105.0)   |                |           |
| type               | character varying (60.0)    |                |           |
| last_test_time     | timestamp without time zone |                |           |
| helmet_lock        | smallint                    |                |           |
| sticker            | character varying (90.0)    |                |           |
| last_sanitize_time | timestamp without time zone |                |           |

## scooter_issue

| column_name            | data_type                   | related_keys   | remarks   |
|:-----------------------|:----------------------------|:---------------|:----------|
| id                     | bigint                      |                |           |
| type                   | character varying (300.0)   |                |           |
| content                | character varying (65535.0) |                |           |
| created_at             | timestamp without time zone |                |           |
| updated_at             | timestamp without time zone |                |           |
| created_by             | bigint                      |                |           |
| updated_by             | bigint                      |                |           |
| status                 | character varying (60.0)    |                |           |
| maintenance_id         | bigint                      |                |           |
| city                   | character varying (96.0)    |                |           |
| vehicle_type           | character varying (96.0)    |                |           |
| create_user_name       | character varying (768.0)   |                |           |
| create_user_role       | character varying (150.0)   |                |           |
| update_user_name       | character varying (768.0)   |                |           |
| update_user_role       | character varying (150.0)   |                |           |
| muted_users            | character varying (65535.0) |                |           |
| high_risk              | smallint                    |                |           |
| tag                    | character varying (768.0)   |                |           |
| users_marked_important | character varying (65535.0) |                |           |
| scooter_latitude       | numeric                     |                |           |
| scooter_longitude      | numeric                     |                |           |
| device_id              | bigint                      |                |           |

## scooter_issue_extend_info

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| issue_id      | bigint                      |                |           |
| phone         | character varying (150.0)   |                |           |
| lang          | character varying (60.0)    |                |           |
| need_callback | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## scooter_issue_image

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| image_id      | character varying (150.0)   |                |           |
| issue_id      | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| issue_log_id  | bigint                      |                |           |

## scooter_issue_log

| column_name           | data_type                   | related_keys   | remarks   |
|:----------------------|:----------------------------|:---------------|:----------|
| id                    | bigint                      |                |           |
| action                | character varying (1500.0)  |                |           |
| comment               | character varying (3000.0)  |                |           |
| action_template       | character varying (1500.0)  |                |           |
| action_template_param | character varying (1500.0)  |                |           |
| created_at            | timestamp without time zone |                |           |
| created_by            | bigint                      |                |           |
| type                  | character varying (150.0)   |                |           |
| issue_id              | bigint                      |                |           |

## scooter_issue_notification

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| message       | character varying (768.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| issue_id      | bigint                      |                |           |

## scooter_maintenance

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |
| status        | character varying (150.0)   |                |           |
| station_id    | bigint                      |                |           |
| comment       | character varying (65535.0) |                |           |
| vehicle_type  | character varying (96.0)    |                |           |
| device_id     | bigint                      |                |           |

## scooter_maintenance_status_record

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| id             | bigint                      |                |           |
| maintenance_id | bigint                      |                |           |
| status         | character varying (150.0)   |                |           |
| created_at     | timestamp without time zone |                |           |
| created_by     | bigint                      |                |           |
| device_id      | bigint                      |                |           |

## scooter_missing_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| scooter_id    | bigint                      |                |           |
| comment       | character varying (1500.0)  |                |           |
| photos        | character varying (1500.0)  |                |           |
| city          | character varying (96.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| reason        | character varying (750.0)   |                |           |

## scooter_reservation

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| status        | character varying (60.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |
| device_id     | bigint                      |                |           |
| vehicle_type  | character varying (96.0)    |                |           |
| city          | character varying (60.0)    |                |           |
| zone          | character varying (60.0)    |                |           |
| valid_count   | smallint                    |                |           |
| user_id       | bigint                      |                |           |

## scooter_retired_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| scooter_id    | bigint                      |                |           |
| comment       | character varying (1500.0)  |                |           |
| photos        | character varying (1500.0)  |                |           |
| city          | character varying (96.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| reason        | character varying (750.0)   |                |           |
| type          | character varying (150.0)   |                |           |

## scooter_sanitize_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| city          | character varying (150.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| scooter_id    | bigint                      |                |           |

## scooter_speed_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| imei          | character varying (96.0)    |                |           |
| mode          | character varying (48.0)    |                |           |
| user_id       | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| speed         | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |

## scooter_status_record

| column_name                | data_type                   | related_keys   | remarks   |
|:---------------------------|:----------------------------|:---------------|:----------|
| id                         | bigint                      |                |           |
| status                     | character varying (60.0)    |                |           |
| created_at                 | timestamp without time zone |                |           |
| scooter_latitude           | double precision            |                |           |
| scooter_longitude          | double precision            |                |           |
| previous_status            | character varying (60.0)    |                |           |
| previous_status_created_at | timestamp without time zone |                |           |
| battery                    | double precision            |                |           |
| object_id                  | bigint                      |                |           |
| vehicle_type               | character varying (96.0)    |                |           |
| zone                       | character varying (60.0)    |                |           |
| previous_zone              | character varying (90.0)    |                |           |
| device_id                  | bigint                      |                |           |
| city                       | character varying (96.0)    |                |           |

## shop

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | character varying (48.0)    |                |           |
| city          | character varying (60.0)    |                |           |
| name          | character varying (768.0)   |                |           |
| category      | character varying (60.0)    |                |           |
| address       | character varying (768.0)   |                |           |
| suburb        | character varying (90.0)    |                |           |
| website       | character varying (1536.0)  |                |           |
| phone         | character varying (60.0)    |                |           |
| contact_name  | character varying (768.0)   |                |           |
| contact_email | character varying (768.0)   |                |           |
| image         | character varying (768.0)   |                |           |
| disabled      | smallint                    |                |           |
| deleted       | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |

## shop_offer

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| title         | character varying (150.0)   |                |           |
| frequency     | character varying (60.0)    |                |           |
| detail        | character varying (65535.0) |                |           |
| disabled      | smallint                    |                |           |
| deleted       | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |
| shop_id       | character varying (48.0)    |                |           |
| total_number  | bigint                      |                |           |

## shop_offer_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| offer_id      | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |

## sign_up_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| mobile        | character varying (150.0)   |                |           |
| country_code  | character varying (48.0)    |                |           |
| remote_ip     | character varying (150.0)   |                |           |
| geo_city      | character varying (300.0)   |                |           |
| ip_city       | character varying (300.0)   |                |           |
| latitude      | double precision            |                |           |
| longitude     | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| app_version   | character varying (192.0)   |                |           |
| system        | character varying (192.0)   |                |           |
| device_id     | character varying (192.0)   |                |           |
| user_city     | character varying (96.0)    |                |           |
| social_id     | character varying (300.0)   |                |           |
| social_type   | character varying (96.0)    |                |           |
| email         | character varying (300.0)   |                |           |
| user_id       | bigint                      |                |           |

## sms_delivery_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| country_code  | character varying (24.0)    |                |           |
| status        | character varying (96.0)    |                |           |
| type          | character varying (48.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| error_code    | integer                     |                |           |
| error_msg     | character varying (1536.0)  |                |           |
| remote_ip     | character varying (96.0)    |                |           |
| mobile        | character varying (96.0)    |                |           |

## station

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| id             | bigint                      |                |           |
| name           | character varying (765.0)   |                |           |
| description    | character varying (1500.0)  |                |           |
| latitude       | double precision            |                |           |
| longitude      | double precision            |                |           |
| photo          | character varying (1500.0)  |                |           |
| created_at     | timestamp without time zone |                |           |
| updated_at     | timestamp without time zone |                |           |
| ibeacon_radius | double precision            |                |           |
| gps_radius     | double precision            |                |           |
| deleted        | smallint                    |                |           |
| optimal_number | integer                     |                |           |
| guide          | character varying (1536.0)  |                |           |
| type           | character varying (90.0)    |                |           |
| city           | character varying (60.0)    |                |           |
| zone           | character varying (60.0)    |                |           |
| qr_code        | character varying (60.0)    |                |           |
| vehicle_type   | character varying (96.0)    |                |           |
| start_date     | character varying (60.0)    |                |           |
| end_date       | character varying (60.0)    |                |           |
| start_time     | character varying (30.0)    |                |           |
| end_time       | character varying (30.0)    |                |           |
| weekdays       | character varying (96.0)    |                |           |

## station_images

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| station_id    | bigint                      |                |           |
| image_id      | character varying (192.0)   |                |           |
| sort          | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |

## station_location_request

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| name          | character varying (300.0)   |                |           |
| email         | character varying (300.0)   |                |           |
| comment       | character varying (65535.0) |                |           |
| latitude      | double precision            |                |           |
| longitude     | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |

## station_optimal

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| station_id    | bigint                      |                |           |
| weekday       | character varying (30.0)    |                |           |
| capacity      | integer                     |                |           |
| start_time    | character varying (30.0)    |                |           |
| end_time      | character varying (30.0)    |                |           |
| created_at    | timestamp without time zone |                |           |

## status

| column_name           | data_type                   | related_keys   | remarks   |
|:----------------------|:----------------------------|:---------------|:----------|
| id                    | bigint                      |                |           |
| motor_phase           | smallint                    |                |           |
| voltage_protection    | smallint                    |                |           |
| controller_failure    | smallint                    |                |           |
| turn_brushless        | smallint                    |                |           |
| communication_failure | smallint                    |                |           |
| current               | integer                     |                |           |
| speed                 | double precision            |                |           |
| battery_percentage    | double precision            |                |           |
| device_id             | bigint                      |                |           |
| created_at            | timestamp without time zone |                |           |
| voltage               | numeric                     |                |           |

## stripe_exception_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| type          | character varying (48.0)    |                |           |
| order_id      | bigint                      |                |           |
| pass_id       | bigint                      |                |           |
| charge_id     | character varying (384.0)   |                |           |
| message       | character varying (3072.0)  |                |           |
| created_by    | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| pay_method    | character varying (96.0)    |                |           |
| decline_code  | character varying (768.0)   |                |           |
| platform      | character varying (96.0)    |                |           |
| user_id       | bigint                      |                |           |

## top_up_card

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| code          | character varying (60.0)    |                |           |
| value         | numeric                     |                |           |
| status        | character varying (48.0)    |                |           |
| redeemed_by   | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |

## trip

| column_name         | data_type                   | related_keys   | remarks   |
|:--------------------|:----------------------------|:---------------|:----------|
| id                  | bigint                      |                |           |
| end_time            | timestamp without time zone |                |           |
| user_id             | bigint                      |                |           |
| device_id           | bigint                      |                |           |
| trip_status         | character varying (48.0)    |                |           |
| total_minutes       | integer                     |                |           |
| total_mileage       | double precision            |                |           |
| start_station_id    | bigint                      |                |           |
| returned_station_id | bigint                      |                |           |
| mx_user_id          | character varying (300.0)   |                |           |
| force_end           | smallint                    |                |           |
| bt_unlock           | smallint                    |                |           |
| gold_trip           | smallint                    |                |           |
| auto_end            | smallint                    |                |           |
| temp_lock           | smallint                    |                |           |
| is_short_trip       | smallint                    |                |           |
| is_vip              | smallint                    |                |           |
| zone                | character varying (60.0)    |                |           |
| group_id            | character varying (192.0)   |                |           |
| engine_off_minutes  | integer                     |                |           |
| user_name           | character varying (300.0)   |                |           |
| email               | character varying (300.0)   |                |           |
| gps_id              | bigint                      |                |           |
| transfer_ride       | smallint                    |                |           |
| helmet_wore         | smallint                    |                |           |
| helmet_returned     | smallint                    |                |           |
| app_version         | character varying (60.0)    |                |           |
| start_latitude      | double precision            |                |           |
| start_longitude     | double precision            |                |           |
| end_latitude        | double precision            |                |           |
| end_longitude       | double precision            |                |           |
| require_helmet      | smallint                    |                |           |
| start_no_riding     | smallint                    |                |           |
| end_no_riding       | smallint                    |                |           |
| imei                | character varying (96.0)    |                |           |
| battery_no          | character varying (192.0)   |                |           |
| vehicle_type        | character varying (96.0)    |                |           |
| start_zone          | character varying (60.0)    |                |           |
| end_zone            | character varying (60.0)    |                |           |
| positions           | character varying (65535.0) |                |           |
| campaign_id         | bigint                      |                |           |
| ride_mile           | bigint                      |                |           |
| start_time          | timestamp without time zone |                |           |
| city                | character varying (60.0)    |                |           |

## trip_alert

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | numeric                     |                |           |
| trip_id       | bigint                      |                |           |
| scooter_id    | bigint                      |                |           |
| duration      | bigint                      |                |           |
| alert_time    | timestamp without time zone |                |           |
| complete_time | timestamp without time zone |                |           |
| type          | character varying (75.0)    |                |           |
| status        | character varying (75.0)    |                |           |
| is_read       | smallint                    |                |           |
| user_id       | bigint                      |                |           |

## trip_deposit

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| user_id         | bigint                      |                |           |
| trip_id         | bigint                      |                |           |
| status          | character varying (96.0)    |                |           |
| charge_id       | character varying (192.0)   |                |           |
| amount          | double precision            |                |           |
| captured_amount | double precision            |                |           |
| created_at      | timestamp without time zone |                |           |
| updated_at      | timestamp without time zone |                |           |
| job_id          | character varying (96.0)    |                |           |

## trip_end_record

| column_name         | data_type                   | related_keys   | remarks   |
|:--------------------|:----------------------------|:---------------|:----------|
| id                  | bigint                      |                |           |
| scooter_id          | bigint                      |                |           |
| gps_id              | bigint                      |                |           |
| user_id             | bigint                      |                |           |
| created_at          | timestamp without time zone |                |           |
| user_latitude       | numeric                     |                |           |
| user_longitude      | numeric                     |                |           |
| user_gps_time       | timestamp without time zone |                |           |
| scooter_latitude    | numeric                     |                |           |
| scooter_longitude   | numeric                     |                |           |
| return_station_id   | bigint                      |                |           |
| station_source_type | character varying (90.0)    |                |           |
| trip_id             | bigint                      |                |           |

## trip_error_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| device_id     | bigint                      |                |           |
| station_id    | bigint                      |                |           |
| error_code    | character varying (150.0)   |                |           |
| error_message | character varying (765.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| trip_id       | bigint                      |                |           |
| user_id       | bigint                      |                |           |

## trip_event_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| trip_event    | character varying (150.0)   |                |           |
| trip_info     | character varying (765.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| user_id       | bigint                      |                |           |

## trip_finish_source

| column_name        | data_type                   | related_keys   | remarks   |
|:-------------------|:----------------------------|:---------------|:----------|
| id                 | bigint                      |                |           |
| station_id         | bigint                      |                |           |
| user_latitude      | double precision            |                |           |
| user_longitude     | double precision            |                |           |
| device_latitude    | double precision            |                |           |
| device_longitude   | double precision            |                |           |
| type               | character varying (150.0)   |                |           |
| created_at         | timestamp without time zone |                |           |
| scooter_locked     | smallint                    |                |           |
| station_latitude   | double precision            |                |           |
| station_longitude  | double precision            |                |           |
| updated_at         | timestamp without time zone |                |           |
| bluetooth_locked   | smallint                    |                |           |
| image_id           | character varying (192.0)   |                |           |
| scooter_id         | bigint                      |                |           |
| user_id            | bigint                      |                |           |
| user_gps_time      | timestamp without time zone |                |           |
| device_active_time | timestamp without time zone |                |           |
| trip_id            | bigint                      |                |           |

## trip_helmet

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| image_id      | character varying (192.0)   |                |           |
| checked       | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |
| trip_id       | bigint                      |                |           |

## trip_parking_photo

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| image_id      | character varying (192.0)   |                |           |
| trip_id       | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| checked       | smallint                    |                |           |
| reminded      | smallint                    |                |           |
| status        | character varying (60.0)    |                |           |
| check_answers | character varying (1500.0)  |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |
| duration      | bigint                      |                |           |
| city          | character varying (60.0)    |                |           |
| vehicle_id    | bigint                      |                |           |

## trip_quartz_checker

| column_name              | data_type                   | related_keys   | remarks   |
|:-------------------------|:----------------------------|:---------------|:----------|
| id                       | bigint                      |                |           |
| user_id                  | bigint                      |                |           |
| sidewalk_job_id          | character varying (600.0)   |                |           |
| dangerous_driving_job_id | character varying (600.0)   |                |           |
| created_at               | timestamp without time zone |                |           |
| auto_end_job_id          | character varying (600.0)   |                |           |
| trip_id                  | bigint                      |                |           |

## trip_rating

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| rating        | integer                     |                |           |
| comment       | character varying (6000.0)  |                |           |
| system        | character varying (48.0)    |                |           |
| app_version   | character varying (48.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| trip_id       | bigint                      |                |           |

## trip_report

| column_name           | data_type                   | related_keys   | remarks   |
|:----------------------|:----------------------------|:---------------|:----------|
| id                    | bigint                      |                |           |
| created_at            | timestamp without time zone |                |           |
| user_id               | bigint                      |                |           |
| trip_helmet_id        | bigint                      |                |           |
| trip_parking_photo_id | bigint                      |                |           |
| trip_sidewalk_id      | bigint                      |                |           |
| is_viewed             | smallint                    |                |           |
| updated_at            | timestamp without time zone |                |           |
| trip_id               | bigint                      |                |           |

## trip_rider_info

| column_name            | data_type                   | related_keys   | remarks   |
|:-----------------------|:----------------------------|:---------------|:----------|
| id                     | bigint                      |                |           |
| first_name             | character varying (192.0)   |                |           |
| last_name              | character varying (192.0)   |                |           |
| driver_license         | character varying (768.0)   |                |           |
| driver_license_country | character varying (48.0)    |                |           |
| email                  | character varying (384.0)   |                |           |
| phone                  | character varying (48.0)    |                |           |
| is_account_holder      | smallint                    |                |           |
| created_at             | timestamp without time zone |                |           |
| full_name              | character varying (65535.0) |                |           |
| trip_id                | bigint                      |                |           |

## trip_section

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| start_time    | timestamp without time zone |                |           |
| end_time      | timestamp without time zone |                |           |
| trip_id       | bigint                      |                |           |
| mileage       | double precision            |                |           |

## trip_sidewalk

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| trip_id       | bigint                      |                |           |
| result        | smallint                    |                |           |
| source        | character varying (192.0)   |                |           |

## user_card

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| fingerprint   | character varying (192.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | character varying (192.0)   |                |           |
| verified      | smallint                    |                |           |
| deleted       | smallint                    |                |           |
| user_id       | bigint                      |                |           |

## user_city_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| city          | character varying (60.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| user_id       | bigint                      |                |           |

## user_concession_evidence

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| city          | character varying (60.0)    |                |           |
| full_name     | character varying (150.0)   |                |           |
| type          | character varying (150.0)   |                |           |
| status        | character varying (60.0)    |                |           |
| audited_at    | timestamp without time zone |                |           |
| audited_by    | bigint                      |                |           |
| remark        | character varying (1500.0)  |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |
| check_answers | character varying (1500.0)  |                |           |

## user_concession_evidence_image

| column_name            | data_type                 | related_keys   | remarks   |
|:-----------------------|:--------------------------|:---------------|:----------|
| id                     | bigint                    |                |           |
| concession_evidence_id | bigint                    |                |           |
| image_id               | character varying (192.0) |                |           |

## user_concession_evidence_request

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| city          | character varying (60.0)    |                |           |
| full_name     | character varying (150.0)   |                |           |
| type          | character varying (150.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |

## user_coupon

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| promo_code_id | bigint                      |                |           |
| balance       | numeric                     |                |           |
| expired_at    | timestamp without time zone |                |           |
| status        | character varying (192.0)   |                |           |
| created_by    | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| currency      | character varying (48.0)    |                |           |
| scope         | character varying (45.0)    |                |           |
| source        | character varying (768.0)   |                |           |
| code          | character varying (768.0)   |                |           |
| discount      | smallint                    |                |           |
| pass_days     | smallint                    |                |           |
| is_read       | smallint                    |                |           |
| pass_id       | bigint                      |                |           |
| user_id       | bigint                      |                |           |

## user_device_info

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| system        | character varying (60.0)    |                |           |
| sys_version   | character varying (90.0)    |                |           |
| app_version   | character varying (90.0)    |                |           |
| brand         | character varying (150.0)   |                |           |
| model         | character varying (150.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| device_id     | character varying (192.0)   |                |           |
| type          | character varying (90.0)    |                |           |
| lang          | character varying (30.0)    |                |           |
| user_id       | bigint                      |                |           |

## user_device_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| device_id     | character varying (384.0)   |                |           |
| created_at    | timestamp without time zone |                |           |
| platform      | character varying (96.0)    |                |           |
| user_id       | bigint                      |                |           |

## user_gps_tracing

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | numeric                     |                |           |
| user_id       | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| created_at    | timestamp without time zone |                |           |
| accuracy      | numeric                     |                |           |
| fixed_time    | timestamp without time zone |                |           |
| speed         | numeric                     |                |           |

## user_identification

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| issue_country | character varying (96.0)    |                |           |
| type          | character varying (96.0)    |                |           |
| created_by    | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |

## user_identification_file

| column_name            | data_type                 | related_keys   | remarks   |
|:-----------------------|:--------------------------|:---------------|:----------|
| id                     | bigint                    |                |           |
| user_identification_id | bigint                    |                |           |
| file_type              | character varying (96.0)  |                |           |
| file_id                | character varying (384.0) |                |           |

## user_issue_notification_config

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| config        | character varying (3000.0)  |                |           |
| mute_type     | character varying (60.0)    |                |           |
| mute_date     | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## user_license

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| type            | character varying (48.0)    |                |           |
| data            | character varying (65535.0) |                |           |
| file_id         | character varying (192.0)   |                |           |
| created_at      | timestamp without time zone |                |           |
| updated_at      | timestamp without time zone |                |           |
| verified        | smallint                    |                |           |
| first_name      | character varying (192.0)   |                |           |
| last_name       | character varying (192.0)   |                |           |
| licence_number  | character varying (192.0)   |                |           |
| licence_country | character varying (96.0)    |                |           |
| full_name       | character varying (65535.0) |                |           |
| user_id         | bigint                      |                |           |

## user_license_scan_record

| column_name        | data_type                   | related_keys   | remarks   |
|:-------------------|:----------------------------|:---------------|:----------|
| id                 | bigint                      |                |           |
| license_number     | character varying (150.0)   |                |           |
| license_country    | character varying (60.0)    |                |           |
| full_name          | character varying (765.0)   |                |           |
| scan_status        | character varying (60.0)    |                |           |
| created_at         | timestamp without time zone |                |           |
| scan_result_detail | character varying (65535.0) |                |           |
| data               | character varying (65535.0) |                |           |
| encrypted_raw_data | character varying (65535.0) |                |           |
| user_id            | bigint                      |                |           |

## user_location_reader

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| share_record_id | bigint                      |                |           |
| ip              | character varying (192.0)   |                |           |
| browser         | character varying (3072.0)  |                |           |
| created_at      | timestamp without time zone |                |           |
| updated_at      | timestamp without time zone |                |           |

## user_location_share

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| token         | character varying (192.0)   |                |           |
| share_status  | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |

## user_mpu

| column_name     | data_type                   | related_keys   | remarks   |
|:----------------|:----------------------------|:---------------|:----------|
| id              | bigint                      |                |           |
| user_id         | bigint                      |                |           |
| created_at      | timestamp without time zone |                |           |
| device_time     | timestamp without time zone |                |           |
| x_acceleration  | double precision            |                |           |
| y_acceleration  | double precision            |                |           |
| z_acceleration  | double precision            |                |           |
| x_rotation_rate | double precision            |                |           |
| y_rotation_rate | double precision            |                |           |
| z_rotation_rate | double precision            |                |           |
| sensor_time     | bigint                      |                |           |

## user_notification

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| name          | character varying (384.0)   |                |           |
| trigger_corn  | character varying (96.0)    |                |           |
| description   | character varying (3072.0)  |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |

## user_pass

| column_name              | data_type                   | related_keys   | remarks   |
|:-------------------------|:----------------------------|:---------------|:----------|
| pass_id                  | bigint                      |                |           |
| started_at               | timestamp without time zone |                |           |
| expired_at               | timestamp without time zone |                |           |
| subscribe                | smallint                    |                |           |
| future_pass_id           | bigint                      |                |           |
| future_subscribe         | smallint                    |                |           |
| job_id                   | character varying (192.0)   |                |           |
| refund                   | numeric                     |                |           |
| created_at               | timestamp without time zone |                |           |
| created_by               | bigint                      |                |           |
| updated_at               | timestamp without time zone |                |           |
| updated_by               | bigint                      |                |           |
| organization_discount_id | integer                     |                |           |
| purchase_price           | numeric                     |                |           |
| user_coupon_id           | bigint                      |                |           |
| id                       | bigint                      |                |           |
| user_id                  | bigint                      |                |           |

## user_pass_period

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| balance       | integer                     |                |           |
| start_time    | timestamp without time zone |                |           |
| end_time      | timestamp without time zone |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| id            | bigint                      |                |           |
| user_pass_id  | bigint                      |                |           |

## user_promo_coupon

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| code          | character varying (30.0)    |                |           |
| email         | character varying (765.0)   |                |           |
| reaction_time | double precision            |                |           |
| value         | numeric                     |                |           |
| scope         | character varying (60.0)    |                |           |
| user_id       | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| type          | character varying (60.0)    |                |           |
| promo_code_id | bigint                      |                |           |

## user_referral

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| code          | character varying (48.0)    |                |           |
| created_at    | timestamp without time zone |                |           |
| referrer_id   | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |

## user_setting

| column_name          | data_type                   | related_keys   | remarks   |
|:---------------------|:----------------------------|:---------------|:----------|
| id                   | bigint                      |                |           |
| user_agree_tos       | smallint                    |                |           |
| user_agree_pic       | smallint                    |                |           |
| user_agree_insurance | smallint                    |                |           |
| user_id              | bigint                      |                |           |
| created_at           | timestamp without time zone |                |           |
| updated_at           | timestamp without time zone |                |           |
| beginner_mode        | smallint                    |                |           |
| has_finished_trip    | smallint                    |                |           |

## user_status_record

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |
| status        | character varying (150.0)   |                |           |
| created_at    | timestamp without time zone |                |           |

## wallet

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| balance       | numeric                     |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| currency      | character varying (48.0)    |                |           |
| cash          | numeric                     |                |           |
| credit        | numeric                     |                |           |
| id            | bigint                      |                |           |
| user_id       | bigint                      |                |           |

## zone

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| code          | character varying (60.0)    |                |           |
| city_code     | character varying (60.0)    |                |           |
| name          | character varying (765.0)   |                |           |
| base_fee      | numeric                     |                |           |
| unit_fee      | numeric                     |                |           |
| created_by    | bigint                      |                |           |
| updated_by    | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| updated_at    | timestamp without time zone |                |           |
| deleted       | smallint                    |                |           |