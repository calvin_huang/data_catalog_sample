[TOC]

## currency

| column_name   | data_type                   | related_keys   |
|:--------------|:----------------------------|:---------------|
| country       | character varying (20)      |                |
| date          | timestamp without time zone |                |
| currency      | character varying (20)      |                |
| fx_rate       | double precision            |                |

## revenue

| column_name               | data_type                   | related_keys   |
|:--------------------------|:----------------------------|:---------------|
| country                   | character varying (20)      |                |
| city                      | character varying (20)      |                |
| days_since_ops            | integer                     |                |
| day_of_week               | character varying (20)      |                |
| date                      | timestamp without time zone |                |
| total_gross_revenue       | double precision            |                |
| gross_pass_rev            | double precision            |                |
| new_user_pass_rev         | double precision            |                |
| retained_user_pass_rev    | double precision            |                |
| gross_payg_rev            | double precision            |                |
| payg_scooter_rev          | double precision            |                |
| new_scooter_payg_rev      | double precision            |                |
| retained_scooter_payg_rev | double precision            |                |
| payg_ebike_rev            | double precision            |                |
| new_ebike_payg_rev        | double precision            |                |
| retained_ebike_payg_rev   | double precision            |                |
| total_discount            | double precision            |                |
| pass_discount             | double precision            |                |
| payg_discount             | double precision            |                |
| cac                       | double precision            |                |
| crc                       | double precision            |                |
| total_refund              | double precision            |                |
| trip_refund               | double precision            |                |
| pass_refund               | double precision            |                |
| adjust_off                | double precision            |                |
| pending_amount            | double precision            |                |
| pending_payment_prior     | double precision            |                |
| tax_today                 | double precision            |                |
| net_rev_today             | double precision            |                |
| tax_prior                 | double precision            |                |
| net_rev_prior             | double precision            |                |
| updated_at                | timestamp without time zone |                |

## revenue_usd

| column_name               | data_type                   | related_keys   |
|:--------------------------|:----------------------------|:---------------|
| country                   | character varying (20)      |                |
| city                      | character varying (20)      |                |
| days_since_ops            | integer                     |                |
| day_of_week               | character varying (20)      |                |
| date                      | timestamp without time zone |                |
| total_gross_revenue       | double precision            |                |
| gross_pass_rev            | double precision            |                |
| new_user_pass_rev         | double precision            |                |
| retained_user_pass_rev    | double precision            |                |
| gross_payg_rev            | double precision            |                |
| payg_scooter_rev          | double precision            |                |
| new_scooter_payg_rev      | double precision            |                |
| retained_scooter_payg_rev | double precision            |                |
| payg_ebike_rev            | double precision            |                |
| new_ebike_payg_rev        | double precision            |                |
| retained_ebike_payg_rev   | double precision            |                |
| total_discount            | double precision            |                |
| pass_discount             | double precision            |                |
| payg_discount             | double precision            |                |
| cac                       | double precision            |                |
| crc                       | double precision            |                |
| total_refund              | double precision            |                |
| trip_refund               | double precision            |                |
| pass_refund               | double precision            |                |
| adjust_off                | double precision            |                |
| pending_amount            | double precision            |                |
| pending_payment_prior     | double precision            |                |
| tax_today                 | double precision            |                |
| net_rev_today             | double precision            |                |
| tax_prior                 | double precision            |                |
| net_rev_prior             | double precision            |                |
| updated_at                | timestamp without time zone |                |
| currency                  | character varying (20)      |                |
| fx_rate                   | double precision            |                |

## supply

| column_name                    | data_type                   | related_keys   |
|:-------------------------------|:----------------------------|:---------------|
| country                        | character varying (20)      |                |
| city                           | character varying (20)      |                |
| days_since_ops                 | integer                     |                |
| day_of_week                    | character varying (20)      |                |
| date                           | timestamp without time zone |                |
| rebalance_diff_station_scooter | integer                     |                |
| rebalance_same_station_scooter | integer                     |                |
| rebalance_wh_station_scooter   | integer                     |                |
| battery_swap_scooter           | integer                     |                |
| effective_battery_swap_scooter | integer                     |                |
| rebalance_diff_station_ebike   | integer                     |                |
| rebalance_same_station_ebike   | integer                     |                |
| rebalance_wh_station_ebike     | integer                     |                |
| battery_swap_ebike             | integer                     |                |
| effective_battery_swap_ebike   | integer                     |                |
| daily_deployed_scooter         | integer                     |                |
| daily_operational_scooter      | integer                     |                |
| daily_circulating_scooter      | integer                     |                |
| daily_deployed_ebike           | integer                     |                |
| daily_operational_ebike        | integer                     |                |
| daily_circulating_ebike        | integer                     |                |
| updated_at                     | timestamp without time zone |                |

## tableau_entitlement

| column_name   | data_type               | related_keys   |
|:--------------|:------------------------|:---------------|
| country       | character varying (20)  |                |
| city          | character varying (20)  |                |
| email         | character varying (100) |                |
| role_code     | character varying (100) |                |

## utilisation

| column_name                | data_type                   | related_keys   |
|:---------------------------|:----------------------------|:---------------|
| country                    | character varying (20)      |                |
| city                       | character varying (20)      |                |
| days_since_ops             | integer                     |                |
| day_of_week                | character varying (20)      |                |
| date                       | timestamp without time zone |                |
| avg_temp                   | double precision            |                |
| rain_hours                 | integer                     |                |
| dominant_weather           | character varying (20)      |                |
| daily_deployed_scooter     | integer                     |                |
| daily_deployed_ebike       | integer                     |                |
| daily_active_scooter       | integer                     |                |
| daily_active_ebike         | integer                     |                |
| sign_up                    | integer                     |                |
| total_user                 | integer                     |                |
| scooter_user               | integer                     |                |
| new_scooter_user           | integer                     |                |
| new_scooter_payg_user      | integer                     |                |
| new_scooter_pass_user      | integer                     |                |
| retained_scooter_user      | integer                     |                |
| retained_scooter_payg_user | integer                     |                |
| retained_scooter_pass_user | integer                     |                |
| ebike_user                 | integer                     |                |
| new_ebike_user             | integer                     |                |
| new_ebike_payg_user        | integer                     |                |
| new_ebike_pass_user        | integer                     |                |
| retained_ebike_user        | integer                     |                |
| retained_ebike_payg_user   | integer                     |                |
| retained_ebike_pass_user   | integer                     |                |
| total_trip                 | integer                     |                |
| scooter_trip               | integer                     |                |
| new_scooter_trip           | integer                     |                |
| new_scooter_payg_trip      | integer                     |                |
| new_scooter_pass_trip      | integer                     |                |
| retained_scooter_trip      | integer                     |                |
| retained_scooter_payg_trip | integer                     |                |
| retained_scooter_pass_trip | integer                     |                |
| ebike_trip                 | integer                     |                |
| new_ebike_trip             | integer                     |                |
| new_ebike_payg_trip        | integer                     |                |
| new_ebike_pass_trip        | integer                     |                |
| retained_ebike_trip        | integer                     |                |
| retained_ebike_payg_trip   | integer                     |                |
| retained_ebike_pass_trip   | integer                     |                |
| total_mileage              | double precision            |                |
| scooter_mileage            | double precision            |                |
| ebike_mileage              | double precision            |                |
| total_minutes              | integer                     |                |
| scooter_minutes            | integer                     |                |
| ebike_minutes              | integer                     |                |
| updated_at                 | timestamp without time zone |                |