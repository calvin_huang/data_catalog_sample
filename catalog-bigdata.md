[TOC]


# neuron_bigdata



## activity_history

| column_name    | data_type                | related_keys   | remarks   |
|:---------------|:-------------------------|:---------------|:----------|
| uuid           | character varying (64)   |                |           |
| imei           | character varying (32)   |                |           |
| type           | character varying (50)   |                |           |
| description    | character varying (200)  |                |           |
| created_at     | timestamp with time zone |                |           |
| last_active_at | timestamp with time zone |                |           |
| country        | character varying (10)   |                |           |

## battery_info_record_history

| column_name                   | data_type                | related_keys   | remarks   |
|:------------------------------|:-------------------------|:---------------|:----------|
| uuid                          | character varying (64)   |                |           |
| imei                          | character varying (32)   |                |           |
| milli_voltage                 | integer                  |                |           |
| milli_current                 | integer                  |                |           |
| remaining_capacity            | integer                  |                |           |
| full_capacity                 | integer                  |                |           |
| work_cycle                    | integer                  |                |           |
| voltage1                      | integer                  |                |           |
| voltage2                      | integer                  |                |           |
| voltage3                      | integer                  |                |           |
| voltage4                      | integer                  |                |           |
| voltage5                      | integer                  |                |           |
| voltage6                      | integer                  |                |           |
| voltage7                      | integer                  |                |           |
| voltage8                      | integer                  |                |           |
| voltage9                      | integer                  |                |           |
| voltage10                     | integer                  |                |           |
| battery_no                    | character varying (64)   |                |           |
| undervoltage_protect          | boolean                  |                |           |
| powerlow_warn                 | boolean                  |                |           |
| overvoltage_protect           | boolean                  |                |           |
| overvoltage_warn              | boolean                  |                |           |
| discharge_overcurrent_protect | boolean                  |                |           |
| charge_overcurrent_protect    | boolean                  |                |           |
| discharging                   | boolean                  |                |           |
| charging                      | boolean                  |                |           |
| discharge_mos                 | boolean                  |                |           |
| charge_mos                    | boolean                  |                |           |
| shortout                      | boolean                  |                |           |
| charger_connected             | boolean                  |                |           |
| charge_undertemp_protect      | boolean                  |                |           |
| charge_undertemp_warn         | boolean                  |                |           |
| charge_overtemp_protect       | boolean                  |                |           |
| charge_overtemp_warn          | boolean                  |                |           |
| discharge_undertemp_protect   | boolean                  |                |           |
| discharge_lowtemp_warn        | boolean                  |                |           |
| discharge_overtemp_protect    | boolean                  |                |           |
| discharge_overtemp_warn       | boolean                  |                |           |
| temperature1                  | integer                  |                |           |
| temperature2                  | integer                  |                |           |
| remaining_percentage          | integer                  |                |           |
| bms_hardware_version          | integer                  |                |           |
| bms_software_version          | integer                  |                |           |
| created_at                    | timestamp with time zone |                |           |
| device_time                   | timestamp with time zone |                |           |
| working_status                | boolean                  |                |           |
| country                       | character varying (10)   |                |           |

## command_log_history

| column_name   | data_type                 | related_keys   | remarks   |
|:--------------|:--------------------------|:---------------|:----------|
| uuid          | character varying (64)    |                |           |
| command       | character varying (65535) |                |           |
| imei          | character varying (32)    |                |           |
| created_at    | timestamp with time zone  |                |           |
| country       | character varying (10)    |                |           |

## command_response_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| message       | character varying (1500) |                |           |
| imei          | character varying (32)   |                |           |
| device_time   | timestamp with time zone |                |           |
| created_at    | timestamp with time zone |                |           |
| country       | character varying (10)   |                |           |

## event_record_history

| column_name          | data_type                | related_keys   | remarks   |
|:---------------------|:-------------------------|:---------------|:----------|
| uuid                 | character varying (64)   |                |           |
| user_id              | bigint                   |                |           |
| device_id            | character varying (64)   |                |           |
| device_time          | timestamp with time zone |                |           |
| trip_id              | bigint                   |                |           |
| event                | character varying (64)   |                |           |
| source               | character varying (32)   |                |           |
| created_at           | timestamp with time zone |                |           |
| readable_description | character varying (1000) |                |           |
| comment              | character varying (1000) |                |           |
| country              | character varying (10)   |                |           |
| content              | character varying (2048) |                |           |

## gps_brake_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| imei          | character varying (32)   |                |           |
| brake         | boolean                  |                |           |
| created_at    | timestamp with time zone |                |           |
| country       | character varying (10)   |                |           |

## iot_raw_history

| column_name       | data_type                 | related_keys   | remarks   |
|:------------------|:--------------------------|:---------------|:----------|
| uuid              | character varying (64)    |                |           |
| content           | character varying (65535) |                |           |
| upstream          | boolean                   |                |           |
| created_at        | timestamp with time zone  |                |           |
| device_time       | timestamp with time zone  |                |           |
| formatted_content | character varying (65535) |                |           |
| name              | character varying (50)    |                |           |
| imei              | character varying (64)    |                |           |
| country           | character varying (10)    |                |           |

## location_record_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| user_id       | bigint                   |                |           |
| created_at    | timestamp with time zone |                |           |
| latitude      | numeric                  |                |           |
| longitude     | numeric                  |                |           |
| type          | character varying (50)   |                |           |
| device_id     | character varying (128)  |                |           |
| country       | character varying (10)   |                |           |

## mqtt_dbu_history

| column_name                | data_type                | related_keys   | remarks   |
|:---------------------------|:-------------------------|:---------------|:----------|
| uuid                       | character varying (64)   |                |           |
| imei                       | character varying (32)   |                |           |
| device_time                | timestamp with time zone |                |           |
| sn                         | character varying (32)   |                |           |
| hw                         | character varying (32)   |                |           |
| sw                         | character varying (32)   |                |           |
| created_at                 | timestamp with time zone |                |           |
| working_status             | boolean                  |                |           |
| screen_on                  | boolean                  |                |           |
| headlight_on               | boolean                  |                |           |
| bell_on                    | boolean                  |                |           |
| braking                    | smallint                 |                |           |
| accelerate                 | smallint                 |                |           |
| receive_success_bytes      | integer                  |                |           |
| receive_failure_bytes      | integer                  |                |           |
| send_success_bytes         | integer                  |                |           |
| send_failure_bytes         | integer                  |                |           |
| receive_send_total_failure | integer                  |                |           |
| accel_fault                | boolean                  |                |           |
| brake_fault                | boolean                  |                |           |
| country                    | character varying (10)   |                |           |
| gear                       | smallint                 |                |           |

## mqtt_error_report_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| imei          | character varying (32)   |                |           |
| name          | character varying (35)   |                |           |
| level         | character varying (30)   |                |           |
| status        | character varying (15)   |                |           |
| value         | character varying (100)  |                |           |
| device_time   | timestamp with time zone |                |           |
| country       | character varying (10)   |                |           |
| created_at    | timestamp with time zone |                |           |

## mqtt_iot_history

| column_name        | data_type                | related_keys   | remarks   |
|:-------------------|:-------------------------|:---------------|:----------|
| uuid               | character varying (64)   |                |           |
| imei               | character varying (32)   |                |           |
| created_at         | timestamp with time zone |                |           |
| device_time        | timestamp with time zone |                |           |
| sn                 | character varying (32)   |                |           |
| hw                 | character varying (32)   |                |           |
| sw                 | character varying (32)   |                |           |
| working_status     | boolean                  |                |           |
| up_time_seconds    | integer                  |                |           |
| temperature        | numeric                  |                |           |
| water_leak         | boolean                  |                |           |
| flow               | integer                  |                |           |
| baro               | integer                  |                |           |
| mpu_status         | boolean                  |                |           |
| topple             | boolean                  |                |           |
| total_mile         | integer                  |                |           |
| ride_mile          | integer                  |                |           |
| dangerous_driving  | boolean                  |                |           |
| scu_vs             | boolean                  |                |           |
| scu_vc             | boolean                  |                |           |
| bat_vs             | boolean                  |                |           |
| bat_vc             | boolean                  |                |           |
| dbu_vs             | boolean                  |                |           |
| dbu_vc             | boolean                  |                |           |
| hlu_vs             | boolean                  |                |           |
| hlu_vc             | boolean                  |                |           |
| lte_vs             | boolean                  |                |           |
| lte_vc             | boolean                  |                |           |
| low_power_mode     | boolean                  |                |           |
| vbat               | integer                  |                |           |
| vin                | integer                  |                |           |
| v18                | integer                  |                |           |
| v50                | integer                  |                |           |
| v90                | integer                  |                |           |
| unlocked           | boolean                  |                |           |
| tandem_riding      | boolean                  |                |           |
| underage_riding    | boolean                  |                |           |
| weight_index_array | character varying (1000) |                |           |
| country            | character varying (10)   |                |           |

## mqtt_mpu_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| imei          | character varying (32)   |                |           |
| created_at    | timestamp with time zone |                |           |
| device_time   | timestamp with time zone |                |           |
| ts            | integer                  |                |           |
| gyro1         | numeric                  |                |           |
| gyro2         | numeric                  |                |           |
| gyro3         | numeric                  |                |           |
| accelerate1   | numeric                  |                |           |
| accelerate2   | numeric                  |                |           |
| accelerate3   | numeric                  |                |           |
| yaw_angle     | numeric                  |                |           |
| pitch_angle   | numeric                  |                |           |
| roll_angle    | numeric                  |                |           |
| country       | character varying (10)   |                |           |

## mqtt_position_history

| column_name      | data_type                | related_keys   | remarks   |
|:-----------------|:-------------------------|:---------------|:----------|
| uuid             | character varying (64)   |                |           |
| imei             | character varying (32)   |                |           |
| created_at       | timestamp with time zone |                |           |
| latitude         | numeric                  |                |           |
| longitude        | numeric                  |                |           |
| altitude         | numeric                  |                |           |
| course           | numeric                  |                |           |
| device_time      | timestamp with time zone |                |           |
| visible_st_num   | smallint                 |                |           |
| using_st_num     | smallint                 |                |           |
| inview_st_sn     | character varying (128)  |                |           |
| inview_st_signal | character varying (128)  |                |           |
| status           | smallint                 |                |           |
| mode             | smallint                 |                |           |
| work_status      | smallint                 |                |           |
| country          | character varying (10)   |                |           |
| vdop             | numeric                  |                |           |
| hdop             | numeric                  |                |           |
| speed            | numeric                  |                |           |
| pdop             | numeric                  |                |           |

## mqtt_scu_history

| column_name                | data_type                | related_keys   | remarks   |
|:---------------------------|:-------------------------|:---------------|:----------|
| uuid                       | character varying (64)   |                |           |
| gps_id                     | bigint                   |                |           |
| imei                       | character varying (32)   |                |           |
| created_at                 | timestamp with time zone |                |           |
| device_time                | timestamp with time zone |                |           |
| hw                         | character varying (32)   |                |           |
| sw                         | character varying (32)   |                |           |
| working_status             | boolean                  |                |           |
| voltage                    | integer                  |                |           |
| current                    | integer                  |                |           |
| speed                      | numeric                  |                |           |
| unlocked                   | boolean                  |                |           |
| speed_limit1               | integer                  |                |           |
| speed_limit2               | integer                  |                |           |
| current_limit              | integer                  |                |           |
| gear                       | smallint                 |                |           |
| controller_failure         | boolean                  |                |           |
| low_voltage                | boolean                  |                |           |
| over_current               | boolean                  |                |           |
| mhall_failure              | boolean                  |                |           |
| mstall_failure             | boolean                  |                |           |
| mploss_failure             | boolean                  |                |           |
| battery_network_failure    | boolean                  |                |           |
| battery_lock_failure       | boolean                  |                |           |
| battery_lock_unlocked      | boolean                  |                |           |
| receive_success_bytes      | integer                  |                |           |
| receive_failure_bytes      | integer                  |                |           |
| send_success_bytes         | integer                  |                |           |
| send_failure_bytes         | integer                  |                |           |
| receive_send_total_failure | integer                  |                |           |
| country                    | character varying (10)   |                |           |
| sn                         | character varying (64)   |                |           |

## n3_status_history

| column_name             | data_type                | related_keys   | remarks   |
|:------------------------|:-------------------------|:---------------|:----------|
| uuid                    | character varying (64)   |                |           |
| electric_hall           | boolean                  |                |           |
| a_phase_current         | boolean                  |                |           |
| b_phase_current         | boolean                  |                |           |
| c_phase_current         | boolean                  |                |           |
| dc_current              | boolean                  |                |           |
| accelerator             | boolean                  |                |           |
| dashbaord_communication | boolean                  |                |           |
| bms_communication       | boolean                  |                |           |
| brake                   | boolean                  |                |           |
| iot_communication       | boolean                  |                |           |
| ble_communication       | boolean                  |                |           |
| low_battery             | boolean                  |                |           |
| low_battery_protection  | boolean                  |                |           |
| topple                  | boolean                  |                |           |
| lock_status             | character varying (32)   |                |           |
| battery_percentage      | double precision         |                |           |
| imei                    | character varying (32)   |                |           |
| created_at              | timestamp with time zone |                |           |
| single_range            | double precision         |                |           |
| speed                   | double precision         |                |           |
| voltage                 | double precision         |                |           |
| electricity             | double precision         |                |           |
| helmet_attached         | boolean                  |                |           |
| country                 | character varying (10)   |                |           |

## okai_ecu_error_history

| column_name                    | data_type                | related_keys   | remarks   |
|:-------------------------------|:-------------------------|:---------------|:----------|
| uuid                           | character varying (64)   |                |           |
| imei                           | character varying (32)   |                |           |
| head_lamp_fault                | boolean                  |                |           |
| meter_firmware_lost            | boolean                  |                |           |
| turning_handle_fault           | boolean                  |                |           |
| turning_handle_not_returned    | boolean                  |                |           |
| left_brake_fault               | boolean                  |                |           |
| right_brake_fault              | boolean                  |                |           |
| left_brake_not_returned        | boolean                  |                |           |
| right_brake_not_returned       | boolean                  |                |           |
| ecu_heartbeat_error            | boolean                  |                |           |
| bms_heartbeat_error            | boolean                  |                |           |
| meter_heartbeat_error          | boolean                  |                |           |
| charge_over_temp               | boolean                  |                |           |
| discharge_over_heat            | boolean                  |                |           |
| charge_under_temp              | boolean                  |                |           |
| discharge_under_temp           | boolean                  |                |           |
| mos_over_heat                  | boolean                  |                |           |
| other_over_heat                | boolean                  |                |           |
| pre_discharge_error            | boolean                  |                |           |
| pre_charge_error               | boolean                  |                |           |
| under_voltage_first_protect    | boolean                  |                |           |
| under_voltage_second_protect   | boolean                  |                |           |
| over_voltage_first_protect     | boolean                  |                |           |
| over_voltage_second_protect    | boolean                  |                |           |
| over_current_first_protection  | boolean                  |                |           |
| over_current_second_protection | boolean                  |                |           |
| over_current_third_protection  | boolean                  |                |           |
| over_current_fourth_protection | boolean                  |                |           |
| temp_sensor_damage             | boolean                  |                |           |
| large_temp_diff                | boolean                  |                |           |
| charge_fuse_broken             | boolean                  |                |           |
| discharge_fuse_broken          | boolean                  |                |           |
| cell_imbalance                 | boolean                  |                |           |
| cell_drop                      | boolean                  |                |           |
| charge_over_current            | boolean                  |                |           |
| protect_chip_failed            | boolean                  |                |           |
| device_time                    | timestamp with time zone |                |           |
| created_at                     | timestamp with time zone |                |           |
| country                        | character varying (10)   |                |           |

## okai_position_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| imei          | character varying (32)   |                |           |
| mcc           | character varying (50)   |                |           |
| mnc           | character varying (50)   |                |           |
| lac           | character varying (50)   |                |           |
| cid           | character varying (50)   |                |           |
| accuracy      | smallint                 |                |           |
| speed         | double precision         |                |           |
| course        | integer                  |                |           |
| altitude      | double precision         |                |           |
| latitude      | double precision         |                |           |
| longitude     | double precision         |                |           |
| device_time   | timestamp with time zone |                |           |
| created_at    | timestamp with time zone |                |           |
| country       | character varying (10)   |                |           |

## okai_status_history

| column_name               | data_type                | related_keys   | remarks   |
|:--------------------------|:-------------------------|:---------------|:----------|
| uuid                      | character varying (64)   |                |           |
| imei                      | character varying (32)   |                |           |
| network_type              | smallint                 |                |           |
| power_supply              | smallint                 |                |           |
| main_power_voltage        | double precision         |                |           |
| backup_battery_voltage    | double precision         |                |           |
| backup_battery_percentage | smallint                 |                |           |
| main_battery_percentage   | smallint                 |                |           |
| ecu_error_type            | smallint                 |                |           |
| alive                     | boolean                  |                |           |
| ecu_locked                | boolean                  |                |           |
| speed                     | double precision         |                |           |
| current_mileage           | double precision         |                |           |
| remaining_mileage         | double precision         |                |           |
| total_mileage             | double precision         |                |           |
| head_light_on             | boolean                  |                |           |
| rear_light_on             | boolean                  |                |           |
| ecu_sw                    | character varying (16)   |                |           |
| ecu_hw                    | character varying (16)   |                |           |
| display_sw                | character varying (16)   |                |           |
| display_hw                | character varying (16)   |                |           |
| bms_sw                    | character varying (16)   |                |           |
| bms_hw                    | character varying (16)   |                |           |
| bell_btn_pressed          | boolean                  |                |           |
| charging                  | boolean                  |                |           |
| mechanical_locked         | boolean                  |                |           |
| mechanical_door_closed    | boolean                  |                |           |
| ride_time                 | integer                  |                |           |
| battery_heating_plate     | character varying (50)   |                |           |
| mos_charging              | boolean                  |                |           |
| mos_discharging           | boolean                  |                |           |
| battery_health_status     | smallint                 |                |           |
| maximum_temperature       | smallint                 |                |           |
| minimum_temperature       | smallint                 |                |           |
| mos_temperature           | smallint                 |                |           |
| other_temperature         | smallint                 |                |           |
| current                   | integer                  |                |           |
| created_at                | timestamp with time zone |                |           |
| device_time               | timestamp with time zone |                |           |
| country                   | character varying (10)   |                |           |

## position_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| uuid          | character varying (64)      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| course        | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| imei          | character varying (32)      |                |           |
| satellite     | integer                     |                |           |
| speed         | double precision            |                |           |
| device_time   | timestamp without time zone |                |           |
| positioned    | boolean                     |                |           |
| acc           | boolean                     |                |           |
| mcc           | integer                     |                |           |
| mnc           | integer                     |                |           |
| lac           | integer                     |                |           |
| cid           | bigint                      |                |           |
| odometer      | bigint                      |                |           |
| country       | character varying (10)      |                |           |

## staging_battery_info_record_history

| column_name                   | data_type                | related_keys   | remarks   |
|:------------------------------|:-------------------------|:---------------|:----------|
| uuid                          | character varying (64)   |                |           |
| imei                          | character varying (32)   |                |           |
| milli_voltage                 | integer                  |                |           |
| milli_current                 | integer                  |                |           |
| remaining_capacity            | integer                  |                |           |
| full_capacity                 | integer                  |                |           |
| work_cycle                    | integer                  |                |           |
| voltage1                      | integer                  |                |           |
| voltage2                      | integer                  |                |           |
| voltage3                      | integer                  |                |           |
| voltage4                      | integer                  |                |           |
| voltage5                      | integer                  |                |           |
| voltage6                      | integer                  |                |           |
| voltage7                      | integer                  |                |           |
| voltage8                      | integer                  |                |           |
| voltage9                      | integer                  |                |           |
| voltage10                     | integer                  |                |           |
| battery_no                    | character varying (64)   |                |           |
| undervoltage_protect          | boolean                  |                |           |
| powerlow_warn                 | boolean                  |                |           |
| overvoltage_protect           | boolean                  |                |           |
| overvoltage_warn              | boolean                  |                |           |
| discharge_overcurrent_protect | boolean                  |                |           |
| charge_overcurrent_protect    | boolean                  |                |           |
| discharging                   | boolean                  |                |           |
| charging                      | boolean                  |                |           |
| discharge_mos                 | boolean                  |                |           |
| charge_mos                    | boolean                  |                |           |
| shortout                      | boolean                  |                |           |
| charger_connected             | boolean                  |                |           |
| charge_undertemp_protect      | boolean                  |                |           |
| charge_undertemp_warn         | boolean                  |                |           |
| charge_overtemp_protect       | boolean                  |                |           |
| charge_overtemp_warn          | boolean                  |                |           |
| discharge_undertemp_protect   | boolean                  |                |           |
| discharge_lowtemp_warn        | boolean                  |                |           |
| discharge_overtemp_protect    | boolean                  |                |           |
| discharge_overtemp_warn       | boolean                  |                |           |
| temperature1                  | integer                  |                |           |
| temperature2                  | integer                  |                |           |
| remaining_percentage          | integer                  |                |           |
| bms_hardware_version          | integer                  |                |           |
| bms_software_version          | integer                  |                |           |
| created_at                    | timestamp with time zone |                |           |
| device_time                   | timestamp with time zone |                |           |
| working_status                | boolean                  |                |           |
| country                       | character varying (10)   |                |           |

## staging_command_log_history

| column_name   | data_type                 | related_keys   | remarks   |
|:--------------|:--------------------------|:---------------|:----------|
| uuid          | character varying (64)    |                |           |
| command       | character varying (65535) |                |           |
| imei          | character varying (32)    |                |           |
| created_at    | timestamp with time zone  |                |           |
| country       | character varying (10)    |                |           |

## staging_command_response_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| message       | character varying (1500) |                |           |
| imei          | character varying (32)   |                |           |
| device_time   | timestamp with time zone |                |           |
| created_at    | timestamp with time zone |                |           |
| country       | character varying (10)   |                |           |

## staging_event_record_history

| column_name          | data_type                | related_keys   | remarks   |
|:---------------------|:-------------------------|:---------------|:----------|
| uuid                 | character varying (64)   |                |           |
| user_id              | bigint                   |                |           |
| device_id            | character varying (64)   |                |           |
| device_time          | timestamp with time zone |                |           |
| trip_id              | bigint                   |                |           |
| event                | character varying (64)   |                |           |
| source               | character varying (32)   |                |           |
| created_at           | timestamp with time zone |                |           |
| readable_description | character varying (1000) |                |           |
| comment              | character varying (1000) |                |           |
| country              | character varying (10)   |                |           |
| content              | character varying (2048) |                |           |

## staging_iot_raw_history

| column_name       | data_type                 | related_keys   | remarks   |
|:------------------|:--------------------------|:---------------|:----------|
| uuid              | character varying (64)    |                |           |
| content           | character varying (65535) |                |           |
| upstream          | boolean                   |                |           |
| created_at        | timestamp with time zone  |                |           |
| device_time       | timestamp with time zone  |                |           |
| formatted_content | character varying (65535) |                |           |
| name              | character varying (50)    |                |           |
| imei              | character varying (64)    |                |           |
| country           | character varying (10)    |                |           |

## staging_location_record_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| user_id       | bigint                   |                |           |
| created_at    | timestamp with time zone |                |           |
| latitude      | numeric                  |                |           |
| longitude     | numeric                  |                |           |
| type          | character varying (50)   |                |           |
| device_id     | character varying (128)  |                |           |
| country       | character varying (10)   |                |           |

## staging_mqtt_dbu_history

| column_name                | data_type                | related_keys   | remarks   |
|:---------------------------|:-------------------------|:---------------|:----------|
| uuid                       | character varying (64)   |                |           |
| imei                       | character varying (32)   |                |           |
| created_at                 | timestamp with time zone |                |           |
| device_time                | timestamp with time zone |                |           |
| sn                         | character varying (32)   |                |           |
| hw                         | character varying (32)   |                |           |
| sw                         | character varying (32)   |                |           |
| gear                       | smallint                 |                |           |
| working_status             | boolean                  |                |           |
| screen_on                  | boolean                  |                |           |
| headlight_on               | boolean                  |                |           |
| bell_on                    | boolean                  |                |           |
| braking                    | smallint                 |                |           |
| accelerate                 | smallint                 |                |           |
| receive_success_bytes      | integer                  |                |           |
| receive_failure_bytes      | integer                  |                |           |
| send_success_bytes         | integer                  |                |           |
| send_failure_bytes         | integer                  |                |           |
| receive_send_total_failure | integer                  |                |           |
| accel_fault                | boolean                  |                |           |
| brake_fault                | boolean                  |                |           |
| country                    | character varying (10)   |                |           |

## staging_mqtt_error_report_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| imei          | character varying (32)   |                |           |
| name          | character varying (35)   |                |           |
| level         | character varying (30)   |                |           |
| status        | character varying (15)   |                |           |
| value         | character varying (100)  |                |           |
| device_time   | timestamp with time zone |                |           |
| created_at    | timestamp with time zone |                |           |
| country       | character varying (10)   |                |           |

## staging_mqtt_iot_history

| column_name        | data_type                | related_keys   | remarks   |
|:-------------------|:-------------------------|:---------------|:----------|
| uuid               | character varying (64)   |                |           |
| imei               | character varying (32)   |                |           |
| created_at         | timestamp with time zone |                |           |
| device_time        | timestamp with time zone |                |           |
| sn                 | character varying (32)   |                |           |
| hw                 | character varying (32)   |                |           |
| sw                 | character varying (32)   |                |           |
| working_status     | boolean                  |                |           |
| up_time_seconds    | integer                  |                |           |
| temperature        | numeric                  |                |           |
| water_leak         | boolean                  |                |           |
| flow               | integer                  |                |           |
| baro               | integer                  |                |           |
| mpu_status         | boolean                  |                |           |
| topple             | boolean                  |                |           |
| total_mile         | integer                  |                |           |
| ride_mile          | integer                  |                |           |
| dangerous_driving  | boolean                  |                |           |
| scu_vs             | boolean                  |                |           |
| scu_vc             | boolean                  |                |           |
| bat_vs             | boolean                  |                |           |
| bat_vc             | boolean                  |                |           |
| dbu_vs             | boolean                  |                |           |
| dbu_vc             | boolean                  |                |           |
| hlu_vs             | boolean                  |                |           |
| hlu_vc             | boolean                  |                |           |
| lte_vs             | boolean                  |                |           |
| lte_vc             | boolean                  |                |           |
| low_power_mode     | boolean                  |                |           |
| vbat               | integer                  |                |           |
| vin                | integer                  |                |           |
| v18                | integer                  |                |           |
| v50                | integer                  |                |           |
| v90                | integer                  |                |           |
| unlocked           | boolean                  |                |           |
| tandem_riding      | boolean                  |                |           |
| underage_riding    | boolean                  |                |           |
| weight_index_array | character varying (1000) |                |           |
| country            | character varying (10)   |                |           |

## staging_mqtt_mpu_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| imei          | character varying (32)   |                |           |
| created_at    | timestamp with time zone |                |           |
| device_time   | timestamp with time zone |                |           |
| ts            | integer                  |                |           |
| gyro1         | numeric                  |                |           |
| gyro2         | numeric                  |                |           |
| gyro3         | numeric                  |                |           |
| accelerate1   | numeric                  |                |           |
| accelerate2   | numeric                  |                |           |
| accelerate3   | numeric                  |                |           |
| yaw_angle     | numeric                  |                |           |
| pitch_angle   | numeric                  |                |           |
| roll_angle    | numeric                  |                |           |
| country       | character varying (10)   |                |           |

## staging_mqtt_scu_history

| column_name                | data_type                | related_keys   | remarks   |
|:---------------------------|:-------------------------|:---------------|:----------|
| uuid                       | character varying (64)   |                |           |
| gps_id                     | bigint                   |                |           |
| imei                       | character varying (32)   |                |           |
| created_at                 | timestamp with time zone |                |           |
| device_time                | timestamp with time zone |                |           |
| hw                         | character varying (32)   |                |           |
| sw                         | character varying (32)   |                |           |
| working_status             | boolean                  |                |           |
| voltage                    | integer                  |                |           |
| current                    | integer                  |                |           |
| speed                      | numeric                  |                |           |
| unlocked                   | boolean                  |                |           |
| speed_limit1               | integer                  |                |           |
| speed_limit2               | integer                  |                |           |
| current_limit              | integer                  |                |           |
| gear                       | smallint                 |                |           |
| controller_failure         | boolean                  |                |           |
| low_voltage                | boolean                  |                |           |
| over_current               | boolean                  |                |           |
| mhall_failure              | boolean                  |                |           |
| mstall_failure             | boolean                  |                |           |
| mploss_failure             | boolean                  |                |           |
| battery_network_failure    | boolean                  |                |           |
| battery_lock_failure       | boolean                  |                |           |
| battery_lock_unlocked      | boolean                  |                |           |
| receive_success_bytes      | integer                  |                |           |
| receive_failure_bytes      | integer                  |                |           |
| send_success_bytes         | integer                  |                |           |
| send_failure_bytes         | integer                  |                |           |
| receive_send_total_failure | integer                  |                |           |
| country                    | character varying (10)   |                |           |
| sn                         | character varying (64)   |                |           |

## staging_n3_status_history

| column_name             | data_type                | related_keys   | remarks   |
|:------------------------|:-------------------------|:---------------|:----------|
| uuid                    | character varying (64)   |                |           |
| electric_hall           | boolean                  |                |           |
| a_phase_current         | boolean                  |                |           |
| b_phase_current         | boolean                  |                |           |
| c_phase_current         | boolean                  |                |           |
| dc_current              | boolean                  |                |           |
| accelerator             | boolean                  |                |           |
| dashbaord_communication | boolean                  |                |           |
| bms_communication       | boolean                  |                |           |
| brake                   | boolean                  |                |           |
| iot_communication       | boolean                  |                |           |
| ble_communication       | boolean                  |                |           |
| low_battery             | boolean                  |                |           |
| low_battery_protection  | boolean                  |                |           |
| topple                  | boolean                  |                |           |
| lock_status             | character varying (32)   |                |           |
| battery_percentage      | double precision         |                |           |
| imei                    | character varying (32)   |                |           |
| created_at              | timestamp with time zone |                |           |
| single_range            | double precision         |                |           |
| speed                   | double precision         |                |           |
| voltage                 | double precision         |                |           |
| electricity             | double precision         |                |           |
| helmet_attached         | boolean                  |                |           |
| country                 | character varying (10)   |                |           |

## staging_okai_ecu_error_history

| column_name                    | data_type                | related_keys   | remarks   |
|:-------------------------------|:-------------------------|:---------------|:----------|
| uuid                           | character varying (64)   |                |           |
| imei                           | character varying (32)   |                |           |
| head_lamp_fault                | boolean                  |                |           |
| meter_firmware_lost            | boolean                  |                |           |
| turning_handle_fault           | boolean                  |                |           |
| turning_handle_not_returned    | boolean                  |                |           |
| left_brake_fault               | boolean                  |                |           |
| right_brake_fault              | boolean                  |                |           |
| left_brake_not_returned        | boolean                  |                |           |
| right_brake_not_returned       | boolean                  |                |           |
| ecu_heartbeat_error            | boolean                  |                |           |
| bms_heartbeat_error            | boolean                  |                |           |
| meter_heartbeat_error          | boolean                  |                |           |
| charge_over_temp               | boolean                  |                |           |
| discharge_over_heat            | boolean                  |                |           |
| charge_under_temp              | boolean                  |                |           |
| discharge_under_temp           | boolean                  |                |           |
| mos_over_heat                  | boolean                  |                |           |
| other_over_heat                | boolean                  |                |           |
| pre_discharge_error            | boolean                  |                |           |
| pre_charge_error               | boolean                  |                |           |
| under_voltage_first_protect    | boolean                  |                |           |
| under_voltage_second_protect   | boolean                  |                |           |
| over_voltage_first_protect     | boolean                  |                |           |
| over_voltage_second_protect    | boolean                  |                |           |
| over_current_first_protection  | boolean                  |                |           |
| over_current_second_protection | boolean                  |                |           |
| over_current_third_protection  | boolean                  |                |           |
| over_current_fourth_protection | boolean                  |                |           |
| temp_sensor_damage             | boolean                  |                |           |
| large_temp_diff                | boolean                  |                |           |
| charge_fuse_broken             | boolean                  |                |           |
| discharge_fuse_broken          | boolean                  |                |           |
| cell_imbalance                 | boolean                  |                |           |
| cell_drop                      | boolean                  |                |           |
| charge_over_current            | boolean                  |                |           |
| protect_chip_failed            | boolean                  |                |           |
| device_time                    | timestamp with time zone |                |           |
| created_at                     | timestamp with time zone |                |           |
| country                        | character varying (10)   |                |           |

## staging_okai_position_history

| column_name   | data_type                | related_keys   | remarks   |
|:--------------|:-------------------------|:---------------|:----------|
| uuid          | character varying (64)   |                |           |
| imei          | character varying (32)   |                |           |
| mcc           | character varying (50)   |                |           |
| mnc           | character varying (50)   |                |           |
| lac           | character varying (50)   |                |           |
| cid           | character varying (50)   |                |           |
| accuracy      | smallint                 |                |           |
| speed         | double precision         |                |           |
| course        | integer                  |                |           |
| altitude      | double precision         |                |           |
| latitude      | double precision         |                |           |
| longitude     | double precision         |                |           |
| device_time   | timestamp with time zone |                |           |
| created_at    | timestamp with time zone |                |           |
| country       | character varying (10)   |                |           |

## staging_okai_status_history

| column_name               | data_type                | related_keys   | remarks   |
|:--------------------------|:-------------------------|:---------------|:----------|
| uuid                      | character varying (64)   |                |           |
| imei                      | character varying (32)   |                |           |
| network_type              | smallint                 |                |           |
| power_supply              | smallint                 |                |           |
| main_power_voltage        | double precision         |                |           |
| backup_battery_voltage    | double precision         |                |           |
| backup_battery_percentage | smallint                 |                |           |
| main_battery_percentage   | smallint                 |                |           |
| ecu_error_type            | smallint                 |                |           |
| alive                     | boolean                  |                |           |
| ecu_locked                | boolean                  |                |           |
| speed                     | double precision         |                |           |
| current_mileage           | double precision         |                |           |
| remaining_mileage         | double precision         |                |           |
| total_mileage             | double precision         |                |           |
| head_light_on             | boolean                  |                |           |
| rear_light_on             | boolean                  |                |           |
| ecu_sw                    | character varying (16)   |                |           |
| ecu_hw                    | character varying (16)   |                |           |
| display_sw                | character varying (16)   |                |           |
| display_hw                | character varying (16)   |                |           |
| bms_sw                    | character varying (16)   |                |           |
| bms_hw                    | character varying (16)   |                |           |
| bell_btn_pressed          | boolean                  |                |           |
| charging                  | boolean                  |                |           |
| mechanical_locked         | boolean                  |                |           |
| mechanical_door_closed    | boolean                  |                |           |
| ride_time                 | integer                  |                |           |
| battery_heating_plate     | character varying (50)   |                |           |
| mos_charging              | boolean                  |                |           |
| mos_discharging           | boolean                  |                |           |
| battery_health_status     | smallint                 |                |           |
| maximum_temperature       | smallint                 |                |           |
| minimum_temperature       | smallint                 |                |           |
| mos_temperature           | smallint                 |                |           |
| other_temperature         | smallint                 |                |           |
| current                   | integer                  |                |           |
| created_at                | timestamp with time zone |                |           |
| device_time               | timestamp with time zone |                |           |
| country                   | character varying (10)   |                |           |

## staging_position_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| uuid          | character varying (64)      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| course        | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| imei          | character varying (32)      |                |           |
| satellite     | integer                     |                |           |
| speed         | double precision            |                |           |
| device_time   | timestamp without time zone |                |           |
| positioned    | boolean                     |                |           |
| acc           | boolean                     |                |           |
| mcc           | integer                     |                |           |
| mnc           | integer                     |                |           |
| lac           | integer                     |                |           |
| cid           | bigint                      |                |           |
| odometer      | bigint                      |                |           |
| country       | character varying (10)      |                |           |

## staging_user_gps_tracing_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| uuid          | character varying (64)      |                |           |
| user_id       | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| created_at    | timestamp without time zone |                |           |
| accuracy      | numeric                     |                |           |
| fixed_time    | timestamp without time zone |                |           |
| speed         | numeric                     |                |           |
| country       | character varying (10)      |                |           |

## user_gps_tracing_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| user_id       | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| created_at    | timestamp without time zone |                |           |
| accuracy      | numeric                     |                |           |
| fixed_time    | timestamp without time zone |                |           |
| speed         | numeric                     |                |           |
| uuid          | character varying (64)      |                |           |
| country       | character varying (10)      |                |           |

# neuron_dev_rds_au_big_data



## n3_status_history

| column_name             | data_type                   | related_keys   | remarks   |
|:------------------------|:----------------------------|:---------------|:----------|
| uuid                    | character varying (32)      |                |           |
| electric_hall           | smallint                    |                |           |
| a_phase_current         | smallint                    |                |           |
| b_phase_current         | smallint                    |                |           |
| c_phase_current         | smallint                    |                |           |
| dc_current              | smallint                    |                |           |
| accelerator             | smallint                    |                |           |
| dashbaord_communication | smallint                    |                |           |
| bms_communication       | smallint                    |                |           |
| brake                   | smallint                    |                |           |
| iot_communication       | smallint                    |                |           |
| ble_communication       | smallint                    |                |           |
| low_battery             | smallint                    |                |           |
| low_battery_protection  | smallint                    |                |           |
| topple                  | smallint                    |                |           |
| lock_status             | character varying (32)      |                |           |
| battery_percentage      | double precision            |                |           |
| gps_id                  | bigint                      |                |           |
| created_at              | timestamp without time zone |                |           |
| single_range            | double precision            |                |           |
| speed                   | double precision            |                |           |
| voltage                 | double precision            |                |           |
| electricity             | double precision            |                |           |
| helmet_attached         | smallint                    |                |           |
| imei                    | character varying (32)      |                |           |

## position_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| uuid          | character varying (32)      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| course        | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| gps_id        | bigint                      |                |           |
| satellite     | integer                     |                |           |
| speed         | double precision            |                |           |
| device_time   | timestamp without time zone |                |           |
| positioned    | smallint                    |                |           |
| acc           | smallint                    |                |           |
| mcc           | integer                     |                |           |
| mnc           | integer                     |                |           |
| lac           | integer                     |                |           |
| cid           | bigint                      |                |           |
| odometer      | bigint                      |                |           |
| imei          | character varying (32)      |                |           |

## staging_n3_status_history

| column_name             | data_type                   | related_keys   | remarks   |
|:------------------------|:----------------------------|:---------------|:----------|
| uuid                    | character varying (32)      |                |           |
| electric_hall           | smallint                    |                |           |
| a_phase_current         | smallint                    |                |           |
| b_phase_current         | smallint                    |                |           |
| c_phase_current         | smallint                    |                |           |
| dc_current              | smallint                    |                |           |
| accelerator             | smallint                    |                |           |
| dashbaord_communication | smallint                    |                |           |
| bms_communication       | smallint                    |                |           |
| brake                   | smallint                    |                |           |
| iot_communication       | smallint                    |                |           |
| ble_communication       | smallint                    |                |           |
| low_battery             | smallint                    |                |           |
| low_battery_protection  | smallint                    |                |           |
| topple                  | smallint                    |                |           |
| lock_status             | character varying (32)      |                |           |
| battery_percentage      | double precision            |                |           |
| gps_id                  | bigint                      |                |           |
| imei                    | character varying (32)      |                |           |
| created_at              | timestamp without time zone |                |           |
| single_range            | double precision            |                |           |
| speed                   | double precision            |                |           |
| voltage                 | double precision            |                |           |
| electricity             | double precision            |                |           |
| helmet_attached         | smallint                    |                |           |

## staging_position_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| uuid          | character varying (32)      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| course        | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| gps_id        | bigint                      |                |           |
| imei          | character varying (32)      |                |           |
| satellite     | integer                     |                |           |
| speed         | double precision            |                |           |
| device_time   | timestamp without time zone |                |           |
| positioned    | smallint                    |                |           |
| acc           | smallint                    |                |           |
| mcc           | integer                     |                |           |
| mnc           | integer                     |                |           |
| lac           | integer                     |                |           |
| cid           | bigint                      |                |           |
| odometer      | bigint                      |                |           |

# neuron_gb_big_data



## activity_history

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| uuid           | character varying (32)      |                |           |
| gps_id         | bigint                      |                |           |
| type           | character varying (50)      |                |           |
| description    | character varying (200)     |                |           |
| created_at     | timestamp without time zone |                |           |
| last_active_at | timestamp without time zone |                |           |
| imei           | character varying (32)      |                |           |

## battery_info_record_history

| column_name                   | data_type                   | related_keys   | remarks   |
|:------------------------------|:----------------------------|:---------------|:----------|
| gps_id                        | bigint                      |                |           |
| milli_voltage                 | integer                     |                |           |
| milli_current                 | integer                     |                |           |
| remaining_capacity            | integer                     |                |           |
| full_capacity                 | integer                     |                |           |
| work_cycle                    | integer                     |                |           |
| voltage_1                     | integer                     |                |           |
| voltage_2                     | integer                     |                |           |
| voltage_3                     | integer                     |                |           |
| voltage_4                     | integer                     |                |           |
| voltage_5                     | integer                     |                |           |
| voltage_6                     | integer                     |                |           |
| voltage_7                     | integer                     |                |           |
| voltage_8                     | integer                     |                |           |
| voltage_9                     | integer                     |                |           |
| voltage_10                    | integer                     |                |           |
| battery_no                    | character varying (64)      |                |           |
| undervoltage_protect          | smallint                    |                |           |
| powerlow_warn                 | smallint                    |                |           |
| overvoltage_protect           | smallint                    |                |           |
| overvoltage_warn              | smallint                    |                |           |
| discharge_overcurrent_protect | smallint                    |                |           |
| charge_overcurrent_protect    | smallint                    |                |           |
| discharging                   | smallint                    |                |           |
| charging                      | smallint                    |                |           |
| discharge_mos                 | smallint                    |                |           |
| charge_mos                    | smallint                    |                |           |
| shortout                      | smallint                    |                |           |
| charger_connected             | smallint                    |                |           |
| charge_undertemp_protect      | smallint                    |                |           |
| charge_undertemp_warn         | smallint                    |                |           |
| charge_overtemp_protect       | smallint                    |                |           |
| charge_overtemp_warn          | smallint                    |                |           |
| discharge_undertemp_protect   | smallint                    |                |           |
| discharge_lowtemp_warn        | smallint                    |                |           |
| discharge_overtemp_protect    | smallint                    |                |           |
| discharge_overtemp_warn       | smallint                    |                |           |
| temperature_2                 | integer                     |                |           |
| remaining_percentage          | integer                     |                |           |
| bms_hardware_version          | integer                     |                |           |
| bms_software_version          | integer                     |                |           |
| created_at                    | timestamp without time zone |                |           |
| device_time                   | timestamp without time zone |                |           |
| working_status                | smallint                    |                |           |
| imei                          | character varying (32)      |                |           |
| temperature_1                 | integer                     |                |           |
| uuid                          | character varying (64)      |                |           |

## command_log_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| command       | character varying (1024)    |                |           |
| succeed       | smallint                    |                |           |
| gps_id        | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| imei          | character varying (32)      |                |           |
| uuid          | character varying (64)      |                |           |

## command_response_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| gps_id        | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| device_time   | timestamp without time zone |                |           |
| imei          | character varying (32)      |                |           |
| message       | character varying (1500)    |                |           |
| uuid          | character varying (64)      |                |           |

## event_record_history

| column_name          | data_type                   | related_keys   | remarks   |
|:---------------------|:----------------------------|:---------------|:----------|
| user_id              | bigint                      |                |           |
| device_id            | character varying (64)      |                |           |
| device_time          | timestamp without time zone |                |           |
| trip_id              | bigint                      |                |           |
| event                | character varying (64)      |                |           |
| content              | character varying (512)     |                |           |
| source               | character varying (32)      |                |           |
| created_at           | timestamp without time zone |                |           |
| uuid                 | character varying (64)      |                |           |
| readable_description | character varying (1000)    |                |           |
| comment              | character varying (1000)    |                |           |

## gps_brake_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| uuid          | character varying (32)      |                |           |
| gps_id        | bigint                      |                |           |
| brake         | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |
| imei          | character varying (32)      |                |           |

## iot_raw_history

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| imei              | character varying (32)      |                |           |
| upstream          | smallint                    |                |           |
| created_at        | timestamp without time zone |                |           |
| device_time       | timestamp without time zone |                |           |
| name              | character varying (32)      |                |           |
| formatted_content | character varying (4096)    |                |           |
| content           | character varying (65535)   |                |           |
| uuid              | character varying (64)      |                |           |

## location_record_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| user_id       | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| latitude      | double precision            |                |           |
| longitude     | double precision            |                |           |
| type          | character varying (50)      |                |           |
| device_id     | character varying (128)     |                |           |
| uuid          | character varying (64)      |                |           |

## n3_status_history

| column_name             | data_type                   | related_keys   | remarks   |
|:------------------------|:----------------------------|:---------------|:----------|
| electric_hall           | smallint                    |                |           |
| a_phase_current         | smallint                    |                |           |
| b_phase_current         | smallint                    |                |           |
| c_phase_current         | smallint                    |                |           |
| dc_current              | smallint                    |                |           |
| accelerator             | smallint                    |                |           |
| dashbaord_communication | smallint                    |                |           |
| bms_communication       | smallint                    |                |           |
| brake                   | smallint                    |                |           |
| iot_communication       | smallint                    |                |           |
| ble_communication       | smallint                    |                |           |
| low_battery             | smallint                    |                |           |
| low_battery_protection  | smallint                    |                |           |
| topple                  | smallint                    |                |           |
| lock_status             | character varying (32)      |                |           |
| battery_percentage      | double precision            |                |           |
| gps_id                  | bigint                      |                |           |
| created_at              | timestamp without time zone |                |           |
| single_range            | double precision            |                |           |
| speed                   | double precision            |                |           |
| voltage                 | double precision            |                |           |
| electricity             | double precision            |                |           |
| helmet_attached         | smallint                    |                |           |
| imei                    | character varying (32)      |                |           |
| uuid                    | character varying (64)      |                |           |

## position_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| course        | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| gps_id        | bigint                      |                |           |
| satellite     | integer                     |                |           |
| speed         | double precision            |                |           |
| device_time   | timestamp without time zone |                |           |
| positioned    | smallint                    |                |           |
| acc           | smallint                    |                |           |
| mcc           | integer                     |                |           |
| mnc           | integer                     |                |           |
| lac           | integer                     |                |           |
| cid           | bigint                      |                |           |
| odometer      | bigint                      |                |           |
| imei          | character varying (32)      |                |           |
| uuid          | character varying (64)      |                |           |

## staging_activity_history

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| uuid           | character varying (32)      |                |           |
| imei           | character varying (32)      |                |           |
| type           | character varying (50)      |                |           |
| description    | character varying (200)     |                |           |
| created_at     | timestamp without time zone |                |           |
| last_active_at | timestamp without time zone |                |           |

## staging_battery_info_record_history

| column_name                   | data_type                   | related_keys   | remarks   |
|:------------------------------|:----------------------------|:---------------|:----------|
| imei                          | character varying (32)      |                |           |
| milli_voltage                 | integer                     |                |           |
| milli_current                 | integer                     |                |           |
| remaining_capacity            | integer                     |                |           |
| full_capacity                 | integer                     |                |           |
| work_cycle                    | integer                     |                |           |
| voltage_1                     | integer                     |                |           |
| voltage_2                     | integer                     |                |           |
| voltage_3                     | integer                     |                |           |
| voltage_4                     | integer                     |                |           |
| voltage_5                     | integer                     |                |           |
| voltage_6                     | integer                     |                |           |
| voltage_7                     | integer                     |                |           |
| voltage_8                     | integer                     |                |           |
| voltage_9                     | integer                     |                |           |
| voltage_10                    | integer                     |                |           |
| battery_no                    | character varying (64)      |                |           |
| undervoltage_protect          | smallint                    |                |           |
| powerlow_warn                 | smallint                    |                |           |
| overvoltage_protect           | smallint                    |                |           |
| overvoltage_warn              | smallint                    |                |           |
| discharge_overcurrent_protect | smallint                    |                |           |
| charge_overcurrent_protect    | smallint                    |                |           |
| discharging                   | smallint                    |                |           |
| charging                      | smallint                    |                |           |
| discharge_mos                 | smallint                    |                |           |
| charge_mos                    | smallint                    |                |           |
| shortout                      | smallint                    |                |           |
| charger_connected             | smallint                    |                |           |
| charge_undertemp_protect      | smallint                    |                |           |
| charge_undertemp_warn         | smallint                    |                |           |
| charge_overtemp_protect       | smallint                    |                |           |
| charge_overtemp_warn          | smallint                    |                |           |
| discharge_undertemp_protect   | smallint                    |                |           |
| discharge_lowtemp_warn        | smallint                    |                |           |
| discharge_overtemp_protect    | smallint                    |                |           |
| discharge_overtemp_warn       | smallint                    |                |           |
| temperature_1                 | integer                     |                |           |
| temperature_2                 | integer                     |                |           |
| remaining_percentage          | integer                     |                |           |
| bms_hardware_version          | integer                     |                |           |
| bms_software_version          | integer                     |                |           |
| created_at                    | timestamp without time zone |                |           |
| device_time                   | timestamp without time zone |                |           |
| working_status                | smallint                    |                |           |
| uuid                          | character varying (64)      |                |           |

## staging_command_log_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| command       | character varying (1024)    |                |           |
| imei          | character varying (32)      |                |           |
| created_at    | timestamp without time zone |                |           |
| uuid          | character varying (64)      |                |           |

## staging_command_response_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| message       | character varying (1500)    |                |           |
| imei          | character varying (32)      |                |           |
| device_time   | timestamp without time zone |                |           |
| created_at    | timestamp without time zone |                |           |
| uuid          | character varying (64)      |                |           |

## staging_event_record_history

| column_name          | data_type                   | related_keys   | remarks   |
|:---------------------|:----------------------------|:---------------|:----------|
| user_id              | bigint                      |                |           |
| device_id            | character varying (64)      |                |           |
| device_time          | timestamp without time zone |                |           |
| trip_id              | bigint                      |                |           |
| event                | character varying (64)      |                |           |
| content              | character varying (512)     |                |           |
| source               | character varying (32)      |                |           |
| created_at           | timestamp without time zone |                |           |
| uuid                 | character varying (64)      |                |           |
| readable_description | character varying (1000)    |                |           |
| comment              | character varying (1000)    |                |           |

## staging_gps_brake_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| uuid          | character varying (32)      |                |           |
| gps_id        | bigint                      |                |           |
| imei          | character varying (32)      |                |           |
| brake         | smallint                    |                |           |
| created_at    | timestamp without time zone |                |           |

## staging_iot_raw_history

| column_name       | data_type                   | related_keys   | remarks   |
|:------------------|:----------------------------|:---------------|:----------|
| imei              | character varying (32)      |                |           |
| upstream          | smallint                    |                |           |
| created_at        | timestamp without time zone |                |           |
| device_time       | timestamp without time zone |                |           |
| name              | character varying (32)      |                |           |
| formatted_content | character varying (4096)    |                |           |
| content           | character varying (65535)   |                |           |
| uuid              | character varying (64)      |                |           |

## staging_location_record_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| user_id       | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |
| latitude      | double precision            |                |           |
| longitude     | double precision            |                |           |
| type          | character varying (50)      |                |           |
| device_id     | character varying (128)     |                |           |
| uuid          | character varying (64)      |                |           |

## staging_n3_status_history

| column_name             | data_type                   | related_keys   | remarks   |
|:------------------------|:----------------------------|:---------------|:----------|
| electric_hall           | smallint                    |                |           |
| a_phase_current         | smallint                    |                |           |
| b_phase_current         | smallint                    |                |           |
| c_phase_current         | smallint                    |                |           |
| dc_current              | smallint                    |                |           |
| accelerator             | smallint                    |                |           |
| dashbaord_communication | smallint                    |                |           |
| bms_communication       | smallint                    |                |           |
| brake                   | smallint                    |                |           |
| iot_communication       | smallint                    |                |           |
| ble_communication       | smallint                    |                |           |
| low_battery             | smallint                    |                |           |
| low_battery_protection  | smallint                    |                |           |
| topple                  | smallint                    |                |           |
| lock_status             | character varying (32)      |                |           |
| battery_percentage      | double precision            |                |           |
| imei                    | character varying (32)      |                |           |
| created_at              | timestamp without time zone |                |           |
| single_range            | double precision            |                |           |
| speed                   | double precision            |                |           |
| voltage                 | double precision            |                |           |
| electricity             | double precision            |                |           |
| helmet_attached         | smallint                    |                |           |
| uuid                    | character varying (64)      |                |           |

## staging_position_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| course        | double precision            |                |           |
| created_at    | timestamp without time zone |                |           |
| imei          | character varying (32)      |                |           |
| satellite     | integer                     |                |           |
| speed         | double precision            |                |           |
| device_time   | timestamp without time zone |                |           |
| positioned    | smallint                    |                |           |
| acc           | smallint                    |                |           |
| mcc           | integer                     |                |           |
| mnc           | integer                     |                |           |
| lac           | integer                     |                |           |
| cid           | bigint                      |                |           |
| odometer      | bigint                      |                |           |
| uuid          | character varying (64)      |                |           |

## staging_user_gps_tracing_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| user_id       | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| created_at    | timestamp without time zone |                |           |
| accuracy      | numeric                     |                |           |
| fixed_time    | timestamp without time zone |                |           |
| speed         | numeric                     |                |           |
| uuid          | character varying (64)      |                |           |

## user_gps_tracing_history

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| user_id       | bigint                      |                |           |
| trip_id       | bigint                      |                |           |
| latitude      | numeric                     |                |           |
| longitude     | numeric                     |                |           |
| created_at    | timestamp without time zone |                |           |
| accuracy      | numeric                     |                |           |
| fixed_time    | timestamp without time zone |                |           |
| speed         | numeric                     |                |           |
| uuid          | character varying (64)      |                |           |