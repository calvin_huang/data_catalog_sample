[TOC]

## user_city_prediction_version_3

| column_name   | data_type                   | related_keys                                                    |
|:--------------|:----------------------------|:----------------------------------------------------------------|
| city          | character varying (256)     |                                                                 |
| country       | character varying (256)     |                                                                 |
| created_at    | character varying (256)     |                                                                 |
| role_code     | character varying (256)     |                                                                 |
| is_vip        | integer                     |                                                                 |
| user_id       | bigint                      | user_stripe.user_id, user_country.user_id, user_deleted.user_id |
| id            | bigint                      |                                                                 |
| status        | character varying (256)     |                                                                 |
| updated_at    | timestamp without time zone |                                                                 |
| source        | character varying (256)     |                                                                 |
| match_on      | character varying (256)     |                                                                 |
| pred_dist     | double precision            |                                                                 |

## user_country

| column_name   | data_type                   | related_keys                                                                      |
|:--------------|:----------------------------|:----------------------------------------------------------------------------------|
| user_id       | bigint                      | user_stripe.user_id, user_city_prediction_version_3.user_id, user_deleted.user_id |
| role_code     | character varying (96)      |                                                                                   |
| is_vip        | smallint                    |                                                                                   |
| country       | character varying (24)      |                                                                                   |
| city          | character varying (192)     |                                                                                   |
| status        | character varying (96)      |                                                                                   |
| updated_at    | timestamp without time zone |                                                                                   |
| created_at    | timestamp without time zone |                                                                                   |
| id            | bigint                      |                                                                                   |

## user_deleted

| column_name     | data_type                   | related_keys                                                                      |
|:----------------|:----------------------------|:----------------------------------------------------------------------------------|
| user_id         | bigint                      | user_stripe.user_id, user_country.user_id, user_city_prediction_version_3.user_id |
| country         | character varying (24)      |                                                                                   |
| update_date     | timestamp without time zone |                                                                                   |
| segment_job_id  | character varying (256)     |                                                                                   |
| mixpanel_job_id | character varying (256)     |                                                                                   |
| segment_done    | boolean                     |                                                                                   |
| mixpanel_done   | boolean                     |                                                                                   |
| id              | bigint                      |                                                                                   |
| create_date     | timestamp without time zone |                                                                                   |

## user_stripe

| column_name   | data_type                   | related_keys                                                                       |
|:--------------|:----------------------------|:-----------------------------------------------------------------------------------|
| user_id       | bigint                      | user_country.user_id, user_city_prediction_version_3.user_id, user_deleted.user_id |
| customer_id   | character varying (192)     |                                                                                    |
| country       | character varying (24)      |                                                                                    |
| created_at    | timestamp without time zone |                                                                                    |
| platform      | character varying (96)      |                                                                                    |
| account       | character varying (96)      |                                                                                    |
| primary       | smallint                    |                                                                                    |
| updated_at    | timestamp without time zone |                                                                                    |
| id            | bigint                      |                                                                                    |