[TOC]


# neuron_user



## card_block

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| fingerprint   | character varying (192.0)   |                |           |
| account       | character varying (24.0)    |                |           |
| reason        | character varying (192.0)   |                |           |
| comment       | character varying (1536.0)  |                |           |
| blocked       | smallint                    |                |           |
| count         | integer                     |                |           |
| created_at    | timestamp without time zone |                |           |
| created_by    | bigint                      |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |
| source        | character varying (192.0)   |                |           |

## flyway_schema_history

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| installed_rank | integer                     |                |           |
| version        | character varying (150.0)   |                |           |
| description    | character varying (600.0)   |                |           |
| type           | character varying (60.0)    |                |           |
| script         | character varying (3000.0)  |                |           |
| checksum       | integer                     |                |           |
| installed_by   | character varying (300.0)   |                |           |
| installed_on   | timestamp without time zone |                |           |
| execution_time | integer                     |                |           |
| success        | smallint                    |                |           |

## password_update_record

| column_name     | data_type                   | related_keys                                                                                        | remarks   |
|:----------------|:----------------------------|:----------------------------------------------------------------------------------------------------|:----------|
| id              | bigint                      |                                                                                                     |           |
| user_id         | bigint                      | user_role.user_id, user_social.user_id, user_country.user_id, user_mfa.user_id, user_stripe.user_id |           |
| created_at      | timestamp without time zone |                                                                                                     |           |
| created_by      | bigint                      |                                                                                                     |           |
| password_length | smallint                    |                                                                                                     |           |
| password        | character varying (768.0)   |                                                                                                     |           |

## permission

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| name          | character varying (192.0)   |                |           |
| created_at    | timestamp without time zone |                |           |

## role

| column_name    | data_type                   | related_keys   | remarks   |
|:---------------|:----------------------------|:---------------|:----------|
| id             | bigint                      |                |           |
| role_code      | character varying (150.0)   |                |           |
| parent_role_id | bigint                      |                |           |
| updated_at     | timestamp without time zone |                |           |
| updated_by     | bigint                      |                |           |

## role_permission

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| role          | character varying (150.0)   |                |           |
| permission_id | bigint                      |                |           |
| created_at    | timestamp without time zone |                |           |

## role_resource_permission

| column_name   | data_type                   | related_keys   | remarks   |
|:--------------|:----------------------------|:---------------|:----------|
| id            | bigint                      |                |           |
| role_code     | character varying (150.0)   |                |           |
| resource      | character varying (150.0)   |                |           |
| permission    | character varying (1500.0)  |                |           |
| updated_at    | timestamp without time zone |                |           |
| updated_by    | bigint                      |                |           |

## user

| column_name            | data_type                   | related_keys   | remarks   |
|:-----------------------|:----------------------------|:---------------|:----------|
| id                     | bigint                      |                |           |
| username               | character varying (768.0)   |                |           |
| password               | character varying (768.0)   |                |           |
| country_code           | character varying (24.0)    |                |           |
| mobile                 | character varying (96.0)    |                |           |
| email                  | character varying (384.0)   |                |           |
| email_verified         | smallint                    |                |           |
| address                | character varying (768.0)   |                |           |
| full_name              | character varying (384.0)   |                |           |
| id_card                | character varying (192.0)   |                |           |
| avatar                 | character varying (768.0)   |                |           |
| nickname               | character varying (384.0)   |                |           |
| slogan                 | character varying (768.0)   |                |           |
| deleted                | smallint                    |                |           |
| disabled               | smallint                    |                |           |
| created_at             | timestamp without time zone |                |           |
| updated_at             | timestamp without time zone |                |           |
| updated_by             | bigint                      |                |           |
| driver_license         | smallint                    |                |           |
| email_marketing        | smallint                    |                |           |
| notification_marketing | smallint                    |                |           |
| location_marketing     | smallint                    |                |           |
| usage_tracking         | smallint                    |                |           |
| sms_marketing          | smallint                    |                |           |
| city                   | character varying (192.0)   |                |           |
| is_vip                 | smallint                    |                |           |
| country                | character varying (96.0)    |                |           |
| user_protocol          | smallint                    |                |           |
| disable_reason         | character varying (3072.0)  |                |           |

## user_country

| column_name   | data_type                   | related_keys                                                                                                  | remarks   |
|:--------------|:----------------------------|:--------------------------------------------------------------------------------------------------------------|:----------|
| id            | bigint                      |                                                                                                               |           |
| user_id       | bigint                      | user_role.user_id, user_social.user_id, password_update_record.user_id, user_mfa.user_id, user_stripe.user_id |           |
| role_code     | character varying (96.0)    |                                                                                                               |           |
| is_vip        | smallint                    |                                                                                                               |           |
| country       | character varying (24.0)    |                                                                                                               |           |
| city          | character varying (192.0)   |                                                                                                               |           |
| status        | character varying (96.0)    |                                                                                                               |           |
| updated_at    | timestamp without time zone |                                                                                                               |           |
| created_at    | timestamp without time zone |                                                                                                               |           |

## user_mfa

| column_name   | data_type                   | related_keys                                                                                                      | remarks   |
|:--------------|:----------------------------|:------------------------------------------------------------------------------------------------------------------|:----------|
| id            | bigint                      |                                                                                                                   |           |
| user_id       | bigint                      | user_role.user_id, user_social.user_id, user_country.user_id, password_update_record.user_id, user_stripe.user_id |           |
| secret        | character varying (192.0)   |                                                                                                                   |           |
| created_at    | timestamp without time zone |                                                                                                                   |           |

## user_role

| column_name   | data_type                   | related_keys                                                                                                     | remarks   |
|:--------------|:----------------------------|:-----------------------------------------------------------------------------------------------------------------|:----------|
| id            | bigint                      |                                                                                                                  |           |
| user_id       | bigint                      | user_social.user_id, user_country.user_id, password_update_record.user_id, user_mfa.user_id, user_stripe.user_id |           |
| role_code     | character varying (150.0)   |                                                                                                                  |           |
| city          | character varying (60.0)    |                                                                                                                  |           |
| created_at    | timestamp without time zone |                                                                                                                  |           |
| updated_at    | timestamp without time zone |                                                                                                                  |           |
| updated_by    | bigint                      |                                                                                                                  |           |

## user_social

| column_name   | data_type                   | related_keys                                                                                                   | remarks   |
|:--------------|:----------------------------|:---------------------------------------------------------------------------------------------------------------|:----------|
| id            | bigint                      |                                                                                                                |           |
| user_id       | bigint                      | user_role.user_id, user_country.user_id, password_update_record.user_id, user_mfa.user_id, user_stripe.user_id |           |
| social_id     | character varying (150.0)   |                                                                                                                |           |
| social_type   | character varying (60.0)    |                                                                                                                |           |
| name          | character varying (150.0)   |                                                                                                                |           |
| first_name    | character varying (150.0)   |                                                                                                                |           |
| last_name     | character varying (150.0)   |                                                                                                                |           |
| created_at    | timestamp without time zone |                                                                                                                |           |
| updated_at    | timestamp without time zone |                                                                                                                |           |

## user_stripe

| column_name   | data_type                   | related_keys                                                                                                   | remarks   |
|:--------------|:----------------------------|:---------------------------------------------------------------------------------------------------------------|:----------|
| id            | bigint                      |                                                                                                                |           |
| user_id       | bigint                      | user_role.user_id, user_social.user_id, user_country.user_id, password_update_record.user_id, user_mfa.user_id |           |
| customer_id   | character varying (192.0)   |                                                                                                                |           |
| country       | character varying (24.0)    |                                                                                                                |           |
| created_at    | timestamp without time zone |                                                                                                                |           |
| platform      | character varying (96.0)    |                                                                                                                |           |
| account       | character varying (96.0)    |                                                                                                                |           |
| primary       | smallint                    |                                                                                                                |           |
| updated_at    | timestamp without time zone |                                                                                                                |           |