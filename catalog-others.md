[TOC]


# comp_research


## scooter_history

| column_name       | data_type                   |
|:------------------|:----------------------------|
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_adelaide

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_ansan

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_anyang

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_auckland

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_brisbane

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_bunbury

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_busan

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_canberra

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_daegu

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_esperence

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_gimhae

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_goyang

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_gwangju

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_hobart

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_incheon

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_jeonju

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_launceston

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_logan

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_miami

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_namyangju

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_nowon

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_pohang

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_port_douglas

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_pyeongtaek

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_santa_monica

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_seoul

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_suwon

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_sydney

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_townsville

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_washington_dc_gbfs

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_yongin

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## trip_adelaide

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_all

| column_name     | data_type                   |
|:----------------|:----------------------------|
| start_time      | timestamp without time zone |
| operator        | character varying (20)      |
| scooter_id      | character varying (50)      |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| city            | character varying (20)      |
| vehicle_type    | character varying (20)      |

## trip_ansan

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_anyang

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_auckland

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_brisbane

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_bunbury

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_busan

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_canberra

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_daegu

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_esperence

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_goyang

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_gwangju

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_hobart

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_incheon

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_jeonju

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_launceston

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_logan

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_miami

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_namyangju

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_nowon

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_pohang

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_pyeongtaek

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_santa_monica

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_seoul

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_suwon

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_sydney

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_townsville

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_washington_dc_gbfs

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |

## trip_yongin

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | character varying (64)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| start_battery   | double precision            |
| end_battery     | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| total_minutes   | double precision            |
| operator        | character varying (20)      |
| city            | character varying (20)      |
| vehicle_type    | character varying (25)      |



# information_schema



## applicable_roles

| column_name   | data_type         |
|:--------------|:------------------|
| grantee       | character varying |
| role_name     | character varying |
| is_grantable  | character varying |

## check_constraints

| column_name        | data_type         |
|:-------------------|:------------------|
| constraint_catalog | character varying |
| constraint_schema  | character varying |
| constraint_name    | character varying |
| check_clause       | character varying |

## column_domain_usage

| column_name    | data_type         |
|:---------------|:------------------|
| domain_catalog | character varying |
| domain_schema  | character varying |
| domain_name    | character varying |
| table_catalog  | character varying |
| table_schema   | character varying |
| table_name     | character varying |
| column_name    | character varying |

## column_privileges

| column_name    | data_type         |
|:---------------|:------------------|
| grantor        | character varying |
| grantee        | character varying |
| table_catalog  | character varying |
| table_schema   | character varying |
| table_name     | character varying |
| column_name    | character varying |
| privilege_type | character varying |
| is_grantable   | character varying |

## column_udt_usage

| column_name   | data_type         |
|:--------------|:------------------|
| udt_catalog   | character varying |
| udt_schema    | character varying |
| udt_name      | character varying |
| table_catalog | character varying |
| table_schema  | character varying |
| table_name    | character varying |
| column_name   | character varying |

## columns

| column_name              | data_type         |
|:-------------------------|:------------------|
| table_catalog            | character varying |
| table_schema             | character varying |
| table_name               | character varying |
| column_name              | character varying |
| ordinal_position         | integer           |
| column_default           | character varying |
| is_nullable              | character varying |
| data_type                | character varying |
| character_maximum_length | integer           |
| character_octet_length   | integer           |
| numeric_precision        | integer           |
| numeric_precision_radix  | integer           |
| numeric_scale            | integer           |
| datetime_precision       | integer           |
| interval_type            | character varying |
| interval_precision       | character varying |
| character_set_catalog    | character varying |
| character_set_schema     | character varying |
| character_set_name       | character varying |
| collation_catalog        | character varying |
| collation_schema         | character varying |
| collation_name           | character varying |
| domain_catalog           | character varying |
| domain_schema            | character varying |
| domain_name              | character varying |
| udt_catalog              | character varying |
| udt_schema               | character varying |
| udt_name                 | character varying |
| scope_catalog            | character varying |
| scope_schema             | character varying |
| scope_name               | character varying |
| maximum_cardinality      | integer           |
| dtd_identifier           | character varying |
| is_self_referencing      | character varying |

## constraint_column_usage

| column_name        | data_type         |
|:-------------------|:------------------|
| table_catalog      | character varying |
| table_schema       | character varying |
| table_name         | character varying |
| column_name        | character varying |
| constraint_catalog | character varying |
| constraint_schema  | character varying |
| constraint_name    | character varying |

## constraint_table_usage

| column_name        | data_type         |
|:-------------------|:------------------|
| table_catalog      | character varying |
| table_schema       | character varying |
| table_name         | character varying |
| constraint_catalog | character varying |
| constraint_schema  | character varying |
| constraint_name    | character varying |

## data_type_privileges

| column_name    | data_type         |
|:---------------|:------------------|
| object_catalog | character varying |
| object_schema  | character varying |
| object_name    | character varying |
| object_type    | character varying |
| dtd_identifier | character varying |

## domain_constraints

| column_name        | data_type         |
|:-------------------|:------------------|
| constraint_catalog | character varying |
| constraint_schema  | character varying |
| constraint_name    | character varying |
| domain_catalog     | character varying |
| domain_schema      | character varying |
| domain_name        | character varying |
| is_deferrable      | character varying |
| initially_deferred | character varying |

## domain_udt_usage

| column_name    | data_type         |
|:---------------|:------------------|
| udt_catalog    | character varying |
| udt_schema     | character varying |
| udt_name       | character varying |
| domain_catalog | character varying |
| domain_schema  | character varying |
| domain_name    | character varying |

## domains

| column_name              | data_type         |
|:-------------------------|:------------------|
| domain_catalog           | character varying |
| domain_schema            | character varying |
| domain_name              | character varying |
| data_type                | character varying |
| character_maximum_length | integer           |
| character_octet_length   | integer           |
| character_set_catalog    | character varying |
| character_set_schema     | character varying |
| character_set_name       | character varying |
| collation_catalog        | character varying |
| collation_schema         | character varying |
| collation_name           | character varying |
| numeric_precision        | integer           |
| numeric_precision_radix  | integer           |
| numeric_scale            | integer           |
| datetime_precision       | integer           |
| interval_type            | character varying |
| interval_precision       | character varying |
| domain_default           | character varying |
| udt_catalog              | character varying |
| udt_schema               | character varying |
| udt_name                 | character varying |
| scope_catalog            | character varying |
| scope_schema             | character varying |
| scope_name               | character varying |
| maximum_cardinality      | integer           |
| dtd_identifier           | character varying |

## element_types

| column_name              | data_type         |
|:-------------------------|:------------------|
| object_catalog           | character varying |
| object_schema            | character varying |
| object_name              | character varying |
| object_type              | character varying |
| array_type_identifier    | character varying |
| data_type                | character varying |
| character_maximum_length | integer           |
| character_octet_length   | integer           |
| character_set_catalog    | character varying |
| character_set_schema     | character varying |
| character_set_name       | character varying |
| collation_catalog        | character varying |
| collation_schema         | character varying |
| collation_name           | character varying |
| numeric_precision        | integer           |
| numeric_precision_radix  | integer           |
| numeric_scale            | integer           |
| datetime_precision       | integer           |
| interval_type            | character varying |
| interval_precision       | character varying |
| domain_default           | character varying |
| udt_catalog              | character varying |
| udt_schema               | character varying |
| udt_name                 | character varying |
| scope_catalog            | character varying |
| scope_schema             | character varying |
| scope_name               | character varying |
| maximum_cardinality      | integer           |
| dtd_identifier           | character varying |

## enabled_roles

| column_name   | data_type         |
|:--------------|:------------------|
| role_name     | character varying |

## information_schema_catalog_name

| column_name   | data_type         |
|:--------------|:------------------|
| catalog_name  | character varying |

## key_column_usage

| column_name        | data_type         |
|:-------------------|:------------------|
| constraint_catalog | character varying |
| constraint_schema  | character varying |
| constraint_name    | character varying |
| table_catalog      | character varying |
| table_schema       | character varying |
| table_name         | character varying |
| column_name        | character varying |
| ordinal_position   | integer           |

## parameters

| column_name              | data_type         |
|:-------------------------|:------------------|
| specific_catalog         | character varying |
| specific_schema          | character varying |
| specific_name            | character varying |
| ordinal_position         | integer           |
| parameter_mode           | character varying |
| is_result                | character varying |
| as_locator               | character varying |
| parameter_name           | character varying |
| data_type                | character varying |
| character_maximum_length | integer           |
| character_octet_length   | integer           |
| character_set_catalog    | character varying |
| character_set_schema     | character varying |
| character_set_name       | character varying |
| collation_catalog        | character varying |
| collation_schema         | character varying |
| collation_name           | character varying |
| numeric_precision        | integer           |
| numeric_precision_radix  | integer           |
| numeric_scale            | integer           |
| datetime_precision       | integer           |
| interval_type            | character varying |
| interval_precision       | character varying |
| udt_catalog              | character varying |
| udt_schema               | character varying |
| udt_name                 | character varying |
| scope_catalog            | character varying |
| scope_schema             | character varying |
| scope_name               | character varying |
| maximum_cardinality      | integer           |
| dtd_identifier           | character varying |

## referential_constraints

| column_name               | data_type         |
|:--------------------------|:------------------|
| constraint_catalog        | character varying |
| constraint_schema         | character varying |
| constraint_name           | character varying |
| unique_constraint_catalog | character varying |
| unique_constraint_schema  | character varying |
| unique_constraint_name    | character varying |
| match_option              | character varying |
| update_rule               | character varying |
| delete_rule               | character varying |

## role_column_grants

| column_name    | data_type         |
|:---------------|:------------------|
| grantor        | character varying |
| grantee        | character varying |
| table_catalog  | character varying |
| table_schema   | character varying |
| table_name     | character varying |
| column_name    | character varying |
| privilege_type | character varying |
| is_grantable   | character varying |

## role_routine_grants

| column_name      | data_type         |
|:-----------------|:------------------|
| grantor          | character varying |
| grantee          | character varying |
| specific_catalog | character varying |
| specific_schema  | character varying |
| specific_name    | character varying |
| routine_catalog  | character varying |
| routine_schema   | character varying |
| routine_name     | character varying |
| privilege_type   | character varying |
| is_grantable     | character varying |

## role_table_grants

| column_name    | data_type         |
|:---------------|:------------------|
| grantor        | character varying |
| grantee        | character varying |
| table_catalog  | character varying |
| table_schema   | character varying |
| table_name     | character varying |
| privilege_type | character varying |
| is_grantable   | character varying |
| with_hierarchy | character varying |

## role_usage_grants

| column_name    | data_type         |
|:---------------|:------------------|
| grantor        | character varying |
| grantee        | character varying |
| object_catalog | character varying |
| object_schema  | character varying |
| object_name    | character varying |
| object_type    | character varying |
| privilege_type | character varying |
| is_grantable   | character varying |

## routine_privileges

| column_name      | data_type         |
|:-----------------|:------------------|
| grantor          | character varying |
| grantee          | character varying |
| specific_catalog | character varying |
| specific_schema  | character varying |
| specific_name    | character varying |
| routine_catalog  | character varying |
| routine_schema   | character varying |
| routine_name     | character varying |
| privilege_type   | character varying |
| is_grantable     | character varying |

## routines

| column_name              | data_type         |
|:-------------------------|:------------------|
| specific_catalog         | character varying |
| specific_schema          | character varying |
| specific_name            | character varying |
| routine_catalog          | character varying |
| routine_schema           | character varying |
| routine_name             | character varying |
| routine_type             | character varying |
| module_catalog           | character varying |
| module_schema            | character varying |
| module_name              | character varying |
| udt_catalog              | character varying |
| udt_schema               | character varying |
| udt_name                 | character varying |
| data_type                | character varying |
| character_maximum_length | integer           |
| character_octet_length   | integer           |
| character_set_catalog    | character varying |
| character_set_schema     | character varying |
| character_set_name       | character varying |
| collation_catalog        | character varying |
| collation_schema         | character varying |
| collation_name           | character varying |
| numeric_precision        | integer           |
| numeric_precision_radix  | integer           |
| numeric_scale            | integer           |
| datetime_precision       | integer           |
| interval_type            | character varying |
| interval_precision       | character varying |
| type_udt_catalog         | character varying |
| type_udt_schema          | character varying |
| type_udt_name            | character varying |
| scope_catalog            | character varying |
| scope_schema             | character varying |
| scope_name               | character varying |
| maximum_cardinality      | integer           |
| dtd_identifier           | character varying |
| routine_body             | character varying |
| routine_definition       | character varying |
| external_name            | character varying |
| external_language        | character varying |
| parameter_style          | character varying |
| is_deterministic         | character varying |
| sql_data_access          | character varying |
| is_null_call             | character varying |
| sql_path                 | character varying |
| schema_level_routine     | character varying |
| max_dynamic_result_sets  | integer           |
| is_user_defined_cast     | character varying |
| is_implicitly_invocable  | character varying |
| security_type            | character varying |
| to_sql_specific_catalog  | character varying |
| to_sql_specific_schema   | character varying |
| to_sql_specific_name     | character varying |
| as_locator               | character varying |

## schemata

| column_name                   | data_type         |
|:------------------------------|:------------------|
| catalog_name                  | character varying |
| schema_name                   | character varying |
| schema_owner                  | character varying |
| default_character_set_catalog | character varying |
| default_character_set_schema  | character varying |
| default_character_set_name    | character varying |
| sql_path                      | character varying |

## sql_features

| column_name      | data_type         |
|:-----------------|:------------------|
| feature_id       | character varying |
| feature_name     | character varying |
| sub_feature_id   | character varying |
| sub_feature_name | character varying |
| is_supported     | character varying |
| is_verified_by   | character varying |
| comments         | character varying |

## sql_implementation_info

| column_name              | data_type         |
|:-------------------------|:------------------|
| implementation_info_id   | character varying |
| implementation_info_name | character varying |
| integer_value            | integer           |
| character_value          | character varying |
| comments                 | character varying |

## sql_languages

| column_name                       | data_type         |
|:----------------------------------|:------------------|
| sql_language_source               | character varying |
| sql_language_year                 | character varying |
| sql_language_conformance          | character varying |
| sql_language_integrity            | character varying |
| sql_language_implementation       | character varying |
| sql_language_binding_style        | character varying |
| sql_language_programming_language | character varying |

## sql_packages

| column_name    | data_type         |
|:---------------|:------------------|
| feature_id     | character varying |
| feature_name   | character varying |
| is_supported   | character varying |
| is_verified_by | character varying |
| comments       | character varying |

## sql_sizing

| column_name     | data_type         |
|:----------------|:------------------|
| sizing_id       | integer           |
| sizing_name     | character varying |
| supported_value | integer           |
| comments        | character varying |

## sql_sizing_profiles

| column_name    | data_type         |
|:---------------|:------------------|
| sizing_id      | integer           |
| sizing_name    | character varying |
| profile_id     | character varying |
| required_value | integer           |
| comments       | character varying |

## table_constraints

| column_name        | data_type         |
|:-------------------|:------------------|
| constraint_catalog | character varying |
| constraint_schema  | character varying |
| constraint_name    | character varying |
| table_catalog      | character varying |
| table_schema       | character varying |
| table_name         | character varying |
| constraint_type    | character varying |
| is_deferrable      | character varying |
| initially_deferred | character varying |

## table_privileges

| column_name    | data_type         |
|:---------------|:------------------|
| grantor        | character varying |
| grantee        | character varying |
| table_catalog  | character varying |
| table_schema   | character varying |
| table_name     | character varying |
| privilege_type | character varying |
| is_grantable   | character varying |
| with_hierarchy | character varying |

## tables

| column_name                  | data_type         |
|:-----------------------------|:------------------|
| table_catalog                | character varying |
| table_schema                 | character varying |
| table_name                   | character varying |
| table_type                   | character varying |
| self_referencing_column_name | character varying |
| reference_generation         | character varying |
| user_defined_type_catalog    | character varying |
| user_defined_type_schema     | character varying |
| user_defined_name            | character varying |

## triggered_update_columns

| column_name          | data_type         |
|:---------------------|:------------------|
| trigger_catalog      | character varying |
| trigger_schema       | character varying |
| trigger_name         | character varying |
| event_object_catalog | character varying |
| event_object_schema  | character varying |
| event_object_table   | character varying |
| event_object_column  | character varying |

## triggers

| column_name                   | data_type         |
|:------------------------------|:------------------|
| trigger_catalog               | character varying |
| trigger_schema                | character varying |
| trigger_name                  | character varying |
| event_manipulation            | character varying |
| event_object_catalog          | character varying |
| event_object_schema           | character varying |
| event_object_table            | character varying |
| action_order                  | integer           |
| action_condition              | character varying |
| action_statement              | character varying |
| action_orientation            | character varying |
| condition_timing              | character varying |
| condition_reference_old_table | character varying |
| condition_reference_new_table | character varying |

## usage_privileges

| column_name    | data_type         |
|:---------------|:------------------|
| grantor        | character varying |
| grantee        | character varying |
| object_catalog | character varying |
| object_schema  | character varying |
| object_name    | character varying |
| object_type    | character varying |
| privilege_type | character varying |
| is_grantable   | character varying |

## view_column_usage

| column_name   | data_type         |
|:--------------|:------------------|
| view_catalog  | character varying |
| view_schema   | character varying |
| view_name     | character varying |
| table_catalog | character varying |
| table_schema  | character varying |
| table_name    | character varying |
| column_name   | character varying |

## view_table_usage

| column_name   | data_type         |
|:--------------|:------------------|
| view_catalog  | character varying |
| view_schema   | character varying |
| view_name     | character varying |
| table_catalog | character varying |
| table_schema  | character varying |
| table_name    | character varying |

## views

| column_name        | data_type         |
|:-------------------|:------------------|
| table_catalog      | character varying |
| table_schema       | character varying |
| table_name         | character varying |
| view_definition    | character varying |
| check_option       | character varying |
| is_updatable       | character varying |
| is_insertable_into | character varying |

# mds



## city_api_keys

| column_name   | data_type                   |
|:--------------|:----------------------------|
| prefix        | character varying (50)      |
| api_key       | character varying (64)      |
| id            | integer                     |
| remark        | character varying (128)     |
| created_at    | timestamp without time zone |

## city_config

| column_name   | data_type                   |
|:--------------|:----------------------------|
| country_code  | character varying (20)      |
| city_code     | character varying (35)      |
| prefix        | character varying (50)      |
| timezone      | character varying (50)      |
| zones         | character varying (255)     |
| id            | integer                     |
| accessible    | boolean                     |
| created_at    | timestamp without time zone |

## trip

| column_name     | data_type                   |
|:----------------|:----------------------------|
| user_id         | bigint                      |
| country         | character varying (32)      |
| city            | character varying (32)      |
| zone            | character varying (32)      |
| vehicle_id      | bigint                      |
| vehicle_qr_code | character varying (32)      |
| vehicle_deck_id | character varying (32)      |
| vehicle_type    | character varying (32)      |
| start_time      | timestamp without time zone |
| total_minutes   | integer                     |
| total_mileage   | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| uuid            | character varying (64)      |
| end_time        | timestamp without time zone |
| trip_id         | bigint                      |
| positions       | character varying (65500)   |

## trip_backload

| column_name     | data_type                   |
|:----------------|:----------------------------|
| uuid            | character varying (64)      |
| user_id         | bigint                      |
| country         | character varying (32)      |
| city            | character varying (32)      |
| zone            | character varying (32)      |
| vehicle_id      | bigint                      |
| vehicle_qr_code | character varying (32)      |
| vehicle_deck_id | character varying (32)      |
| vehicle_type    | character varying (32)      |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| total_minutes   | integer                     |
| total_mileage   | double precision            |
| start_latitude  | double precision            |
| start_longitude | double precision            |
| end_latitude    | double precision            |
| end_longitude   | double precision            |
| trip_id         | bigint                      |
| positions       | character varying (65500)   |

## vehicle_status_change

| column_name       | data_type                   |
|:------------------|:----------------------------|
| country           | character varying (32)      |
| city              | character varying (32)      |
| zone              | character varying (32)      |
| previous_zone     | character varying (32)      |
| vehicle_type      | character varying (64)      |
| device_id         | character varying (64)      |
| vehicle_id        | character varying (64)      |
| event_time        | timestamp without time zone |
| battery_pct       | double precision            |
| vehicle_state     | character varying (64)      |
| event_types       | character varying (250)     |
| event_type        | character varying (64)      |
| event_type_reason | character varying (150)     |
| trip_id           | character varying (64)      |
| longitude         | double precision            |
| latitude          | double precision            |
| uuid              | character varying (64)      |

# netsuite



## netsuite_asset_city

| column_name   | data_type                   |
|:--------------|:----------------------------|
| uuid          | character varying (64)      |
| asset_id      | character varying (64)      |
| country       | character varying (32)      |
| previous_city | character varying (32)      |
| city          | character varying (32)      |
| asset_type    | character varying (32)      |
| source_id     | character varying (32)      |
| vehicle_type  | character varying (32)      |
| created_at    | timestamp without time zone |

## netsuite_asset_record

| column_name     | data_type                   |
|:----------------|:----------------------------|
| city            | character varying (24)      |
| country         | character varying (24)      |
| asset_id        | character varying (64)      |
| asset_type      | character varying (64)      |
| previous_status | character varying (128)     |
| status          | character varying (128)     |
| source_id       | bigint                      |
| vehicle_type    | character varying (32)      |
| created_at      | timestamp without time zone |
| uuid            | character varying (36)      |

# neuron_paypal



## _sdc_rejected

| column_name      | data_type                   |
|:-----------------|:----------------------------|
| record           | character varying (65535)   |
| reason           | character varying (2048)    |
| table_name       | character varying (127)     |
| _sdc_rejected_at | timestamp without time zone |

## transactions

| column_name                                       | data_type                   |
|:--------------------------------------------------|:----------------------------|
| _sdc_batched_at                                   | timestamp without time zone |
| _sdc_extracted_at                                 | timestamp without time zone |
| _sdc_received_at                                  | timestamp without time zone |
| _sdc_sequence                                     | bigint                      |
| _sdc_table_version                                | bigint                      |
| amount                                            | double precision            |
| created_at                                        | timestamp without time zone |
| currency_iso_code                                 | character varying (128)     |
| customer_details__email                           | character varying (128)     |
| customer_details__id                              | character varying (128)     |
| id                                                | character varying (128)     |
| merchant_account_id                               | character varying (128)     |
| order_id                                          | character varying (128)     |
| payment_instrument_type                           | character varying (128)     |
| paypal_details__authorization_id                  | character varying (128)     |
| paypal_details__capture_id                        | character varying (128)     |
| paypal_details__payer_email                       | character varying (128)     |
| paypal_details__payer_id                          | character varying (128)     |
| paypal_details__payer_status                      | character varying (128)     |
| paypal_details__payment_id                        | character varying (128)     |
| paypal_details__refund_id                         | character varying (128)     |
| paypal_details__seller_protection_status          | character varying (128)     |
| paypal_details__transaction_fee_amount            | character varying (128)     |
| paypal_details__transaction_fee_currency_iso_code | character varying (128)     |
| processor_response_code                           | character varying (128)     |
| processor_response_text                           | character varying (128)     |
| refunded_transaction_id                           | character varying (128)     |
| settlement_batch_id                               | character varying (128)     |
| status                                            | character varying (128)     |
| type                                              | character varying (128)     |
| updated_at                                        | timestamp without time zone |
| gateway_rejection_reason                          | character varying (128)     |

# neuron_paypal_nz



## transactions

| column_name                                       | data_type                   |
|:--------------------------------------------------|:----------------------------|
| _sdc_batched_at                                   | timestamp without time zone |
| _sdc_extracted_at                                 | timestamp without time zone |
| _sdc_received_at                                  | timestamp without time zone |
| _sdc_sequence                                     | bigint                      |
| _sdc_table_version                                | bigint                      |
| amount                                            | double precision            |
| created_at                                        | timestamp without time zone |
| currency_iso_code                                 | character varying (128)     |
| customer_details__email                           | character varying (128)     |
| customer_details__id                              | character varying (128)     |
| id                                                | character varying (128)     |
| merchant_account_id                               | character varying (128)     |
| order_id                                          | character varying (128)     |
| payment_instrument_type                           | character varying (128)     |
| paypal_details__authorization_id                  | character varying (128)     |
| paypal_details__capture_id                        | character varying (128)     |
| paypal_details__payer_email                       | character varying (128)     |
| paypal_details__payer_id                          | character varying (128)     |
| paypal_details__payer_status                      | character varying (128)     |
| paypal_details__payment_id                        | character varying (128)     |
| paypal_details__refund_id                         | character varying (128)     |
| paypal_details__seller_protection_status          | character varying (128)     |
| paypal_details__transaction_fee_amount            | character varying (128)     |
| paypal_details__transaction_fee_currency_iso_code | character varying (128)     |
| processor_response_code                           | character varying (128)     |
| processor_response_text                           | character varying (128)     |
| refunded_transaction_id                           | character varying (128)     |
| settlement_batch_id                               | character varying (128)     |
| status                                            | character varying (128)     |
| type                                              | character varying (128)     |
| updated_at                                        | timestamp without time zone |

# neuron_public



## battery_history

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| id                 | bigint                      |
| scooter_id         | bigint                      |
| created_at         | timestamp without time zone |
| updated_at         | timestamp without time zone |
| battery_number     | character varying (256)     |
| remaining_capacity | smallint                    |
| full_capacity      | smallint                    |
| work_cycle         | smallint                    |
| electricity        | double precision            |
| city               | character varying (256)     |

## command_log_history

| column_name   | data_type                   |
|:--------------|:----------------------------|
| id            | bigint                      |
| command       | character varying (1024)    |
| succeed       | smallint                    |
| device_id     | bigint                      |
| created_at    | timestamp without time zone |

## command_response_history

| column_name   | data_type                   |
|:--------------|:----------------------------|
| id            | bigint                      |
| message       | character varying (255)     |
| device_id     | bigint                      |
| created_at    | timestamp without time zone |

## gps_history

| column_name          | data_type                   |
|:---------------------|:----------------------------|
| id                   | bigint                      |
| imei                 | character varying (50)      |
| created_at           | timestamp without time zone |
| updated_at           | timestamp without time zone |
| remaining_battery    | double precision            |
| latitude             | double precision            |
| longitude            | double precision            |
| engine_off           | smallint                    |
| gps_battery_level    | smallint                    |
| gps_external_power   | smallint                    |
| status               | character varying (30)      |
| alarm_on             | smallint                    |
| voltage              | double precision            |
| location_source      | character varying (50)      |
| position_active_time | timestamp without time zone |
| helmet_attached      | smallint                    |

## n3_status_history

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| id                      | bigint                      |
| electric_hall           | smallint                    |
| a_phase_current         | smallint                    |
| b_phase_current         | smallint                    |
| c_phase_current         | smallint                    |
| dc_current              | smallint                    |
| accelerator             | smallint                    |
| dashboard_communication | smallint                    |
| bms_communication       | smallint                    |
| brake                   | smallint                    |
| iot_communication       | smallint                    |
| ble_communication       | smallint                    |
| lowe_battery            | smallint                    |
| low_battery_protection  | smallint                    |
| topple                  | smallint                    |
| lock_status             | character varying (32)      |
| battery_percentage      | double precision            |
| device_id               | bigint                      |
| created_at              | timestamp without time zone |
| single_range            | double precision            |
| speed                   | double precision            |
| voltage                 | double precision            |
| electricity             | double precision            |
| helmet_attached         | smallint                    |

## scooter_history

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| id                 | bigint                      |
| imei               | character varying (50)      |
| station_id         | double precision            |
| qr_code            | character varying (50)      |
| deck_id            | character varying (50)      |
| mac                | character varying (70)      |
| status             | character varying (30)      |
| city               | character varying (20)      |
| created_at         | timestamp without time zone |
| updated_at         | timestamp without time zone |
| deleted            | smallint                    |
| sticker            | character varying (10)      |
| last_sanitize_time | timestamp without time zone |

## staging_battery_history

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| id                 | bigint                      |
| scooter_id         | bigint                      |
| created_at         | timestamp without time zone |
| updated_at         | timestamp without time zone |
| battery_number     | character varying (256)     |
| remaining_capacity | smallint                    |
| full_capacity      | smallint                    |
| work_cycle         | smallint                    |
| electricity        | double precision            |
| city               | character varying (256)     |

## staging_command_log_history

| column_name   | data_type                   |
|:--------------|:----------------------------|
| id            | bigint                      |
| command       | character varying (1024)    |
| succeed       | smallint                    |
| device_id     | bigint                      |
| created_at    | timestamp without time zone |

## staging_command_response_history

| column_name   | data_type                   |
|:--------------|:----------------------------|
| id            | bigint                      |
| message       | character varying (255)     |
| device_id     | bigint                      |
| created_at    | timestamp without time zone |

## staging_gps_history

| column_name          | data_type                   |
|:---------------------|:----------------------------|
| op                   | character varying (10)      |
| id                   | bigint                      |
| imei                 | character varying (50)      |
| created_at           | timestamp without time zone |
| updated_at           | timestamp without time zone |
| remaining_battery    | double precision            |
| latitude             | double precision            |
| longitude            | double precision            |
| engine_off           | smallint                    |
| gps_battery_level    | smallint                    |
| gps_external_power   | smallint                    |
| status               | character varying (30)      |
| alarm_on             | smallint                    |
| version              | character varying (256)     |
| voltage              | double precision            |
| apn                  | character varying (256)     |
| iccid                | character varying (256)     |
| location_source      | character varying (50)      |
| position_active_time | timestamp without time zone |
| protocol             | character varying (50)      |
| normal_speed         | smallint                    |
| sport_speed          | smallint                    |
| dashboard_version    | character varying (256)     |
| motor_version        | character varying (256)     |
| bluetooth_version    | character varying (256)     |
| helmet_attached      | smallint                    |
| no_parking_light     | smallint                    |
| wrench_light         | smallint                    |

## staging_n3_status_history

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| id                      | bigint                      |
| electric_hall           | smallint                    |
| a_phase_current         | smallint                    |
| b_phase_current         | smallint                    |
| c_phase_current         | smallint                    |
| dc_current              | smallint                    |
| accelerator             | smallint                    |
| dashboard_communication | smallint                    |
| bms_communication       | smallint                    |
| brake                   | smallint                    |
| iot_communication       | smallint                    |
| ble_communication       | smallint                    |
| lowe_battery            | smallint                    |
| low_battery_protection  | smallint                    |
| topple                  | smallint                    |
| lock_status             | character varying (32)      |
| battery_percentage      | double precision            |
| device_id               | bigint                      |
| created_at              | timestamp without time zone |
| single_range            | double precision            |
| speed                   | double precision            |
| voltage                 | double precision            |
| electricity             | double precision            |
| helmet_attached         | smallint                    |

## staging_scooter_history

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| op                 | character varying (10)      |
| id                 | bigint                      |
| imei               | character varying (50)      |
| dock_id            | double precision            |
| station_id         | double precision            |
| qr_code            | character varying (50)      |
| deck_id            | character varying (50)      |
| mac                | character varying (70)      |
| ic_card            | double precision            |
| status             | character varying (30)      |
| city               | character varying (20)      |
| created_at         | timestamp without time zone |
| updated_at         | timestamp without time zone |
| version            | integer                     |
| deleted            | smallint                    |
| image_id           | character varying (256)     |
| generation         | character varying (16)      |
| zone               | character varying (256)     |
| licence            | character varying (256)     |
| type               | character varying (256)     |
| last_test_time     | timestamp without time zone |
| helmet_lock        | smallint                    |
| sticker            | character varying (10)      |
| last_sanitize_time | timestamp without time zone |

## trip_start_end_locations

| column_name   | data_type               |
|:--------------|:------------------------|
| device_id     | integer                 |
| el            | character varying (256) |
| end_lat       | double precision        |
| end_lon       | double precision        |
| end_time      | character varying (256) |
| sl            | character varying (256) |
| start_lat     | double precision        |
| start_lon     | double precision        |
| start_time    | character varying (256) |
| city          | character varying (256) |
| trip_id       | integer                 |

# neuron_public_backend



## login_completed

| column_name                 | data_type                   |
|:----------------------------|:----------------------------|
| id                          | character varying (512)     |
| received_at                 | timestamp without time zone |
| country                     | character varying (512)     |
| timestamp                   | timestamp without time zone |
| app_version                 | character varying (512)     |
| city                        | character varying (512)     |
| sent_at                     | timestamp without time zone |
| signin_type                 | character varying (512)     |
| user_id                     | character varying (512)     |
| original_timestamp          | timestamp without time zone |
| uuid                        | bigint                      |
| uuid_ts                     | timestamp without time zone |
| context_protocols_source_id | character varying (512)     |
| vip_status                  | character varying (512)     |
| event_category              | character varying (512)     |

# neuron_qa



## incident_report

| column_name             | data_type                 |
|:------------------------|:--------------------------|
| incident_id             | bigint                    |
| city                    | character varying (8)     |
| business_risk_level     | character varying (16)    |
| risk_level              | character varying (16)    |
| status                  | character varying (32)    |
| type                    | character varying (32)    |
| incident_time           | timestamp with time zone  |
| aware_time              | timestamp with time zone  |
| police                  | boolean                   |
| ticket_url              | character varying (256)   |
| short_description       | character varying (65535) |
| incident_description    | character varying (65535) |
| remarks                 | character varying (65535) |
| user_involved           | character varying (32)    |
| third_party_involved    | character varying (32)    |
| location                | character varying (65535) |
| created_user            | character varying (32)    |
| created_user_role       | character varying (32)    |
| updated_user            | character varying (32)    |
| updated_user_role       | character varying (32)    |
| platform                | character varying (16)    |
| version                 | double precision          |
| created_at              | timestamp with time zone  |
| updated_at              | timestamp with time zone  |
| updated_by              | bigint                    |
| created_by              | bigint                    |
| latitude                | double precision          |
| longitude               | double precision          |
| category                | character varying (16)    |
| reason                  | character varying (65535) |
| source                  | character varying (32)    |
| edm_sent                | boolean                   |
| user_id                 | bigint                    |
| injury                  | character varying (16)    |
| injury_severity         | character varying (16)    |
| medical                 | character varying (16)    |
| email                   | character varying (64)    |
| paramedic               | character varying (16)    |
| user_description        | character varying (65535) |
| trip_id                 | bigint                    |
| start_time              | timestamp with time zone  |
| end_time                | timestamp with time zone  |
| trip_status             | character varying (16)    |
| start_station_id        | integer                   |
| start_station_name      | character varying (32)    |
| end_station_id          | integer                   |
| end_station_name        | character varying (32)    |
| start_zone_code         | character varying (16)    |
| start_zone_name         | character varying (32)    |
| end_zone_code           | character varying (16)    |
| end_zone_name           | character varying (32)    |
| deck_id                 | character varying (32)    |
| generation              | character varying (8)     |
| device_id               | bigint                    |
| imei                    | character varying (32)    |
| qr_code                 | character varying (16)    |
| sticker_id              | character varying (16)    |
| party                   | character varying (16)    |
| third_party_category    | character varying (16)    |
| third_party_description | character varying (65535) |

# neuron_test



## balance_transactions

| column_name   | data_type                   |
|:--------------|:----------------------------|
| updated       | timestamp without time zone |
| id            | character varying (30)      |
| object        | character varying (20)      |
| amount        | bigint                      |
| available_on  | character varying (30)      |
| created       | timestamp without time zone |
| currency      | character varying (20)      |
| description   | character varying (128)     |
| exchange_rate | character varying (128)     |
| fee           | character varying (128)     |
| net           | character varying (128)     |
| source        | character varying (128)     |
| status        | character varying (128)     |
| type          | character varying (128)     |

## balance_transactions_fee_details

| column_name   | data_type              |
|:--------------|:-----------------------|
| id            | character varying (30) |
| amount        | real                   |
| currency      | character varying (30) |
| description   | character varying (30) |
| type          | character varying (20) |

## charges

| column_name                            | data_type                   |
|:---------------------------------------|:----------------------------|
| updated                                | timestamp without time zone |
| id                                     | character varying (30)      |
| object                                 | character varying (20)      |
| amount                                 | bigint                      |
| amount_refunded                        | bigint                      |
| balance_transaction                    | character varying (30)      |
| captured                               | boolean                     |
| created                                | timestamp without time zone |
| currency                               | character varying (20)      |
| customer                               | character varying (128)     |
| description                            | character varying (128)     |
| dispute                                | character varying (128)     |
| disputed                               | boolean                     |
| failure_code                           | character varying (128)     |
| failure_message                        | character varying (128)     |
| livemode                               | boolean                     |
| metadata__user_id                      | bigint                      |
| metadata__email                        | character varying (128)     |
| metadata__primary_card                 | character varying (128)     |
| metadata__card_token                   | character varying (128)     |
| metadata__order_id                     | character varying (128)     |
| metadata__start_tomorrow               | character varying (128)     |
| metadata__pass_id                      | character varying (128)     |
| metadata__subscribe                    | character varying (128)     |
| outcome__network_status                | character varying (128)     |
| outcome__reason                        | character varying (2000)    |
| outcome__risk_level                    | character varying (128)     |
| outcome__seller_message                | character varying (2000)    |
| outcome__type                          | character varying (128)     |
| outcome__rule                          | character varying (128)     |
| paid                                   | boolean                     |
| payment_intent                         | character varying (128)     |
| payment_method                         | character varying (128)     |
| payment_method_details__brand          | character varying (128)     |
| payment_method_details__checks         | character varying (3000)    |
| payment_method_details__country        | character varying (128)     |
| payment_method_details__exp_month      | bigint                      |
| payment_method_details__exp_year       | bigint                      |
| payment_method_details__fingerprint    | character varying (128)     |
| payment_method_details__funding        | character varying (128)     |
| payment_method_details__last4          | character varying (128)     |
| payment_method_details__network        | character varying (128)     |
| payment_method_details__three_d_secure | character varying (128)     |
| payment_method_details__wallet         | character varying (3000)    |
| receipt_email                          | character varying (128)     |
| receipt_number                         | character varying (128)     |
| receipt_url                            | character varying (128)     |
| refunded                               | boolean                     |
| source__id                             | character varying (128)     |
| source__object                         | character varying (128)     |
| source__address_city                   | character varying (128)     |
| source__address_country                | character varying (128)     |
| source__address_line1                  | character varying (128)     |
| source__address_line1_check            | character varying (128)     |
| source__address_line2                  | character varying (128)     |
| source__address_state                  | character varying (128)     |
| source__address_zip                    | character varying (128)     |
| source__address_zip_check              | character varying (128)     |
| source__brand                          | character varying (128)     |
| source__country                        | character varying (128)     |
| source__customer                       | character varying (128)     |
| source__cvc_check                      | character varying (128)     |
| source__dynamic_last4                  | character varying (128)     |
| source__exp_month                      | character varying (30)      |
| source__exp_year                       | character varying (30)      |
| source__fingerprint                    | character varying (128)     |
| source__funding                        | character varying (128)     |
| source__last4                          | character varying (128)     |
| source__metadata                       | character varying (128)     |
| source__name                           | character varying (128)     |
| source__tokenization_method            | character varying (128)     |
| source__amount                         | character varying (128)     |
| source__card                           | character varying (2000)    |
| source__client_secret                  | character varying (128)     |
| source__created                        | timestamp without time zone |
| source__currency                       | character varying (128)     |
| source__flow                           | character varying (128)     |
| source__livemode                       | boolean                     |
| source__owner                          | character varying (2000)    |
| source__statement_descriptor           | character varying (128)     |
| source__status                         | character varying (128)     |
| source__type                           | character varying (128)     |
| source__usage                          | character varying (128)     |
| status                                 | character varying (128)     |

## customers

| column_name     | data_type                   |
|:----------------|:----------------------------|
| updated         | timestamp without time zone |
| id              | character varying (30)      |
| object          | character varying (20)      |
| balance         | bigint                      |
| created         | timestamp without time zone |
| currency        | character varying (20)      |
| default_source  | character varying (3000)    |
| delinquent      | boolean                     |
| description     | character varying (128)     |
| email           | character varying (128)     |
| invoice_prefix  | character varying (128)     |
| livemode        | character varying (128)     |
| metadata__uid   | character varying (128)     |
| metadata__phone | character varying (128)     |

## customers_sources

| column_name         | data_type               |
|:--------------------|:------------------------|
| id                  | character varying (30)  |
| object              | character varying (20)  |
| address_city        | character varying (128) |
| address_country     | character varying (128) |
| address_line1       | character varying (128) |
| address_line1_check | character varying (128) |
| address_line2       | character varying (128) |
| address_state       | character varying (128) |
| address_zip         | character varying (128) |
| address_zip_check   | character varying (128) |
| brand               | character varying (128) |
| country             | character varying (128) |
| customer            | character varying (128) |
| cvc_check           | character varying (128) |
| dynamic_last4       | character varying (128) |
| exp_month           | character varying (128) |
| exp_year            | character varying (128) |
| fingerprint         | character varying (128) |
| funding             | character varying (128) |
| last4               | character varying (128) |
| name                | character varying (128) |
| tokenization_method | character varying (128) |

## disputes

| column_name          | data_type                   |
|:---------------------|:----------------------------|
| updated              | timestamp without time zone |
| id                   | character varying (30)      |
| object               | character varying (20)      |
| amount               | bigint                      |
| balance_transaction  | character varying (30)      |
| balance_transactions | character varying (2000)    |
| charge               | character varying (30)      |
| created              | timestamp without time zone |
| currency             | character varying (20)      |
| evidence             | character varying (2000)    |
| evidence_details     | character varying (2000)    |
| is_charge_refundable | boolean                     |
| livemode             | boolean                     |
| metadata             | character varying (20)      |
| payment_intent       | character varying (30)      |
| reason               | character varying (20)      |
| status               | character varying (20)      |

## event

| column_name      | data_type                   |
|:-----------------|:----------------------------|
| updated_at       | timestamp without time zone |
| id               | character varying (30)      |
| object           | character varying (20)      |
| api_version      | timestamp without time zone |
| created_at       | timestamp without time zone |
| data             | character varying (10000)   |
| livemode         | boolean                     |
| pending_webhooks | character varying (128)     |
| request          | character varying (3000)    |
| type             | character varying (128)     |

## gps_history

| column_name          | data_type                   |
|:---------------------|:----------------------------|
| id                   | bigint                      |
| imei                 | character varying (50)      |
| created_at           | timestamp without time zone |
| updated_at           | timestamp without time zone |
| remaining_battery    | double precision            |
| latitude             | double precision            |
| longitude            | double precision            |
| engine_off           | smallint                    |
| gps_battery_level    | smallint                    |
| gps_external_power   | smallint                    |
| status               | character varying (30)      |
| alarm_on             | smallint                    |
| voltage              | double precision            |
| location_source      | character varying (50)      |
| position_active_time | timestamp without time zone |
| helmet_attached      | smallint                    |
| no_parking_light     | smallint                    |
| wrench_light         | smallint                    |
| bt_mac               | character varying (32)      |
| is_connected         | smallint                    |
| topple               | smallint                    |
| remaining_range      | numeric                     |
| blt_password         | character varying (128)     |
| iot_version          | character varying (50)      |
| dbu_hw               | character varying (10)      |

## iot_raw

| column_name       | data_type                   |
|:------------------|:----------------------------|
| uuid              | character varying (32)      |
| imei              | character varying (32)      |
| content           | character varying (65535)   |
| upstream          | boolean                     |
| created_at        | timestamp without time zone |
| device_time       | timestamp without time zone |
| formatted_content | character varying (4096)    |
| name              | character varying (32)      |

## iot_raw_test

| column_name   | data_type                   |
|:--------------|:----------------------------|
| imei          | character varying (32)      |
| created_at    | timestamp without time zone |
| uuid          | character varying (64)      |
| content       | character varying (65535)   |
| upstream      | boolean                     |
| device_time   | timestamp without time zone |
| name          | character varying (32)      |

## n3_status_history

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| uuid                    | character varying (32)      |
| electric_hall           | smallint                    |
| a_phase_current         | smallint                    |
| b_phase_current         | smallint                    |
| c_phase_current         | smallint                    |
| dc_current              | smallint                    |
| accelerator             | smallint                    |
| dashbaord_communication | smallint                    |
| bms_communication       | smallint                    |
| brake                   | smallint                    |
| iot_communication       | smallint                    |
| ble_communication       | smallint                    |
| low_battery             | smallint                    |
| low_battery_protection  | smallint                    |
| topple                  | smallint                    |
| lock_status             | character varying (32)      |
| battery_percentage      | double precision            |
| gps_id                  | bigint                      |
| created_at              | timestamp without time zone |
| single_range            | double precision            |
| speed                   | double precision            |
| voltage                 | double precision            |
| electricity             | double precision            |
| helmet_attached         | smallint                    |

## okai_position_test

| column_name   | data_type                   |
|:--------------|:----------------------------|
| imei          | character varying (32)      |
| created_at    | timestamp without time zone |
| uuid          | character varying (64)      |
| mcc           | character varying (50)      |
| mnc           | character varying (50)      |
| lac           | character varying (50)      |
| cid           | character varying (50)      |
| accuracy      | smallint                    |
| speed         | double precision            |
| course        | integer                     |
| altitude      | double precision            |
| latitude      | double precision            |
| longitude     | double precision            |
| device_time   | timestamp without time zone |

## payouts

| column_name                        | data_type                   |
|:-----------------------------------|:----------------------------|
| updated                            | timestamp without time zone |
| id                                 | character varying (30)      |
| object                             | character varying (20)      |
| amount                             | bigint                      |
| arrival_date                       | timestamp without time zone |
| automatic                          | boolean                     |
| balance_transaction                | character varying (128)     |
| created                            | timestamp without time zone |
| currency                           | character varying (30)      |
| description                        | character varying (128)     |
| destination                        | character varying (128)     |
| failure_balance_transaction        | character varying (128)     |
| failure_code                       | character varying (128)     |
| failure_message                    | character varying (128)     |
| livemode                           | boolean                     |
| method                             | character varying (30)      |
| source_type                        | character varying (30)      |
| status                             | character varying (30)      |
| type                               | character varying (30)      |
| bank_account__account              | character varying (128)     |
| bank_account__account_holder_name  | character varying (128)     |
| bank_account__account_holder_type  | character varying (128)     |
| bank_account__bank_name            | character varying (128)     |
| bank_account__country              | character varying (128)     |
| bank_account__currency             | character varying (128)     |
| bank_account__default_for_currency | character varying (128)     |
| bank_account__fingerprint          | character varying (128)     |
| bank_account__last4                | character varying (128)     |
| bank_account__metadata             | character varying (128)     |
| bank_account__object               | character varying (128)     |
| bank_account__routing_number       | character varying (128)     |
| bank_account__status               | character varying (128)     |

## review_au

| column_name         | data_type                   |
|:--------------------|:----------------------------|
| updated             | timestamp without time zone |
| id                  | character varying (30)      |
| object              | character varying (20)      |
| billing_zip         | character varying (30)      |
| charge              | character varying (30)      |
| closed_reason       | character varying (20)      |
| created             | timestamp without time zone |
| ip_address          | character varying (200)     |
| ip_address_location | character varying (2000)    |
| livemode            | boolean                     |
| opened              | character varying (20)      |
| opened_reason       | character varying (30)      |
| reason              | character varying (200)     |
| session             | character varying (3000)    |
| payment_intent      | character varying (30)      |

## review_nz

| column_name         | data_type                   |
|:--------------------|:----------------------------|
| updated             | timestamp without time zone |
| id                  | character varying (30)      |
| object              | character varying (20)      |
| billing_zip         | character varying (30)      |
| charge              | character varying (30)      |
| closed_reason       | character varying (20)      |
| created             | timestamp without time zone |
| ip_address          | character varying (200)     |
| ip_address_location | character varying (2000)    |
| livemode            | boolean                     |
| opened              | character varying (20)      |
| opened_reason       | character varying (30)      |
| reason              | character varying (200)     |
| session             | character varying (3000)    |
| payment_intent      | character varying (30)      |

## scooter_recent_trips

| column_name     | data_type                   |
|:----------------|:----------------------------|
| scooter_id      | bigint                      |
| city            | character varying (10)      |
| first_trip_time | timestamp without time zone |
| last_trip_time  | timestamp without time zone |
| mid_cut_rate    | double precision            |

## test_start_end_locations

| column_name   | data_type                   |
|:--------------|:----------------------------|
| end_time      | timestamp without time zone |
| start_time    | timestamp without time zone |
| trip_id       | bigint                      |
| city          | character varying (20)      |
| start_point   | geometry                    |
| end_point     | geometry                    |
| scooter_id    | bigint                      |

## user_gps_tracing

| column_name   | data_type                   |
|:--------------|:----------------------------|
| uuid          | character varying (32)      |
| user_id       | bigint                      |
| trip_id       | bigint                      |
| latitude      | numeric                     |
| longitude     | numeric                     |
| created_at    | timestamp without time zone |
| accuracy      | numeric                     |
| fixed_time    | timestamp without time zone |
| speed         | numeric                     |

## user_gps_tracing_history_test

| column_name   | data_type                   |
|:--------------|:----------------------------|
| uuid          | character varying (32)      |
| user_id       | bigint                      |
| trip_id       | bigint                      |
| latitude      | numeric                     |
| longitude     | numeric                     |
| created_at    | timestamp without time zone |
| accuracy      | numeric                     |
| fixed_time    | timestamp without time zone |
| speed         | numeric                     |

## user_group

| column_name                       | data_type                   |
|:----------------------------------|:----------------------------|
| user_id                           | bigint                      |
| disabled                          | bigint                      |
| email                             | character varying (256)     |
| full_name                         | character varying (256)     |
| signup_date                       | timestamp without time zone |
| historical_top_up                 | double precision            |
| pending_amount                    | double precision            |
| total_amount                      | double precision            |
| total_trips                       | double precision            |
| pending_trips                     | double precision            |
| refund_amount                     | double precision            |
| pass_purchase                     | double precision            |
| large_unrefunded_trips            | double precision            |
| sum_large_unrefunded_amount       | double precision            |
| missing_devices                   | double precision            |
| disputed_trip_count               | double precision            |
| disputed_amount                   | double precision            |
| blocked_card_count                | double precision            |
| group_size                        | bigint                      |
| group_disabled_accounts           | bigint                      |
| group_leader                      | bigint                      |
| group_historical_top_up           | double precision            |
| group_pending_amount              | double precision            |
| group_total_amount                | double precision            |
| group_total_trips                 | double precision            |
| group_pending_trips               | double precision            |
| group_refund_amount               | double precision            |
| group_pass_purchase               | double precision            |
| group_large_unrefunded_trips      | double precision            |
| group_sum_large_unrefunded_amount | double precision            |
| group_missing_devices             | double precision            |
| group_disputed_trip_count         | double precision            |
| group_disputed_amount             | double precision            |
| group_fingerprint_count           | bigint                      |
| group_blocked_cards               | bigint                      |
| created_at                        | timestamp without time zone |
| country                           | character varying (256)     |
| last_login                        | timestamp without time zone |
| disputed_topup_count              | integer                     |
| disputed_pass_count               | integer                     |
| disputed_payg_amount              | double precision            |
| disputed_topup_amount             | double precision            |
| disputed_pass_amount              | double precision            |
| group_disputed_topup_count        | integer                     |
| group_disputed_pass_count         | integer                     |
| group_disputed_payg_amount        | double precision            |
| group_disputed_topup_amount       | double precision            |
| group_disputed_pass_amount        | double precision            |
| disputed_count                    | integer                     |
| max_daily_fail                    | double precision            |
| pre_auth_success_rate             | double precision            |
| pre_auth_cnt                      | integer                     |
| group_disputed_count              | integer                     |
| unique_card_count                 | character varying (256)     |
| pending_group_ride_amount         | double precision            |
| pending_group_ride_trips          | double precision            |
| pending_group_ride_groups         | double precision            |
| group_pending_group_ride_amount   | double precision            |
| group_pending_group_ride_trips    | double precision            |
| group_pending_group_ride_groups   | double precision            |
| reason                            | character varying (10000)   |

# neuron_weather



## weather

| column_name         | data_type                   |
|:--------------------|:----------------------------|
| visibility          | bigint                      |
| dt                  | timestamp without time zone |
| city_id             | bigint                      |
| city_name           | character varying (50)      |
| city                | character varying (10)      |
| temp                | double precision            |
| temp_feels_like     | double precision            |
| temp_min            | double precision            |
| temp_max            | double precision            |
| pressure            | bigint                      |
| humidity            | smallint                    |
| wind_speed          | double precision            |
| wind_dir_deg        | smallint                    |
| wind_gust           | double precision            |
| clouds_all          | smallint                    |
| city_country        | character varying (50)      |
| city_sunrise        | timestamp without time zone |
| city_sunset         | timestamp without time zone |
| rain_1h             | bigint                      |
| rain_3h             | bigint                      |
| snow_1h             | double precision            |
| snow_3h             | bigint                      |
| weather_id          | smallint                    |
| weather_name        | character varying (30)      |
| weather_description | character varying (50)      |
| created_at          | timestamp without time zone |
| is_historical       | boolean                     |

# neuron_zendesk



## ticket_metrics

| column_name                      | data_type                |
|:---------------------------------|:-------------------------|
| agent_wait_time_in_minutes       | character varying (256)  |
| assigned_at                      | timestamp with time zone |
| assignee_stations                | bigint                   |
| assignee_updated_at              | timestamp with time zone |
| created_at                       | timestamp with time zone |
| first_resolution_time_in_minutes | character varying (256)  |
| full_resolution_time_in_minutes  | character varying (256)  |
| group_stations                   | bigint                   |
| initially_assigned_at            | timestamp with time zone |
| latest_comment_added_at          | timestamp with time zone |
| on_hold_time_in_minutes          | character varying (256)  |
| reopens                          | bigint                   |
| replies                          | bigint                   |
| reply_time_in_minutes            | character varying (256)  |
| requester_updated_at             | timestamp with time zone |
| requester_wait_time_in_minutes   | character varying (256)  |
| solved_at                        | timestamp with time zone |
| status_updated_at                | timestamp with time zone |
| ticket_id                        | bigint                   |
| updated_at                       | timestamp with time zone |
| url                              | character varying (256)  |
| id                               | bigint                   |

## tickets

| column_name           | data_type                 |
|:----------------------|:--------------------------|
| allow_attachments     | boolean                   |
| allow_channelback     | boolean                   |
| assignee_id           | double precision          |
| brand_id              | bigint                    |
| collaborator_ids      | character varying (1000)  |
| created_at            | timestamp with time zone  |
| custom_fields         | character varying (256)   |
| description           | character varying (65535) |
| due_at                | character varying (256)   |
| email_cc_ids          | character varying (1000)  |
| external_id           | character varying (1000)  |
| fields                | character varying (256)   |
| follower_ids          | character varying (1000)  |
| followup_ids          | character varying (1000)  |
| forum_topic_id        | character varying (256)   |
| generated_timestamp   | bigint                    |
| group_id              | double precision          |
| has_incidents         | boolean                   |
| is_public             | boolean                   |
| organization_id       | double precision          |
| priority              | character varying (256)   |
| problem_id            | character varying (256)   |
| raw_subject           | character varying (10000) |
| recipient             | character varying (256)   |
| requester_id          | bigint                    |
| sharing_agreement_ids | character varying (256)   |
| status                | character varying (256)   |
| subject               | character varying (10000) |
| submitter_id          | bigint                    |
| type                  | character varying (256)   |
| updated_at            | timestamp with time zone  |
| url                   | character varying (256)   |
| via                   | character varying (10000) |
| first_assignee_id     | double precision          |
| satisfaction_rating   | character varying (2500)  |
| city                  | character varying (256)   |
| id                    | bigint                    |
| tags                  | character varying (1000)  |

# paypal_uk



## transactions

| column_name                                       | data_type                   |
|:--------------------------------------------------|:----------------------------|
| _sdc_batched_at                                   | timestamp without time zone |
| _sdc_extracted_at                                 | timestamp without time zone |
| _sdc_received_at                                  | timestamp without time zone |
| _sdc_sequence                                     | bigint                      |
| _sdc_table_version                                | bigint                      |
| amount                                            | double precision            |
| created_at                                        | timestamp without time zone |
| currency_iso_code                                 | character varying (128)     |
| customer_details__email                           | character varying (128)     |
| customer_details__id                              | character varying (128)     |
| id                                                | character varying (128)     |
| merchant_account_id                               | character varying (128)     |
| order_id                                          | character varying (128)     |
| payment_instrument_type                           | character varying (128)     |
| paypal_details__authorization_id                  | character varying (128)     |
| paypal_details__capture_id                        | character varying (128)     |
| paypal_details__payer_email                       | character varying (128)     |
| paypal_details__payer_id                          | character varying (128)     |
| paypal_details__payer_status                      | character varying (128)     |
| paypal_details__payment_id                        | character varying (128)     |
| paypal_details__refund_id                         | character varying (128)     |
| paypal_details__seller_protection_status          | character varying (128)     |
| paypal_details__transaction_fee_amount            | character varying (128)     |
| paypal_details__transaction_fee_currency_iso_code | character varying (128)     |
| processor_response_code                           | character varying (128)     |
| processor_response_text                           | character varying (128)     |
| refunded_transaction_id                           | character varying (128)     |
| settlement_batch_id                               | character varying (128)     |
| status                                            | character varying (128)     |
| type                                              | character varying (128)     |
| updated_at                                        | timestamp without time zone |
| gateway_rejection_reason                          | character varying (128)     |

# pg_catalog



## pg_aggregate

| column_name   | data_type   |
|:--------------|:------------|
| aggfnoid      | regproc     |
| aggtransfn    | regproc     |
| aggfinalfn    | regproc     |
| aggtranstype  | oid         |
| agginitval    | text        |

## pg_am

| column_name     | data_type   |
|:----------------|:------------|
| amname          | name        |
| amowner         | integer     |
| amstrategies    | smallint    |
| amsupport       | smallint    |
| amorderstrategy | smallint    |
| amcanunique     | boolean     |
| amcanmulticol   | boolean     |
| amindexnulls    | boolean     |
| amconcurrent    | boolean     |
| amgettuple      | regproc     |
| aminsert        | regproc     |
| ambeginscan     | regproc     |
| amrescan        | regproc     |
| amendscan       | regproc     |
| ammarkpos       | regproc     |
| amrestrpos      | regproc     |
| ambuild         | regproc     |
| ambulkdelete    | regproc     |
| amvacuumcleanup | regproc     |
| amcostestimate  | regproc     |

## pg_amop

| column_name   | data_type   |
|:--------------|:------------|
| amopclaid     | oid         |
| amopsubtype   | oid         |
| amopstrategy  | smallint    |
| amopreqcheck  | boolean     |
| amopopr       | oid         |

## pg_amproc

| column_name   | data_type   |
|:--------------|:------------|
| amopclaid     | oid         |
| amprocsubtype | oid         |
| amprocnum     | smallint    |
| amproc        | regproc     |

## pg_attrdef

| column_name   | data_type   |
|:--------------|:------------|
| adrelid       | oid         |
| adnum         | smallint    |
| adbin         | text        |
| adsrc         | text        |

## pg_attribute

| column_name     | data_type   |
|:----------------|:------------|
| attrelid        | oid         |
| attname         | name        |
| atttypid        | oid         |
| attstattarget   | integer     |
| attlen          | smallint    |
| attnum          | smallint    |
| attndims        | integer     |
| attcacheoff     | integer     |
| atttypmod       | integer     |
| attbyval        | boolean     |
| attstorage      | "char"      |
| attalign        | "char"      |
| attnotnull      | boolean     |
| atthasdef       | boolean     |
| attisdropped    | boolean     |
| attislocal      | boolean     |
| attinhcount     | integer     |
| attisdistkey    | boolean     |
| attispreloaded  | boolean     |
| attsortkeyord   | integer     |
| attencodingtype | smallint    |
| attencrypttype  | smallint    |

## pg_attribute_info

| column_name     | data_type   |
|:----------------|:------------|
| attrelid        | oid         |
| attname         | name        |
| atttypid        | oid         |
| attstattarget   | integer     |
| attlen          | smallint    |
| attnum          | smallint    |
| attndims        | integer     |
| attcacheoff     | integer     |
| atttypmod       | integer     |
| attbyval        | boolean     |
| attstorage      | "char"      |
| attalign        | "char"      |
| attnotnull      | boolean     |
| atthasdef       | boolean     |
| attisdropped    | boolean     |
| attislocal      | boolean     |
| attinhcount     | integer     |
| attisdistkey    | boolean     |
| attispreloaded  | boolean     |
| attsortkeyord   | integer     |
| attencodingtype | smallint    |
| attencrypttype  | smallint    |
| attacl          | ARRAY       |

## pg_bar_ddllog

| column_name      | data_type   |
|:-----------------|:------------|
| bar_ddllog_dboid | oid         |
| bar_ddllog_xid   | xid         |
| bar_ddllog_txt   | text        |
| bar_ddllog_cmd   | smallint    |
| bar_ddllog_lng   | smallint    |
| bar_ddllog_oids  | text        |
| bar_ddllog_names | text        |
| bar_ddllog_path  | text        |

## pg_bar_repos

| column_name           | data_type   |
|:----------------------|:------------|
| bar_repos_name        | name        |
| bar_repos_leader_path | text        |
| bar_repos_data_paths  | ARRAY       |

## pg_bar_state

| column_name                  | data_type   |
|:-----------------------------|:------------|
| bar_state_dboid              | oid         |
| bar_state_backup_name        | text        |
| bar_state_restore_xid        | xid         |
| bar_state_restore_suspension | boolean     |

## pg_cast

| column_name   | data_type   |
|:--------------|:------------|
| castsource    | oid         |
| casttarget    | oid         |
| castfunc      | oid         |
| castcontext   | "char"      |

## pg_class

| column_name     | data_type   |
|:----------------|:------------|
| relname         | name        |
| relnamespace    | oid         |
| reltype         | oid         |
| relowner        | integer     |
| relam           | oid         |
| relfilenode     | oid         |
| reltablespace   | oid         |
| relpages        | integer     |
| reltuples       | real        |
| reltoastrelid   | oid         |
| reltoastidxid   | oid         |
| relhasindex     | boolean     |
| relisshared     | boolean     |
| relkind         | "char"      |
| relnatts        | smallint    |
| relexternid     | oid         |
| relisreplicated | boolean     |
| relispinned     | boolean     |
| reldiststyle    | smallint    |
| relprojbaseid   | oid         |
| relchecks       | smallint    |
| reltriggers     | smallint    |
| relukeys        | smallint    |
| relfkeys        | smallint    |
| relrefs         | smallint    |
| relhasoids      | boolean     |
| relhaspkey      | boolean     |
| relhasrules     | boolean     |
| relhassubclass  | boolean     |
| relacl          | ARRAY       |

## pg_class_info

| column_name           | data_type                   |
|:----------------------|:----------------------------|
| reldiststyle          | smallint                    |
| relprojbaseid         | oid                         |
| relchecks             | smallint                    |
| reltriggers           | smallint                    |
| relukeys              | smallint                    |
| relfkeys              | smallint                    |
| relrefs               | smallint                    |
| relhasoids            | boolean                     |
| relhaspkey            | boolean                     |
| relhasrules           | boolean                     |
| reloid                | oid                         |
| relname               | name                        |
| relnamespace          | oid                         |
| reltype               | oid                         |
| relowner              | integer                     |
| relam                 | oid                         |
| relfilenode           | oid                         |
| reltablespace         | oid                         |
| relpages              | integer                     |
| reltuples             | real                        |
| reltoastrelid         | oid                         |
| reltoastidxid         | oid                         |
| relhasindex           | boolean                     |
| relisshared           | boolean                     |
| relkind               | "char"                      |
| relnatts              | smallint                    |
| relexternid           | oid                         |
| relisreplicated       | boolean                     |
| relispinned           | boolean                     |
| relhassubclass        | boolean                     |
| relacl                | ARRAY                       |
| releffectivediststyle | smallint                    |
| relcreationtime       | timestamp without time zone |

## pg_conf

| column_name   | data_type   |
|:--------------|:------------|
| key           | name        |
| value         | text        |

## pg_constraint

| column_name   | data_type   |
|:--------------|:------------|
| conname       | name        |
| connamespace  | oid         |
| contype       | "char"      |
| condeferrable | boolean     |
| condeferred   | boolean     |
| conrelid      | oid         |
| contypid      | oid         |
| confrelid     | oid         |
| confupdtype   | "char"      |
| confdeltype   | "char"      |
| confmatchtype | "char"      |
| conkey        | ARRAY       |
| confkey       | ARRAY       |
| conbin        | text        |
| consrc        | text        |

## pg_conversion

| column_name    | data_type   |
|:---------------|:------------|
| conname        | name        |
| connamespace   | oid         |
| conowner       | integer     |
| conforencoding | integer     |
| contoencoding  | integer     |
| conproc        | regproc     |
| condefault     | boolean     |

## pg_database

| column_name   | data_type   |
|:--------------|:------------|
| datname       | name        |
| datdba        | integer     |
| encoding      | integer     |
| datistemplate | boolean     |
| datallowconn  | boolean     |
| datlastsysoid | oid         |
| datvacuumxid  | xid         |
| datfrozenxid  | xid         |
| dattablespace | oid         |
| datconfig     | ARRAY       |
| datacl        | ARRAY       |

## pg_default_acl

| column_name     | data_type   |
|:----------------|:------------|
| defacluser      | integer     |
| defaclnamespace | oid         |
| defaclobjtype   | "char"      |
| defaclacl       | ARRAY       |

## pg_depend

| column_name   | data_type   |
|:--------------|:------------|
| classid       | oid         |
| objid         | oid         |
| objsubid      | integer     |
| refclassid    | oid         |
| refobjid      | oid         |
| refobjsubid   | integer     |
| deptype       | "char"      |

## pg_description

| column_name   | data_type   |
|:--------------|:------------|
| objoid        | oid         |
| classoid      | oid         |
| objsubid      | integer     |
| description   | text        |

## pg_external_schema

| column_name   | data_type   |
|:--------------|:------------|
| esoid         | oid         |
| eskind        | smallint    |
| esdbname      | text        |
| esoptions     | text        |

## pg_group

| column_name   | data_type   |
|:--------------|:------------|
| groname       | name        |
| grosysid      | integer     |
| grolist       | ARRAY       |

## pg_highwatermark

| column_name   | data_type   |
|:--------------|:------------|
| hwmrelid      | oid         |
| hwmcolnum     | integer     |
| hwm           | integer     |

## pg_index

| column_name    | data_type   |
|:---------------|:------------|
| indexrelid     | oid         |
| indrelid       | oid         |
| indkey         | int2vector  |
| indclass       | oidvector   |
| indnatts       | smallint    |
| indisunique    | boolean     |
| indisprimary   | boolean     |
| indisclustered | boolean     |
| indexprs       | text        |
| indpred        | text        |

## pg_indexes

| column_name   | data_type   |
|:--------------|:------------|
| schemaname    | name        |
| tablename     | name        |
| indexname     | name        |
| tablespace    | name        |
| indexdef      | text        |

## pg_inherits

| column_name   | data_type   |
|:--------------|:------------|
| inhrelid      | oid         |
| inhparent     | oid         |
| inhseqno      | integer     |

## pg_language

| column_name   | data_type   |
|:--------------|:------------|
| lanname       | name        |
| lanispl       | boolean     |
| lanpltrusted  | boolean     |
| lanplcallfoid | oid         |
| lanvalidator  | oid         |
| lanacl        | ARRAY       |

## pg_largeobject

| column_name   | data_type   |
|:--------------|:------------|
| loid          | oid         |
| pageno        | integer     |
| data          | bytea       |

## pg_library

| column_name   | data_type   |
|:--------------|:------------|
| name          | name        |
| language_oid  | oid         |
| file_store_id | integer     |
| owner         | integer     |

## pg_listener

| column_name   | data_type   |
|:--------------|:------------|
| relname       | name        |
| listenerpid   | integer     |
| notification  | integer     |

## pg_locks

| column_name   | data_type         |
|:--------------|:------------------|
| relation      | oid               |
| database      | oid               |
| transaction   | xid               |
| pid           | integer           |
| mode          | character varying |
| granted       | boolean           |

## pg_namespace

| column_name   | data_type   |
|:--------------|:------------|
| nspname       | name        |
| nspowner      | integer     |
| nspacl        | ARRAY       |

## pg_opclass

| column_name   | data_type   |
|:--------------|:------------|
| opcamid       | oid         |
| opcname       | name        |
| opcnamespace  | oid         |
| opcowner      | integer     |
| opcintype     | oid         |
| opcdefault    | boolean     |
| opckeytype    | oid         |

## pg_operator

| column_name   | data_type   |
|:--------------|:------------|
| oprname       | name        |
| oprnamespace  | oid         |
| oprowner      | integer     |
| oprkind       | "char"      |
| oprcanhash    | boolean     |
| oprleft       | oid         |
| oprright      | oid         |
| oprresult     | oid         |
| oprcom        | oid         |
| oprnegate     | oid         |
| oprlsortop    | oid         |
| oprrsortop    | oid         |
| oprltcmpop    | oid         |
| oprgtcmpop    | oid         |
| oprcode       | regproc     |
| oprrest       | regproc     |
| oprjoin       | regproc     |

## pg_proc

| column_name   | data_type   |
|:--------------|:------------|
| proname       | name        |
| pronamespace  | oid         |
| proowner      | integer     |
| prolang       | oid         |
| proisagg      | boolean     |
| prosecdef     | boolean     |
| proisstrict   | boolean     |
| proretset     | boolean     |
| provolatile   | "char"      |
| pronargs      | smallint    |
| prorettype    | oid         |
| proargtypes   | oidvector   |
| proargnames   | ARRAY       |
| prosrc        | text        |
| probin        | bytea       |
| proacl        | ARRAY       |

## pg_proc_info

| column_name    | data_type   |
|:---------------|:------------|
| prooid         | oid         |
| proname        | name        |
| pronamespace   | oid         |
| proowner       | integer     |
| prolang        | oid         |
| prokind        | "char"      |
| proisagg       | boolean     |
| prosecdef      | boolean     |
| proisstrict    | boolean     |
| proretset      | boolean     |
| provolatile    | "char"      |
| pronargs       | smallint    |
| prorettype     | oid         |
| proargtypes    | oidvector   |
| proargmodes    | ARRAY       |
| proallargtypes | ARRAY       |
| proargnames    | ARRAY       |
| prosrc         | text        |
| probin         | bytea       |
| proacl         | ARRAY       |

## pg_resize

| column_name   | data_type                |
|:--------------|:-------------------------|
| statexml      | character varying (8192) |

## pg_rewrite

| column_name   | data_type   |
|:--------------|:------------|
| rulename      | name        |
| ev_class      | oid         |
| ev_attr       | smallint    |
| ev_type       | "char"      |
| is_instead    | boolean     |
| ev_qual       | text        |
| ev_action     | text        |

## pg_rules

| column_name   | data_type   |
|:--------------|:------------|
| schemaname    | name        |
| tablename     | name        |
| rulename      | name        |
| definition    | text        |

## pg_settings

| column_name   | data_type         |
|:--------------|:------------------|
| name          | character varying |
| setting       | character varying |
| category      | character varying |
| short_desc    | character varying |
| extra_desc    | character varying |
| context       | character varying |
| vartype       | character varying |
| source        | character varying |
| min_val       | character varying |
| max_val       | character varying |

## pg_shdepend

| column_name   | data_type   |
|:--------------|:------------|
| dbid          | oid         |
| classid       | oid         |
| objid         | oid         |
| objsubid      | integer     |
| refclassid    | oid         |
| refobjid      | oid         |
| deptype       | "char"      |

## pg_stat_activity

| column_name   | data_type                |
|:--------------|:-------------------------|
| datid         | oid                      |
| datname       | name                     |
| procpid       | integer                  |
| usesysid      | integer                  |
| usename       | name                     |
| current_query | text                     |
| query_start   | timestamp with time zone |

## pg_stat_all_indexes

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| indexrelid    | oid         |
| schemaname    | name        |
| relname       | name        |
| indexrelname  | name        |
| idx_scan      | bigint      |
| idx_tup_read  | bigint      |
| idx_tup_fetch | bigint      |

## pg_stat_all_tables

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| schemaname    | name        |
| relname       | name        |
| seq_scan      | bigint      |
| seq_tup_read  | bigint      |
| idx_scan      | bigint      |
| idx_tup_fetch | bigint      |
| n_tup_ins     | bigint      |
| n_tup_upd     | bigint      |
| n_tup_del     | bigint      |

## pg_stat_database

| column_name   | data_type   |
|:--------------|:------------|
| datid         | oid         |
| datname       | name        |
| numbackends   | integer     |
| xact_commit   | bigint      |
| xact_rollback | bigint      |
| blks_read     | bigint      |
| blks_hit      | bigint      |

## pg_stat_sys_indexes

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| indexrelid    | oid         |
| schemaname    | name        |
| relname       | name        |
| indexrelname  | name        |
| idx_scan      | bigint      |
| idx_tup_read  | bigint      |
| idx_tup_fetch | bigint      |

## pg_stat_sys_tables

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| schemaname    | name        |
| relname       | name        |
| seq_scan      | bigint      |
| seq_tup_read  | bigint      |
| idx_scan      | bigint      |
| idx_tup_fetch | bigint      |
| n_tup_ins     | bigint      |
| n_tup_upd     | bigint      |
| n_tup_del     | bigint      |

## pg_stat_user_indexes

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| indexrelid    | oid         |
| schemaname    | name        |
| relname       | name        |
| indexrelname  | name        |
| idx_scan      | bigint      |
| idx_tup_read  | bigint      |
| idx_tup_fetch | bigint      |

## pg_stat_user_tables

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| schemaname    | name        |
| relname       | name        |
| seq_scan      | bigint      |
| seq_tup_read  | bigint      |
| idx_scan      | bigint      |
| idx_tup_fetch | bigint      |
| n_tup_ins     | bigint      |
| n_tup_upd     | bigint      |
| n_tup_del     | bigint      |

## pg_statio_all_indexes

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| indexrelid    | oid         |
| schemaname    | name        |
| relname       | name        |
| indexrelname  | name        |
| idx_blks_read | bigint      |
| idx_blks_hit  | bigint      |

## pg_statio_all_sequences

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| schemaname    | name        |
| relname       | name        |
| blks_read     | bigint      |
| blks_hit      | bigint      |

## pg_statio_all_tables

| column_name     | data_type   |
|:----------------|:------------|
| relid           | oid         |
| schemaname      | name        |
| relname         | name        |
| heap_blks_read  | bigint      |
| heap_blks_hit   | bigint      |
| idx_blks_read   | bigint      |
| idx_blks_hit    | bigint      |
| toast_blks_read | bigint      |
| toast_blks_hit  | bigint      |
| tidx_blks_read  | bigint      |
| tidx_blks_hit   | bigint      |

## pg_statio_sys_indexes

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| indexrelid    | oid         |
| schemaname    | name        |
| relname       | name        |
| indexrelname  | name        |
| idx_blks_read | bigint      |
| idx_blks_hit  | bigint      |

## pg_statio_sys_sequences

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| schemaname    | name        |
| relname       | name        |
| blks_read     | bigint      |
| blks_hit      | bigint      |

## pg_statio_sys_tables

| column_name     | data_type   |
|:----------------|:------------|
| relid           | oid         |
| schemaname      | name        |
| relname         | name        |
| heap_blks_read  | bigint      |
| heap_blks_hit   | bigint      |
| idx_blks_read   | bigint      |
| idx_blks_hit    | bigint      |
| toast_blks_read | bigint      |
| toast_blks_hit  | bigint      |
| tidx_blks_read  | bigint      |
| tidx_blks_hit   | bigint      |

## pg_statio_user_indexes

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| indexrelid    | oid         |
| schemaname    | name        |
| relname       | name        |
| indexrelname  | name        |
| idx_blks_read | bigint      |
| idx_blks_hit  | bigint      |

## pg_statio_user_sequences

| column_name   | data_type   |
|:--------------|:------------|
| relid         | oid         |
| schemaname    | name        |
| relname       | name        |
| blks_read     | bigint      |
| blks_hit      | bigint      |

## pg_statio_user_tables

| column_name     | data_type   |
|:----------------|:------------|
| relid           | oid         |
| schemaname      | name        |
| relname         | name        |
| heap_blks_read  | bigint      |
| heap_blks_hit   | bigint      |
| idx_blks_read   | bigint      |
| idx_blks_hit    | bigint      |
| toast_blks_read | bigint      |
| toast_blks_hit  | bigint      |
| tidx_blks_read  | bigint      |
| tidx_blks_hit   | bigint      |

## pg_statistic_indicator

| column_name   | data_type   |
|:--------------|:------------|
| stairelid     | oid         |
| stairows      | real        |
| staiins       | real        |
| staidels      | real        |

## pg_statistic_multicol

| column_name   | data_type   |
|:--------------|:------------|
| stamcname     | name        |
| stamcrelid    | oid         |
| stamcdistinct | real        |
| stamcattnum   | ARRAY       |

## pg_stats

| column_name       | data_type   |
|:------------------|:------------|
| schemaname        | name        |
| tablename         | name        |
| attname           | name        |
| null_frac         | real        |
| avg_width         | integer     |
| n_distinct        | real        |
| most_common_vals  | anyarray    |
| most_common_freqs | ARRAY       |
| histogram_bounds  | anyarray    |
| correlation       | real        |

## pg_table_def

| column_name   | data_type      |
|:--------------|:---------------|
| schemaname    | name           |
| tablename     | name           |
| column        | name           |
| type          | text           |
| encoding      | character (32) |
| distkey       | boolean        |
| sortkey       | integer        |
| notnull       | boolean        |

## pg_tables

| column_name   | data_type   |
|:--------------|:------------|
| schemaname    | name        |
| tablename     | name        |
| tableowner    | name        |
| tablespace    | name        |
| hasindexes    | boolean     |
| hasrules      | boolean     |
| hastriggers   | boolean     |

## pg_tablespace

| column_name   | data_type   |
|:--------------|:------------|
| spcname       | name        |
| spcowner      | integer     |
| spclocation   | text        |
| spcacl        | ARRAY       |

## pg_test

| column_name   | data_type                   |
|:--------------|:----------------------------|
| tesbool       | boolean                     |
| tesenum       | "char"                      |
| tesfloat4     | real                        |
| tesint2       | smallint                    |
| tesint4       | integer                     |
| tesint8       | bigint                      |
| tesname       | name                        |
| tesoid        | oid                         |
| tesregproc    | regproc                     |
| testext       | character varying (256)     |
| tesvarchar    | character varying (256)     |
| tesxid        | xid                         |
| teschar       | character (13)              |
| tesdate       | date                        |
| tesfloat8     | double precision            |
| tesinterval   | interval                    |
| tesnumeric    | numeric                     |
| testid        | tid                         |
| testimestamp  | timestamp without time zone |

## pg_trigger

| column_name    | data_type   |
|:---------------|:------------|
| tgrelid        | oid         |
| tgname         | name        |
| tgfoid         | oid         |
| tgtype         | smallint    |
| tgenabled      | boolean     |
| tgisconstraint | boolean     |
| tgconstrname   | name        |
| tgconstrrelid  | oid         |
| tgdeferrable   | boolean     |
| tginitdeferred | boolean     |
| tgnargs        | smallint    |
| tgattr         | int2vector  |
| tgargs         | bytea       |

## pg_type

| column_name   | data_type   |
|:--------------|:------------|
| typname       | name        |
| typnamespace  | oid         |
| typowner      | integer     |
| typlen        | smallint    |
| typbyval      | boolean     |
| typtype       | "char"      |
| typisdefined  | boolean     |
| typdelim      | "char"      |
| typrelid      | oid         |
| typelem       | oid         |
| typinput      | regproc     |
| typoutput     | regproc     |
| typreceive    | regproc     |
| typsend       | regproc     |
| typanalyze    | regproc     |
| typalign      | "char"      |
| typstorage    | "char"      |
| typnotnull    | boolean     |
| typbasetype   | oid         |
| typtypmod     | integer     |
| typndims      | integer     |
| typdefaultbin | text        |
| typdefault    | text        |

## pg_udf

| column_name   | data_type   |
|:--------------|:------------|
| udfid         | oid         |
| nargs         | smallint    |
| argtypmods    | oidvector   |
| rettypmod     | integer     |
| version       | integer     |
| funcname      | text        |

## pg_user

| column_name   | data_type             |
|:--------------|:----------------------|
| usename       | name                  |
| usesysid      | integer               |
| usecreatedb   | boolean               |
| usesuper      | boolean               |
| usecatupd     | boolean               |
| passwd        | character varying (8) |
| valuntil      | abstime               |
| useconfig     | ARRAY                 |

## pg_views

| column_name   | data_type   |
|:--------------|:------------|
| schemaname    | name        |
| viewname      | name        |
| viewowner     | name        |
| definition    | text        |

## stl_aggr

| column_name          | data_type                   |
|:---------------------|:----------------------------|
| userid               | integer                     |
| query                | integer                     |
| slice                | integer                     |
| segment              | integer                     |
| step                 | integer                     |
| starttime            | timestamp without time zone |
| endtime              | timestamp without time zone |
| tasknum              | integer                     |
| rows                 | bigint                      |
| bytes                | bigint                      |
| slots                | integer                     |
| occupied             | integer                     |
| maxlength            | integer                     |
| tbl                  | integer                     |
| is_diskbased         | character (1)               |
| workmem              | bigint                      |
| type                 | character (6)               |
| resizes              | integer                     |
| flushable            | integer                     |
| varchar_agg_opt      | character (1)               |
| varagg_opt_bitmap    | bigint                      |
| used_agg_prefetching | character (1)               |

## stl_alert_event_log

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| pid           | integer                     |
| xid           | bigint                      |
| event         | character (1024)            |
| solution      | character (200)             |
| event_time    | timestamp without time zone |

## stl_analyze_compression

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| start_time    | timestamp without time zone |
| xid           | bigint                      |
| tbl           | integer                     |
| tablename     | character (128)             |
| col           | integer                     |
| old_encoding  | character (15)              |
| new_encoding  | character (15)              |
| mode          | character (14)              |

## stl_bcast

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| bytes         | bigint                      |
| packets       | integer                     |

## stl_ddltext

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| xid           | bigint                      |
| pid           | integer                     |
| label         | character (320)             |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| sequence      | integer                     |
| text          | character (200)             |

## stl_delete

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| tbl           | integer                     |

## stl_dist

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| userid             | integer                     |
| query              | integer                     |
| slice              | integer                     |
| segment            | integer                     |
| step               | integer                     |
| starttime          | timestamp without time zone |
| endtime            | timestamp without time zone |
| tasknum            | integer                     |
| rows               | bigint                      |
| bytes              | bigint                      |
| packets            | integer                     |
| topology_signature | bigint                      |
| is_intra_node      | character (1)               |
| distribute_by_lds  | character (1)               |

## stl_error

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| process       | character (32)              |
| recordtime    | timestamp without time zone |
| pid           | integer                     |
| errcode       | integer                     |
| file          | character (90)              |
| linenum       | integer                     |
| context       | character (100)             |
| error         | character (512)             |

## stl_explain

| column_name   | data_type       |
|:--------------|:----------------|
| userid        | integer         |
| query         | integer         |
| nodeid        | integer         |
| parentid      | integer         |
| plannode      | character (400) |
| info          | character (400) |

## stl_file_scan

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| name          | character (90)              |
| lines         | bigint                      |
| bytes         | bigint                      |
| loadtime      | bigint                      |
| curtime       | timestamp without time zone |
| is_partial    | integer                     |
| start_offset  | bigint                      |

## stl_hash

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| userid                  | integer                     |
| query                   | integer                     |
| slice                   | integer                     |
| segment                 | integer                     |
| step                    | integer                     |
| starttime               | timestamp without time zone |
| endtime                 | timestamp without time zone |
| tasknum                 | integer                     |
| rows                    | bigint                      |
| bytes                   | bigint                      |
| slots                   | integer                     |
| occupied                | integer                     |
| maxlength               | integer                     |
| tbl                     | integer                     |
| is_diskbased            | character (1)               |
| workmem                 | bigint                      |
| num_parts               | integer                     |
| est_rows                | bigint                      |
| num_blocks_permitted    | integer                     |
| resizes                 | integer                     |
| checksum                | bigint                      |
| used_prefetching        | character (1)               |
| runtime_filter_size     | integer                     |
| max_runtime_filter_size | integer                     |
| runtime_filter_type     | integer                     |
| is_hashed_subplan       | character (1)               |
| build_global_hash_table | character (1)               |

## stl_hashjoin

| column_name      | data_type                   |
|:-----------------|:----------------------------|
| userid           | integer                     |
| query            | integer                     |
| slice            | integer                     |
| segment          | integer                     |
| step             | integer                     |
| starttime        | timestamp without time zone |
| endtime          | timestamp without time zone |
| tasknum          | integer                     |
| rows             | bigint                      |
| tbl              | integer                     |
| num_parts        | integer                     |
| join_type        | integer                     |
| hash_looped      | character (1)               |
| switched_parts   | character (1)               |
| used_prefetching | character (1)               |
| hash_segment     | integer                     |
| hash_step        | integer                     |
| checksum         | bigint                      |
| distribution     | integer                     |
| is_externalized  | character (1)               |

## stl_insert

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| tbl           | integer                     |

## stl_limit

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| hit_limit     | character (1)               |
| checksum      | bigint                      |

## stl_load_commits

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| name          | character (256)             |
| filename      | character (256)             |
| byte_offset   | integer                     |
| lines_scanned | integer                     |
| errors        | integer                     |
| status        | integer                     |
| curtime       | timestamp without time zone |
| file_format   | character (16)              |
| is_partial    | integer                     |
| start_offset  | bigint                      |

## stl_load_errors

| column_name     | data_type                   |
|:----------------|:----------------------------|
| userid          | integer                     |
| slice           | integer                     |
| tbl             | integer                     |
| starttime       | timestamp without time zone |
| session         | integer                     |
| query           | integer                     |
| filename        | character (256)             |
| line_number     | bigint                      |
| colname         | character (127)             |
| type            | character (10)              |
| col_length      | character (10)              |
| position        | integer                     |
| raw_line        | character (1024)            |
| raw_field_value | character (1024)            |
| err_code        | integer                     |
| err_reason      | character (100)             |
| is_partial      | integer                     |
| start_offset    | bigint                      |

## stl_load_trace

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| recordtime    | timestamp without time zone |
| pid           | integer                     |
| name          | character (256)             |
| bytes_scanned | bigint                      |
| message       | character (256)             |

## stl_loaderror_detail

| column_name   | data_type        |
|:--------------|:-----------------|
| userid        | integer          |
| slice         | integer          |
| session       | integer          |
| query         | integer          |
| filename      | character (256)  |
| line_number   | bigint           |
| field         | integer          |
| colname       | character (1024) |
| value         | character (1024) |
| is_null       | integer          |
| type          | character (10)   |
| col_length    | character (10)   |

## stl_merge

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |

## stl_mergejoin

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| tbl           | integer                     |
| checksum      | bigint                      |
| distribution  | integer                     |

## stl_metadata_step

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| tbl           | integer                     |

## stl_multi_statement_violations

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| database      | character (32)              |
| cmdname       | character (20)              |
| xid           | bigint                      |
| pid           | integer                     |
| eventtime     | timestamp without time zone |
| padb_version  | character (50)              |

## stl_mv_state

| column_name       | data_type                   |
|:------------------|:----------------------------|
| userid            | bigint                      |
| starttime         | timestamp without time zone |
| xid               | bigint                      |
| event_desc        | character (500)             |
| db_name           | character (128)             |
| base_table_schema | character (128)             |
| base_table_name   | character (128)             |
| mv_schema         | character (128)             |
| mv_name           | character (128)             |
| state             | character (32)              |

## stl_nestloop

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| tbl           | integer                     |
| checksum      | bigint                      |
| distribution  | integer                     |

## stl_parse

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |

## stl_plan_info

| column_name   | data_type        |
|:--------------|:-----------------|
| userid        | integer          |
| query         | integer          |
| nodeid        | integer          |
| segment       | integer          |
| step          | integer          |
| locus         | integer          |
| plannode      | integer          |
| startupcost   | double precision |
| totalcost     | double precision |
| rows          | bigint           |
| bytes         | bigint           |

## stl_project

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| checksum      | bigint                      |

## stl_query

| column_name                | data_type                   |
|:---------------------------|:----------------------------|
| userid                     | integer                     |
| query                      | integer                     |
| label                      | character (320)             |
| xid                        | bigint                      |
| pid                        | integer                     |
| database                   | character (32)              |
| querytxt                   | character (4000)            |
| starttime                  | timestamp without time zone |
| endtime                    | timestamp without time zone |
| aborted                    | integer                     |
| insert_pristine            | integer                     |
| concurrency_scaling_status | integer                     |

## stl_query_metrics

| column_name         | data_type                   |
|:--------------------|:----------------------------|
| userid              | integer                     |
| service_class       | integer                     |
| query               | integer                     |
| segment             | integer                     |
| step_type           | integer                     |
| starttime           | timestamp without time zone |
| slices              | integer                     |
| max_rows            | bigint                      |
| rows                | bigint                      |
| max_cpu_time        | bigint                      |
| cpu_time            | bigint                      |
| max_blocks_read     | integer                     |
| blocks_read         | bigint                      |
| max_run_time        | bigint                      |
| run_time            | bigint                      |
| max_blocks_to_disk  | bigint                      |
| blocks_to_disk      | bigint                      |
| step                | integer                     |
| max_query_scan_size | bigint                      |
| query_scan_size     | bigint                      |
| query_priority      | integer                     |
| query_queue_time    | bigint                      |
| service_class_name  | character (64)              |

## stl_querytext

| column_name   | data_type       |
|:--------------|:----------------|
| userid        | integer         |
| xid           | bigint          |
| pid           | integer         |
| query         | integer         |
| sequence      | integer         |
| text          | character (200) |

## stl_replacements

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| slice         | integer                     |
| tbl           | integer                     |
| starttime     | timestamp without time zone |
| session       | integer                     |
| query         | integer                     |
| filename      | character (256)             |
| line_number   | bigint                      |
| colname       | character (127)             |
| raw_line      | character (1024)            |

## stl_restarted_sessions

| column_name   | data_type                   |
|:--------------|:----------------------------|
| currenttime   | timestamp without time zone |
| dbname        | character (50)              |
| newpid        | integer                     |
| oldpid        | integer                     |
| username      | character (50)              |
| remotehost    | character (32)              |
| remoteport    | character (32)              |
| parkedtime    | timestamp without time zone |
| session_vars  | character (2000)            |

## stl_return

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| bytes         | bigint                      |
| packets       | integer                     |

## stl_rpc

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| requests      | bigint                      |
| rpc_type      | integer                     |

## stl_rtree

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| userid                  | integer                     |
| query                   | integer                     |
| slice                   | integer                     |
| segment                 | integer                     |
| step                    | integer                     |
| starttime               | timestamp without time zone |
| endtime                 | timestamp without time zone |
| tasknum                 | integer                     |
| rows                    | bigint                      |
| bytes                   | bigint                      |
| tbl                     | integer                     |
| is_diskbased            | character (1)               |
| workmem                 | bigint                      |
| mem_allowance_for_index | bigint                      |
| cheating_mem            | bigint                      |
| index_predicted_size    | bigint                      |
| index_size              | bigint                      |

## stl_s3client

| column_name                         | data_type                   |
|:------------------------------------|:----------------------------|
| userid                              | integer                     |
| query                               | integer                     |
| slice                               | integer                     |
| recordtime                          | timestamp without time zone |
| pid                                 | integer                     |
| http_method                         | character (64)              |
| s3_action                           | character (64)              |
| bucket                              | character (64)              |
| key                                 | character (256)             |
| transfer_size                       | bigint                      |
| data_size                           | bigint                      |
| start_time                          | bigint                      |
| end_time                            | bigint                      |
| transfer_time                       | bigint                      |
| compression_time                    | bigint                      |
| connect_time                        | bigint                      |
| app_connect_time                    | bigint                      |
| retries                             | integer                     |
| request_id                          | character (32)              |
| extended_request_id                 | character (128)             |
| ip_address                          | character (64)              |
| thread_id                           | integer                     |
| original_start_time_us              | bigint                      |
| total_transfer_time_us_with_retries | bigint                      |
| node                                | integer                     |
| tbl_id                              | integer                     |
| one_perm_rep                        | integer                     |
| flags                               | integer                     |
| s3_write_type                       | character (64)              |
| is_s3commit_write                   | integer                     |
| is_partial                          | integer                     |
| start_offset                        | bigint                      |
| col_num                             | integer                     |
| is_wam_write                        | integer                     |
| wam_precommit_id                    | integer                     |

## stl_s3client_error

| column_name       | data_type                   |
|:------------------|:----------------------------|
| userid            | integer                     |
| query             | integer                     |
| sliceid           | integer                     |
| recordtime        | timestamp without time zone |
| pid               | integer                     |
| http_method       | character (64)              |
| s3_action         | character (64)              |
| bucket            | character (64)              |
| key               | character (256)             |
| error             | character (1024)            |
| retry_count       | integer                     |
| curl_code         | integer                     |
| start_time        | timestamp without time zone |
| resolve_time      | bigint                      |
| connect_time      | bigint                      |
| transfer_size     | bigint                      |
| node              | integer                     |
| table_id          | integer                     |
| one_perm_rep      | integer                     |
| flags             | integer                     |
| s3_write_type     | character (64)              |
| is_s3commit_write | integer                     |
| is_partial        | integer                     |
| start_offset      | bigint                      |
| col_num           | integer                     |
| is_wam_write      | integer                     |
| wam_precommit_id  | integer                     |

## stl_save

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| bytes         | bigint                      |
| tbl           | integer                     |
| is_diskbased  | character (1)               |
| workmem       | bigint                      |
| is_save_rtf   | character (1)               |

## stl_scan

| column_name               | data_type                   |
|:--------------------------|:----------------------------|
| userid                    | integer                     |
| query                     | integer                     |
| slice                     | integer                     |
| segment                   | integer                     |
| step                      | integer                     |
| starttime                 | timestamp without time zone |
| endtime                   | timestamp without time zone |
| tasknum                   | integer                     |
| rows                      | bigint                      |
| bytes                     | bigint                      |
| fetches                   | bigint                      |
| type                      | integer                     |
| tbl                       | integer                     |
| is_rrscan                 | character (1)               |
| is_delayed_scan           | character (1)               |
| rows_pre_filter           | bigint                      |
| rows_pre_user_filter      | bigint                      |
| perm_table_name           | character (136)             |
| is_rlf_scan               | character (1)               |
| is_rlf_scan_reason        | integer                     |
| num_em_blocks             | integer                     |
| checksum                  | bigint                      |
| runtime_filtering         | character (1)               |
| scan_region               | integer                     |
| num_sortkey_as_predicate  | integer                     |
| row_fetcher_state         | integer                     |
| consumed_scan_ranges      | bigint                      |
| work_stealing_reason      | bigint                      |
| is_vectorized_scan        | character (1)               |
| is_vectorized_scan_reason | integer                     |
| row_fetcher_reason        | bigint                      |
| topology_signature        | bigint                      |

## stl_schema_quota_violations

| column_name     | data_type                   |
|:----------------|:----------------------------|
| ownerid         | integer                     |
| userid          | integer                     |
| xid             | bigint                      |
| pid             | integer                     |
| schema_id       | integer                     |
| schema_name     | character (128)             |
| quota           | integer                     |
| disk_usage      | integer                     |
| disk_usage_pct  | double precision            |
| timestamp       | timestamp without time zone |
| disk_usage_diff | integer                     |

## stl_sessions

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| process       | integer                     |
| user_name     | character (50)              |
| db_name       | character (50)              |
| timeout_sec   | integer                     |
| timed_out     | integer                     |

## stl_sort

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| bytes         | bigint                      |
| tbl           | integer                     |
| is_diskbased  | character (1)               |
| workmem       | bigint                      |
| checksum      | bigint                      |

## stl_spatial_join

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| rtree_segment | integer                     |
| rtree_step    | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| predicate     | integer                     |
| rows          | bigint                      |
| pruned_tuples | bigint                      |
| tbl           | integer                     |
| checksum      | bigint                      |
| distribution  | integer                     |

## stl_spectrum_scan_error

| column_name    | data_type        |
|:---------------|:-----------------|
| userid         | integer          |
| query          | integer          |
| location       | character (256)  |
| rowid          | character (2100) |
| colname        | character (127)  |
| original_value | character (1024) |
| modified_value | character (1024) |
| trigger        | integer          |
| action         | integer          |
| action_value   | character (127)  |
| error_code     | integer          |

## stl_sshclient_error

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| sliceid       | integer                     |
| recordtime    | timestamp without time zone |
| pid           | integer                     |
| ssh_username  | character (1024)            |
| endpoint      | character (1024)            |
| command       | character (4096)            |
| error         | character (1024)            |

## stl_stream_segs

| column_name   | data_type   |
|:--------------|:------------|
| userid        | integer     |
| query         | integer     |
| stream        | integer     |
| segment       | integer     |

## stl_udf_compile_error

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| recordtime    | timestamp without time zone |
| pid           | integer                     |
| error         | character (4096)            |

## stl_undone

| column_name    | data_type                   |
|:---------------|:----------------------------|
| userid         | integer                     |
| xact_id        | bigint                      |
| xact_id_undone | bigint                      |
| undo_start_ts  | timestamp without time zone |
| undo_end_ts    | timestamp without time zone |
| table_id       | bigint                      |

## stl_unique

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| userid                  | integer                     |
| query                   | integer                     |
| slice                   | integer                     |
| segment                 | integer                     |
| step                    | integer                     |
| starttime               | timestamp without time zone |
| endtime                 | timestamp without time zone |
| tasknum                 | integer                     |
| rows                    | bigint                      |
| type                    | character (6)               |
| is_diskbased            | character (1)               |
| slots                   | integer                     |
| workmem                 | bigint                      |
| max_buffers_used        | bigint                      |
| resizes                 | integer                     |
| occupied                | integer                     |
| flushable               | integer                     |
| used_unique_prefetching | character (1)               |

## stl_unload_log

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| pid           | integer                     |
| path          | character (1280)            |
| start_time    | timestamp without time zone |
| end_time      | timestamp without time zone |
| line_count    | bigint                      |
| transfer_size | bigint                      |
| file_format   | character (10)              |

## stl_utilitytext

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| xid           | bigint                      |
| pid           | integer                     |
| label         | character (320)             |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| sequence      | integer                     |
| text          | character (200)             |

## stl_warning

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| process       | character (32)              |
| pid           | integer                     |
| recordtime    | timestamp without time zone |
| file          | character (20)              |
| linenum       | integer                     |
| bug_desc      | character (512)             |

## stl_window

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| tasknum       | integer                     |
| rows          | bigint                      |
| is_diskbased  | character (1)               |
| workmem       | bigint                      |

## stl_wlm_error

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| recordtime    | timestamp without time zone |
| pid           | integer                     |
| error_string  | character (256)             |

## stl_wlm_query

| column_name              | data_type                   |
|:-------------------------|:----------------------------|
| userid                   | integer                     |
| xid                      | bigint                      |
| task                     | integer                     |
| query                    | integer                     |
| service_class            | integer                     |
| slot_count               | integer                     |
| service_class_start_time | timestamp without time zone |
| queue_start_time         | timestamp without time zone |
| queue_end_time           | timestamp without time zone |
| total_queue_time         | bigint                      |
| exec_start_time          | timestamp without time zone |
| exec_end_time            | timestamp without time zone |
| total_exec_time          | bigint                      |
| service_class_end_time   | timestamp without time zone |
| final_state              | character (16)              |
| est_peak_mem             | bigint                      |
| query_priority           | character (20)              |
| service_class_name       | character (64)              |

## stl_wlm_rule_action

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| userid             | integer                     |
| query              | integer                     |
| service_class      | integer                     |
| rule               | character (256)             |
| action             | character (256)             |
| recordtime         | timestamp without time zone |
| action_value       | character (256)             |
| service_class_name | character (64)              |

## stv_active_cursors

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| name          | character (256)             |
| xid           | bigint                      |
| pid           | integer                     |
| starttime     | timestamp without time zone |
| row_count     | bigint                      |
| byte_count    | bigint                      |
| fetched_rows  | bigint                      |

## stv_exec_state

| column_name     | data_type                   |
|:----------------|:----------------------------|
| userid          | integer                     |
| query           | integer                     |
| slice           | integer                     |
| segment         | integer                     |
| step            | integer                     |
| starttime       | timestamp without time zone |
| currenttime     | timestamp without time zone |
| tasknum         | integer                     |
| rows            | bigint                      |
| bytes           | bigint                      |
| label           | character (256)             |
| is_diskbased    | character (1)               |
| workmem         | bigint                      |
| num_parts       | integer                     |
| is_rrscan       | character (1)               |
| is_delayed_scan | character (1)               |

## stv_inflight

| column_name                | data_type                   |
|:---------------------------|:----------------------------|
| userid                     | integer                     |
| slice                      | integer                     |
| query                      | integer                     |
| label                      | character (320)             |
| xid                        | bigint                      |
| pid                        | integer                     |
| starttime                  | timestamp without time zone |
| text                       | character (100)             |
| suspended                  | integer                     |
| insert_pristine            | integer                     |
| concurrency_scaling_status | integer                     |

## stv_load_state

| column_name              | data_type                   |
|:-------------------------|:----------------------------|
| userid                   | integer                     |
| session                  | integer                     |
| query                    | integer                     |
| slice                    | integer                     |
| pid                      | integer                     |
| recordtime               | timestamp without time zone |
| bytes_to_load            | bigint                      |
| bytes_loaded             | bigint                      |
| bytes_to_load_compressed | bigint                      |
| bytes_loaded_compressed  | bigint                      |
| lines                    | bigint                      |
| num_files                | integer                     |
| num_files_complete       | integer                     |
| current_file             | character (256)             |
| pct_complete             | integer                     |

## stv_logical_data_slices

| column_name             | data_type   |
|:------------------------|:------------|
| slice                   | integer     |
| num_data_slices         | integer     |
| num_logical_data_slices | integer     |

## stv_ml_model_info

| column_name    | data_type       |
|:---------------|:----------------|
| schema_name    | character (128) |
| user_name      | character (128) |
| model_name     | character (128) |
| life_cycle     | character (20)  |
| is_refreshable | integer         |
| model_state    | character (128) |

## stv_mv_deps

| column_name   | data_type       |
|:--------------|:----------------|
| db_name       | character (128) |
| schema        | character (128) |
| name          | character (128) |
| ref_schema    | character (128) |
| ref_name      | character (128) |

## stv_mv_info

| column_name      | data_type       |
|:-----------------|:----------------|
| db_name          | character (128) |
| schema           | character (128) |
| name             | character (128) |
| updated_upto_xid | bigint          |
| is_stale         | character (1)   |
| owner_user_name  | character (128) |
| state            | integer         |
| autorefresh      | character (1)   |
| autorewrite      | character (1)   |

## stv_proc_stat

| column_name   | data_type      |
|:--------------|:---------------|
| node          | integer        |
| pid           | integer        |
| elapsed       | integer        |
| query         | integer        |
| slice         | integer        |
| segment       | integer        |
| tasknum       | integer        |
| utime         | bigint         |
| stime         | bigint         |
| memory        | bigint         |
| name          | character (50) |
| state         | character (1)  |
| ppid          | integer        |
| pgrp          | integer        |
| session       | integer        |
| tty_nr        | integer        |
| tpgid         | integer        |
| flags         | bigint         |
| minflt        | bigint         |
| cminflt       | bigint         |
| majflt        | bigint         |
| cmajflt       | bigint         |
| cutime        | bigint         |
| cstime        | bigint         |
| priority      | bigint         |
| nice          | bigint         |
| ignore0       | bigint         |
| itrealvalue   | bigint         |
| starttime     | bigint         |
| vsize         | bigint         |
| rss           | bigint         |
| rlim          | bigint         |
| startcode     | bigint         |
| endcode       | bigint         |
| startstack    | bigint         |
| kstkesp       | bigint         |
| kstkeip       | bigint         |
| signal        | bigint         |
| blocked       | bigint         |
| sigignore     | bigint         |
| sigcatch      | bigint         |
| wchan         | bigint         |
| nswap         | bigint         |
| cnswap        | bigint         |
| exit_signal   | integer        |
| processor     | integer        |

## stv_query_metrics

| column_name         | data_type                   |
|:--------------------|:----------------------------|
| userid              | integer                     |
| service_class       | integer                     |
| query               | integer                     |
| segment             | integer                     |
| step_type           | integer                     |
| starttime           | timestamp without time zone |
| slices              | integer                     |
| max_rows            | bigint                      |
| rows                | bigint                      |
| max_cpu_time        | bigint                      |
| cpu_time            | bigint                      |
| max_blocks_read     | integer                     |
| blocks_read         | bigint                      |
| max_run_time        | bigint                      |
| run_time            | bigint                      |
| max_blocks_to_disk  | bigint                      |
| blocks_to_disk      | bigint                      |
| step                | integer                     |
| max_query_scan_size | bigint                      |
| query_scan_size     | bigint                      |
| query_priority      | integer                     |
| query_queue_time    | bigint                      |

## stv_recents

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| status        | character (20)              |
| starttime     | timestamp without time zone |
| duration      | bigint                      |
| user_name     | character (50)              |
| db_name       | character (50)              |
| query         | character (600)             |
| pid           | integer                     |

## stv_sessions

| column_name   | data_type                   |
|:--------------|:----------------------------|
| starttime     | timestamp without time zone |
| process       | integer                     |
| user_name     | character (50)              |
| db_name       | character (50)              |
| timeout_sec   | integer                     |

## stv_slices

| column_name   | data_type     |
|:--------------|:--------------|
| node          | integer       |
| slice         | integer       |
| localslice    | integer       |
| type          | character (1) |

## stv_startup_recovery_state

| column_name   | data_type       |
|:--------------|:----------------|
| db_id         | integer         |
| table_id      | integer         |
| table_name    | character (137) |

## stv_wlm_query_queue_state

| column_name   | data_type                   |
|:--------------|:----------------------------|
| service_class | integer                     |
| position      | integer                     |
| task          | integer                     |
| query         | integer                     |
| slot_count    | integer                     |
| start_time    | timestamp without time zone |
| queue_time    | bigint                      |

## stv_wlm_query_state

| column_name    | data_type                   |
|:---------------|:----------------------------|
| xid            | bigint                      |
| task           | integer                     |
| query          | integer                     |
| service_class  | integer                     |
| slot_count     | integer                     |
| wlm_start_time | timestamp without time zone |
| state          | character (16)              |
| queue_time     | bigint                      |
| exec_time      | bigint                      |
| query_priority | character (20)              |

## stv_wlm_query_task_state

| column_name   | data_type                   |
|:--------------|:----------------------------|
| service_class | integer                     |
| task          | integer                     |
| query         | integer                     |
| slot_count    | integer                     |
| start_time    | timestamp without time zone |
| exec_time     | bigint                      |

## svcs_alert_event_log

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| pid           | integer                     |
| xid           | bigint                      |
| event         | character (1024)            |
| solution      | character (200)             |
| event_time    | timestamp without time zone |

## svcs_compile

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| xid           | bigint                      |
| pid           | integer                     |
| query         | integer                     |
| segment       | integer                     |
| locus         | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| compile       | integer                     |

## svcs_explain

| column_name   | data_type       |
|:--------------|:----------------|
| userid        | integer         |
| query         | integer         |
| nodeid        | integer         |
| parentid      | integer         |
| plannode      | character (400) |
| info          | character (400) |

## svcs_plan_info

| column_name   | data_type        |
|:--------------|:-----------------|
| userid        | integer          |
| query         | integer          |
| nodeid        | integer          |
| segment       | integer          |
| step          | integer          |
| locus         | integer          |
| plannode      | integer          |
| startupcost   | double precision |
| totalcost     | double precision |
| rows          | bigint           |
| bytes         | bigint           |

## svcs_query_summary

| column_name     | data_type               |
|:----------------|:------------------------|
| userid          | integer                 |
| query           | integer                 |
| stm             | integer                 |
| seg             | integer                 |
| step            | integer                 |
| maxtime         | bigint                  |
| avgtime         | bigint                  |
| rows            | bigint                  |
| bytes           | bigint                  |
| rate_row        | double precision        |
| rate_byte       | double precision        |
| label           | character varying (164) |
| is_diskbased    | character (1)           |
| workmem         | bigint                  |
| is_rrscan       | character (1)           |
| is_delayed_scan | character (1)           |
| rows_pre_filter | bigint                  |

## svcs_s3list

| column_name      | data_type                   |
|:-----------------|:----------------------------|
| query            | integer                     |
| segment          | integer                     |
| node             | integer                     |
| eventtime        | timestamp without time zone |
| bucket           | character (256)             |
| prefix           | character (256)             |
| recursive        | character (1)               |
| retrieved_files  | integer                     |
| max_file_size    | bigint                      |
| avg_file_size    | double precision            |
| generated_splits | integer                     |
| avg_split_length | double precision            |
| duration         | bigint                      |

## svcs_s3log

| column_name   | data_type                   |
|:--------------|:----------------------------|
| pid           | integer                     |
| query         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| node          | integer                     |
| eventtime     | timestamp without time zone |
| message       | character (512)             |

## svcs_s3partition_summary

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| query                   | integer                     |
| segment                 | integer                     |
| assignment              | character (1)               |
| min_starttime           | timestamp without time zone |
| max_endtime             | timestamp without time zone |
| min_duration            | bigint                      |
| max_duration            | bigint                      |
| avg_duration            | bigint                      |
| total_partitions        | integer                     |
| qualified_partitions    | integer                     |
| min_assigned_partitions | integer                     |
| max_assigned_partitions | integer                     |
| avg_assigned_partitions | bigint                      |

## svcs_s3query_summary

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| userid                  | integer                     |
| query                   | integer                     |
| xid                     | bigint                      |
| pid                     | integer                     |
| segment                 | integer                     |
| step                    | integer                     |
| starttime               | timestamp without time zone |
| endtime                 | timestamp without time zone |
| elapsed                 | bigint                      |
| aborted                 | integer                     |
| external_table_name     | character varying (136)     |
| file_format             | character (16)              |
| is_partitioned          | character varying (1)       |
| is_rrscan               | character varying (1)       |
| is_nested               | character varying (1)       |
| s3_scanned_rows         | bigint                      |
| s3_scanned_bytes        | bigint                      |
| s3query_returned_rows   | bigint                      |
| s3query_returned_bytes  | bigint                      |
| files                   | bigint                      |
| files_max               | integer                     |
| files_avg               | bigint                      |
| splits                  | bigint                      |
| splits_max              | integer                     |
| splits_avg              | bigint                      |
| total_split_size        | bigint                      |
| max_split_size          | bigint                      |
| avg_split_size          | bigint                      |
| total_retries           | bigint                      |
| max_retries             | integer                     |
| max_request_duration    | bigint                      |
| avg_request_duration    | bigint                      |
| max_request_parallelism | integer                     |
| avg_request_parallelism | double precision            |
| total_slowdown_count    | bigint                      |
| max_slowdown_count      | integer                     |

## svcs_s3retries

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| query              | integer                     |
| segment            | integer                     |
| node               | integer                     |
| eventtime          | timestamp without time zone |
| retries            | integer                     |
| successful_fetches | integer                     |
| file_size          | bigint                      |
| location           | character (256)             |
| message            | character (256)             |

## svcs_stream_segs

| column_name   | data_type   |
|:--------------|:------------|
| userid        | integer     |
| query         | integer     |
| stream        | integer     |
| segment       | integer     |

## svcs_unload_log

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| pid           | integer                     |
| path          | character (1280)            |
| start_time    | timestamp without time zone |
| end_time      | timestamp without time zone |
| line_count    | bigint                      |
| transfer_size | bigint                      |
| file_format   | character (10)              |

## svl_auto_worker_action

| column_name    | data_type                   |
|:---------------|:----------------------------|
| table_id       | integer                     |
| type           | text                        |
| status         | character (128)             |
| eventtime      | timestamp without time zone |
| sequence       | integer                     |
| previous_state | character (200)             |

## svl_awsclient_error

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| query         | integer                     |
| slice         | integer                     |
| recordtime    | timestamp without time zone |
| pid           | integer                     |
| http_method   | character (64)              |
| endpoint      | character (128)             |
| error         | character (1024)            |

## svl_compile

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| xid           | bigint                      |
| pid           | integer                     |
| query         | integer                     |
| segment       | integer                     |
| locus         | integer                     |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| compile       | integer                     |

## svl_datashare_change_log

| column_name            | data_type                   |
|:-----------------------|:----------------------------|
| userid                 | integer                     |
| username               | character varying (128)     |
| pid                    | integer                     |
| xid                    | bigint                      |
| share_id               | integer                     |
| share_name             | character varying (128)     |
| source_database_id     | integer                     |
| source_database_name   | character varying (128)     |
| consumer_database_id   | integer                     |
| consumer_database_name | character varying (128)     |
| recordtime             | timestamp without time zone |
| action                 | character varying (128)     |
| status                 | integer                     |
| share_object_type      | character varying (64)      |
| share_object_id        | integer                     |
| share_object_name      | character varying (128)     |
| target_user_type       | character varying (16)      |
| target_userid          | integer                     |
| target_username        | character varying (128)     |
| consumer_account       | character varying (16)      |
| consumer_namespace     | character varying (64)      |
| producer_account       | character varying (16)      |
| producer_namespace     | character varying (64)      |
| attribute_name         | character varying (64)      |
| attribute_value        | character varying (128)     |
| message                | character varying (512)     |

## svl_datashare_usage_consumer

| column_name     | data_type                   |
|:----------------|:----------------------------|
| userid          | integer                     |
| pid             | integer                     |
| xid             | bigint                      |
| request_id      | character varying (50)      |
| request_type    | character varying (25)      |
| transaction_uid | character varying (50)      |
| recordtime      | timestamp without time zone |
| status          | integer                     |
| error           | character varying (512)     |

## svl_datashare_usage_producer

| column_name              | data_type                   |
|:-------------------------|:----------------------------|
| share_id                 | integer                     |
| share_name               | character varying (128)     |
| request_id               | character varying (50)      |
| request_type             | character varying (25)      |
| object_type              | character varying (64)      |
| object_oid               | integer                     |
| object_name              | character varying (128)     |
| consumer_account         | character varying (16)      |
| consumer_namespace       | character varying (64)      |
| consumer_transaction_uid | character varying (50)      |
| recordtime               | timestamp without time zone |
| status                   | integer                     |
| error                    | character varying (512)     |

## svl_federated_query

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| xid           | bigint                      |
| pid           | integer                     |
| query         | integer                     |
| sourcetype    | character (30)              |
| recordtime    | timestamp without time zone |
| querytext     | character (4000)            |
| num_rows      | bigint                      |
| num_bytes     | bigint                      |
| duration      | bigint                      |

## svl_multi_statement_violations

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| database      | character (32)              |
| cmdname       | character (20)              |
| xid           | bigint                      |
| pid           | integer                     |
| label         | character (320)             |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| sequence      | integer                     |
| type          | character varying (10)      |
| text          | character varying (200)     |

## svl_mv_refresh_status

| column_name   | data_type                   |
|:--------------|:----------------------------|
| db_name       | name                        |
| userid        | bigint                      |
| schema_name   | name                        |
| mv_name       | name                        |
| xid           | bigint                      |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| status        | text                        |
| refresh_type  | character (32)              |

## svl_qlog

| column_name                    | data_type                   |
|:-------------------------------|:----------------------------|
| userid                         | integer                     |
| query                          | integer                     |
| xid                            | bigint                      |
| pid                            | integer                     |
| starttime                      | timestamp without time zone |
| endtime                        | timestamp without time zone |
| elapsed                        | bigint                      |
| aborted                        | integer                     |
| label                          | character (320)             |
| substring                      | text                        |
| source_query                   | integer                     |
| concurrency_scaling_status_txt | text                        |
| from_sp_call                   | integer                     |

## svl_query_metrics

| column_name                | data_type              |
|:---------------------------|:-----------------------|
| userid                     | integer                |
| query                      | integer                |
| service_class              | integer                |
| dimension                  | character varying (24) |
| segment                    | integer                |
| step                       | integer                |
| step_label                 | character varying (30) |
| query_cpu_time             | bigint                 |
| query_blocks_read          | bigint                 |
| query_execution_time       | bigint                 |
| query_cpu_usage_percent    | numeric                |
| query_temp_blocks_to_disk  | bigint                 |
| segment_execution_time     | bigint                 |
| cpu_skew                   | numeric                |
| io_skew                    | numeric                |
| scan_row_count             | bigint                 |
| join_row_count             | bigint                 |
| nested_loop_join_row_count | bigint                 |
| return_row_count           | bigint                 |
| spectrum_scan_row_count    | bigint                 |
| spectrum_scan_size_mb      | bigint                 |
| query_queue_time           | bigint                 |
| service_class_name         | character (64)         |

## svl_query_metrics_summary

| column_name                | data_type      |
|:---------------------------|:---------------|
| userid                     | integer        |
| query                      | integer        |
| service_class              | integer        |
| query_cpu_time             | bigint         |
| query_blocks_read          | bigint         |
| query_execution_time       | bigint         |
| query_cpu_usage_percent    | numeric        |
| query_temp_blocks_to_disk  | bigint         |
| segment_execution_time     | bigint         |
| cpu_skew                   | numeric        |
| io_skew                    | numeric        |
| scan_row_count             | bigint         |
| join_row_count             | bigint         |
| nested_loop_join_row_count | bigint         |
| return_row_count           | bigint         |
| spectrum_scan_row_count    | bigint         |
| spectrum_scan_size_mb      | bigint         |
| query_queue_time           | bigint         |
| service_class_name         | character (64) |

## svl_query_report

| column_name     | data_type                   |
|:----------------|:----------------------------|
| userid          | integer                     |
| query           | integer                     |
| slice           | integer                     |
| segment         | integer                     |
| step            | integer                     |
| start_time      | timestamp without time zone |
| end_time        | timestamp without time zone |
| elapsed_time    | bigint                      |
| rows            | bigint                      |
| bytes           | bigint                      |
| label           | character varying (164)     |
| is_diskbased    | character (1)               |
| workmem         | bigint                      |
| is_rrscan       | character (1)               |
| is_delayed_scan | character (1)               |
| rows_pre_filter | bigint                      |

## svl_query_summary

| column_name     | data_type               |
|:----------------|:------------------------|
| userid          | integer                 |
| query           | integer                 |
| stm             | integer                 |
| seg             | integer                 |
| step            | integer                 |
| maxtime         | bigint                  |
| avgtime         | bigint                  |
| rows            | bigint                  |
| bytes           | bigint                  |
| rate_row        | double precision        |
| rate_byte       | double precision        |
| label           | character varying (164) |
| is_diskbased    | character (1)           |
| workmem         | bigint                  |
| is_rrscan       | character (1)           |
| is_delayed_scan | character (1)           |
| rows_pre_filter | bigint                  |

## svl_s3catalog

| column_name   | data_type                   |
|:--------------|:----------------------------|
| query         | integer                     |
| segment       | integer                     |
| node          | integer                     |
| slice         | integer                     |
| eventtime     | timestamp without time zone |
| call          | integer                     |
| objects       | integer                     |
| duration      | bigint                      |

## svl_s3list

| column_name      | data_type                   |
|:-----------------|:----------------------------|
| query            | integer                     |
| segment          | integer                     |
| node             | integer                     |
| slice            | integer                     |
| eventtime        | timestamp without time zone |
| bucket           | text                        |
| prefix           | text                        |
| recursive        | character (1)               |
| retrieved_files  | integer                     |
| max_file_size    | bigint                      |
| avg_file_size    | double precision            |
| generated_splits | integer                     |
| avg_split_length | double precision            |
| duration         | bigint                      |

## svl_s3log

| column_name   | data_type                   |
|:--------------|:----------------------------|
| pid           | integer                     |
| query         | integer                     |
| segment       | integer                     |
| step          | integer                     |
| node          | integer                     |
| slice         | integer                     |
| eventtime     | timestamp without time zone |
| message       | text                        |

## svl_s3partition

| column_name          | data_type                   |
|:---------------------|:----------------------------|
| query                | integer                     |
| segment              | integer                     |
| node                 | integer                     |
| slice                | integer                     |
| starttime            | timestamp without time zone |
| endtime              | timestamp without time zone |
| duration             | bigint                      |
| total_partitions     | integer                     |
| qualified_partitions | integer                     |
| assigned_partitions  | integer                     |
| assignment           | character (1)               |

## svl_s3partition_summary

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| query                   | integer                     |
| segment                 | integer                     |
| assignment              | character (1)               |
| min_starttime           | timestamp without time zone |
| max_endtime             | timestamp without time zone |
| min_duration            | bigint                      |
| max_duration            | bigint                      |
| avg_duration            | bigint                      |
| total_partitions        | integer                     |
| qualified_partitions    | integer                     |
| min_assigned_partitions | integer                     |
| max_assigned_partitions | integer                     |
| avg_assigned_partitions | bigint                      |

## svl_s3query

| column_name                   | data_type                   |
|:------------------------------|:----------------------------|
| userid                        | integer                     |
| query                         | integer                     |
| segment                       | integer                     |
| step                          | integer                     |
| node                          | integer                     |
| slice                         | integer                     |
| starttime                     | timestamp without time zone |
| endtime                       | timestamp without time zone |
| elapsed                       | bigint                      |
| external_table_name           | text                        |
| file_format                   | character (16)              |
| is_partitioned                | character (1)               |
| is_rrscan                     | character (1)               |
| is_nested                     | character (1)               |
| s3_scanned_rows               | bigint                      |
| s3_scanned_bytes              | bigint                      |
| s3query_returned_rows         | bigint                      |
| s3query_returned_bytes        | bigint                      |
| files                         | integer                     |
| splits                        | integer                     |
| total_split_size              | bigint                      |
| max_split_size                | bigint                      |
| total_retries                 | integer                     |
| max_retries                   | integer                     |
| max_request_duration          | bigint                      |
| avg_request_duration          | bigint                      |
| max_request_parallelism       | integer                     |
| avg_request_parallelism       | double precision            |
| slowdown_count                | integer                     |
| max_concurrent_slowdown_count | integer                     |

## svl_s3query_summary

| column_name             | data_type                   |
|:------------------------|:----------------------------|
| userid                  | integer                     |
| query                   | integer                     |
| xid                     | bigint                      |
| pid                     | integer                     |
| segment                 | integer                     |
| step                    | integer                     |
| starttime               | timestamp without time zone |
| endtime                 | timestamp without time zone |
| elapsed                 | bigint                      |
| aborted                 | integer                     |
| external_table_name     | text                        |
| file_format             | character (16)              |
| is_partitioned          | text                        |
| is_rrscan               | text                        |
| is_nested               | text                        |
| s3_scanned_rows         | bigint                      |
| s3_scanned_bytes        | bigint                      |
| s3query_returned_rows   | bigint                      |
| s3query_returned_bytes  | bigint                      |
| files                   | bigint                      |
| files_max               | integer                     |
| files_avg               | bigint                      |
| splits                  | bigint                      |
| splits_max              | integer                     |
| splits_avg              | bigint                      |
| total_split_size        | bigint                      |
| max_split_size          | bigint                      |
| avg_split_size          | bigint                      |
| total_retries           | bigint                      |
| max_retries             | integer                     |
| max_request_duration    | bigint                      |
| avg_request_duration    | bigint                      |
| max_request_parallelism | integer                     |
| avg_request_parallelism | double precision            |
| total_slowdown_count    | bigint                      |
| max_slowdown_count      | integer                     |

## svl_s3retries

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| query              | integer                     |
| segment            | integer                     |
| node               | integer                     |
| slice              | integer                     |
| eventtime          | timestamp without time zone |
| retries            | integer                     |
| successful_fetches | integer                     |
| file_size          | bigint                      |
| location           | text                        |
| message            | text                        |

## svl_spatial_simplify

| column_name       | data_type        |
|:------------------|:-----------------|
| query             | bigint           |
| line_number       | bigint           |
| maximum_tolerance | double precision |
| initial_size      | bigint           |
| simplified        | character (1)    |
| final_size        | bigint           |
| final_tolerance   | double precision |

## svl_spectrum_scan_error

| column_name    | data_type        |
|:---------------|:-----------------|
| userid         | integer          |
| query          | integer          |
| location       | character (256)  |
| rowid          | character (2100) |
| colname        | character (127)  |
| original_value | character (1024) |
| modified_value | character (1024) |
| trigger        | text             |
| action         | text             |
| action_value   | character (127)  |
| error_code     | integer          |

## svl_statementtext

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| xid           | bigint                      |
| pid           | integer                     |
| label         | character (320)             |
| starttime     | timestamp without time zone |
| endtime       | timestamp without time zone |
| sequence      | integer                     |
| type          | character varying (10)      |
| text          | character varying (200)     |

## svl_stored_proc_call

| column_name    | data_type                   |
|:---------------|:----------------------------|
| userid         | integer                     |
| session_userid | integer                     |
| query          | integer                     |
| label          | character (320)             |
| xid            | bigint                      |
| pid            | integer                     |
| database       | character (32)              |
| querytxt       | character (500)             |
| starttime      | timestamp without time zone |
| endtime        | timestamp without time zone |
| aborted        | integer                     |
| from_sp_call   | integer                     |

## svl_stored_proc_messages

| column_name    | data_type                   |
|:---------------|:----------------------------|
| userid         | integer                     |
| session_userid | integer                     |
| pid            | integer                     |
| xid            | bigint                      |
| query          | integer                     |
| recordtime     | timestamp without time zone |
| loglevel       | integer                     |
| loglevel_text  | text                        |
| message        | character (1024)            |
| linenum        | integer                     |
| querytxt       | character (500)             |
| label          | character (320)             |
| aborted        | integer                     |

## svl_udf_log

| column_name   | data_type                   |
|:--------------|:----------------------------|
| query         | bigint                      |
| message       | character (4096)            |
| created       | timestamp without time zone |
| traceback     | character (4096)            |
| funcname      | character (256)             |
| node          | integer                     |
| slice         | integer                     |
| seq           | integer                     |

## svv_all_columns

| column_name              | data_type                |
|:-------------------------|:-------------------------|
| database_name            | character varying (128)  |
| schema_name              | character varying (128)  |
| table_name               | character varying (128)  |
| column_name              | character varying (128)  |
| ordinal_position         | integer                  |
| column_default           | character varying (4000) |
| is_nullable              | character varying (3)    |
| data_type                | character varying (128)  |
| character_maximum_length | integer                  |
| numeric_precision        | integer                  |
| numeric_scale            | integer                  |
| remarks                  | character varying (256)  |

## svv_all_schemas

| column_name     | data_type               |
|:----------------|:------------------------|
| database_name   | character varying (128) |
| schema_name     | character varying (128) |
| schema_owner    | integer                 |
| schema_type     | character varying (128) |
| schema_acl      | character varying (128) |
| source_database | character varying (128) |
| schema_option   | character varying (128) |

## svv_all_tables

| column_name   | data_type               |
|:--------------|:------------------------|
| database_name | character varying (128) |
| schema_name   | character varying (128) |
| table_name    | character varying (128) |
| table_type    | character varying (128) |
| table_acl     | character varying (128) |
| remarks       | character varying (128) |

## svv_columns

| column_name              | data_type                |
|:-------------------------|:-------------------------|
| table_catalog            | character varying (128)  |
| table_schema             | character varying (128)  |
| table_name               | character varying (128)  |
| column_name              | character varying (128)  |
| ordinal_position         | integer                  |
| column_default           | character varying (4000) |
| is_nullable              | character varying (3)    |
| data_type                | character varying (128)  |
| character_maximum_length | integer                  |
| numeric_precision        | integer                  |
| numeric_precision_radix  | integer                  |
| numeric_scale            | integer                  |
| datetime_precision       | integer                  |
| interval_type            | character varying        |
| interval_precision       | character varying        |
| character_set_catalog    | character varying (128)  |
| character_set_schema     | character varying (128)  |
| character_set_name       | character varying (128)  |
| collation_catalog        | character varying (128)  |
| collation_schema         | character varying (128)  |
| collation_name           | character varying (128)  |
| domain_name              | character varying (128)  |
| remarks                  | character varying        |

## svv_datashare_consumers

| column_name        | data_type                   |
|:-------------------|:----------------------------|
| share_name         | character varying (128)     |
| consumer_account   | character varying (16)      |
| consumer_namespace | character varying (64)      |
| share_date         | timestamp without time zone |

## svv_datashare_objects

| column_name        | data_type               |
|:-------------------|:------------------------|
| share_type         | character varying (8)   |
| share_name         | character varying (128) |
| object_type        | character varying (64)  |
| object_name        | character varying (512) |
| producer_account   | character varying (16)  |
| producer_namespace | character varying (64)  |
| include_new        | boolean                 |

## svv_datashares

| column_name         | data_type                   |
|:--------------------|:----------------------------|
| share_name          | character varying (128)     |
| share_id            | integer                     |
| share_owner         | integer                     |
| source_database     | character varying (128)     |
| consumer_database   | character varying (128)     |
| share_type          | character varying (8)       |
| createdate          | timestamp without time zone |
| is_publicaccessible | boolean                     |
| share_acl           | character varying (256)     |
| producer_account    | character (16)              |
| producer_namespace  | character (64)              |
| managed_by          | character varying (16)      |

## svv_external_columns

| column_name   | data_type               |
|:--------------|:------------------------|
| schemaname    | character varying (128) |
| tablename     | character varying (128) |
| columnname    | character varying (128) |
| external_type | character varying (128) |
| columnnum     | integer                 |
| part_key      | integer                 |
| is_nullable   | character varying (128) |

## svv_external_databases

| column_name   | data_type   |
|:--------------|:------------|
| eskind        | integer     |
| esoptions     | text        |
| databasename  | text        |
| location      | text        |
| parameters    | text        |

## svv_external_partitions

| column_name       | data_type   |
|:------------------|:------------|
| schemaname        | text        |
| tablename         | text        |
| values            | text        |
| location          | text        |
| input_format      | text        |
| output_format     | text        |
| serialization_lib | text        |
| serde_parameters  | text        |
| compressed        | integer     |
| parameters        | text        |

## svv_external_schemas

| column_name   | data_type   |
|:--------------|:------------|
| esoid         | oid         |
| eskind        | smallint    |
| schemaname    | name        |
| esowner       | integer     |
| databasename  | text        |
| esoptions     | text        |

## svv_external_tables

| column_name       | data_type               |
|:------------------|:------------------------|
| schemaname        | character varying (128) |
| tablename         | character varying (128) |
| location          | character varying (128) |
| input_format      | character varying (128) |
| output_format     | character varying (128) |
| serialization_lib | character varying (128) |
| serde_parameters  | character varying (128) |
| compressed        | integer                 |
| parameters        | character varying (128) |
| tabletype         | character varying (128) |

## svv_query_inflight

| column_name   | data_type                   |
|:--------------|:----------------------------|
| userid        | integer                     |
| slice         | integer                     |
| query         | integer                     |
| pid           | integer                     |
| starttime     | timestamp without time zone |
| suspended     | integer                     |
| text          | character (200)             |
| sequence      | integer                     |

## svv_query_state

| column_name     | data_type        |
|:----------------|:-----------------|
| userid          | integer          |
| query           | integer          |
| seg             | integer          |
| step            | integer          |
| maxtime         | bigint           |
| avgtime         | bigint           |
| rows            | bigint           |
| bytes           | bigint           |
| cpu             | bigint           |
| memory          | bigint           |
| rate_row        | double precision |
| rate_byte       | double precision |
| label           | character (256)  |
| is_diskbased    | character (1)    |
| workmem         | bigint           |
| num_parts       | integer          |
| is_rrscan       | character (1)    |
| is_delayed_scan | character (1)    |

## svv_redshift_columns

| column_name      | data_type                |
|:-----------------|:-------------------------|
| database_name    | character varying (128)  |
| schema_name      | character varying (128)  |
| table_name       | character varying (128)  |
| column_name      | character varying (128)  |
| ordinal_position | integer                  |
| data_type        | character varying (128)  |
| column_default   | character varying (4000) |
| is_nullable      | character varying (3)    |
| encoding         | character varying (128)  |
| distkey          | boolean                  |
| sortkey          | integer                  |
| column_acl       | character varying (128)  |
| remarks          | character varying (256)  |

## svv_redshift_databases

| column_name      | data_type               |
|:-----------------|:------------------------|
| database_name    | character varying (128) |
| database_owner   | integer                 |
| database_type    | character varying (16)  |
| database_acl     | character varying (128) |
| database_options | character varying (128) |

## svv_redshift_functions

| column_name   | data_type               |
|:--------------|:------------------------|
| database_name | character varying (128) |
| schema_name   | character varying (128) |
| function_name | character varying (128) |
| function_type | character varying (128) |
| argument_type | character varying (512) |
| result_type   | character varying (128) |

## svv_redshift_schemas

| column_name   | data_type               |
|:--------------|:------------------------|
| database_name | character varying (128) |
| schema_name   | character varying (128) |
| schema_owner  | integer                 |
| schema_type   | character varying (128) |
| schema_acl    | character varying (128) |
| schema_option | character varying (128) |

## svv_redshift_tables

| column_name   | data_type               |
|:--------------|:------------------------|
| database_name | character varying (128) |
| schema_name   | character varying (128) |
| table_name    | character varying (128) |
| table_type    | character varying (128) |
| table_acl     | character varying (128) |
| remarks       | character varying (128) |

## svv_schema_quota_state

| column_name    | data_type        |
|:---------------|:-----------------|
| schema_id      | integer          |
| schema_name    | name             |
| schema_owner   | integer          |
| quota          | integer          |
| disk_usage     | integer          |
| disk_usage_pct | double precision |

## svv_tables

| column_name   | data_type               |
|:--------------|:------------------------|
| table_catalog | character varying (128) |
| table_schema  | character varying (128) |
| table_name    | character varying (128) |
| table_type    | character varying (15)  |
| remarks       | character varying       |

## svv_transactions

| column_name          | data_type                   |
|:---------------------|:----------------------------|
| txn_owner            | text                        |
| txn_db               | text                        |
| xid                  | bigint                      |
| pid                  | integer                     |
| txn_start            | timestamp without time zone |
| lock_mode            | character varying (24)      |
| lockable_object_type | character varying (14)      |
| relation             | integer                     |
| granted              | boolean                     |

# pg_temp_106



## #tableau_1075_2_tuples

| column_name                      | data_type              |
|:---------------------------------|:-----------------------|
| x_calculation_914723362099720207 | character varying (12) |
| x_city                           | character varying (20) |

## #tableau_1075_3_tuples

| column_name                      | data_type              |
|:---------------------------------|:-----------------------|
| x_calculation_610448913469771794 | character varying (12) |
| x_city                           | character varying (20) |

# pg_temp_88



## #tableau_3123_4_tuples

| column_name   | data_type                   |
|:--------------|:----------------------------|
| x_tdy:date:ok | timestamp without time zone |
| x_city        | character varying (20)      |

## #tableau_3123_5_tuples

| column_name   | data_type                   |
|:--------------|:----------------------------|
| x_tdy:date:ok | timestamp without time zone |
| x_city        | character varying (20)      |

## #tableau_3123_6_tuples

| column_name   | data_type                   |
|:--------------|:----------------------------|
| x_date        | timestamp without time zone |
| x_city        | character varying (20)      |

## #tableau_3123_7_tuples

| column_name                      | data_type                   |
|:---------------------------------|:----------------------------|
| x_date                           | timestamp without time zone |
| x_yr:date:ok                     | integer                     |
| x_calculation_610448913469771794 | character varying (12)      |
| x_city                           | character varying (20)      |

## #tableau_3123_8_tuples

| column_name                            | data_type                   |
|:---------------------------------------|:----------------------------|
| x_date                                 | timestamp without time zone |
| x_yr:calculation_487444347904204872:ok | integer                     |
| x_calculation_610448913469771794       | character varying (12)      |
| x_city                                 | character varying (20)      |

## #tableau_3123_9_tuples

| column_name                      | data_type              |
|:---------------------------------|:-----------------------|
| x_yr:date:ok                     | integer                |
| x_calculation_610448913469771794 | character varying (12) |
| x_city                           | character varying (20) |

# public



## awsdms_apply_exceptions

| column_name   | data_type                   |
|:--------------|:----------------------------|
| task_name     | character varying (384)     |
| table_owner   | character varying (384)     |
| table_name    | character varying (384)     |
| error_time    | timestamp without time zone |
| statement     | character varying (32768)   |
| error         | character varying (32768)   |

## brisbane

| column_name         | data_type                   |
|:--------------------|:----------------------------|
| visibility          | bigint                      |
| dt                  | timestamp without time zone |
| city_id             | bigint                      |
| city_name           | character varying (256)     |
| temp                | double precision            |
| temp_feels_like     | double precision            |
| temp_min            | double precision            |
| temp_max            | double precision            |
| pressure            | bigint                      |
| humidity            | bigint                      |
| wind_speed          | double precision            |
| wind_dir_deg        | bigint                      |
| clouds_all          | bigint                      |
| city_country        | character varying (256)     |
| city_sunrise        | timestamp without time zone |
| city_sunset         | timestamp without time zone |
| rain_1h             | character varying (256)     |
| rain_3h             | character varying (256)     |
| snow_1h             | character varying (256)     |
| snow_3h             | character varying (256)     |
| weather_id          | bigint                      |
| weather_name        | character varying (256)     |
| weather_description | character varying (256)     |

## fleet_mapping

| column_name   | data_type                |
|:--------------|:-------------------------|
| id            | bigint                   |
| date_time     | timestamp with time zone |
| counry        | character varying (20)   |
| city          | character varying (20)   |
| vehicle_id    | character varying (20)   |
| vehicle_type  | character varying (20)   |
| operator      | character varying (20)   |
| lat           | double precision         |
| lng           | double precision         |
| battery       | integer                  |

## loadview

| column_name   | data_type                   |
|:--------------|:----------------------------|
| tbl           | integer                     |
| table_name    | text                        |
| query         | integer                     |
| starttime     | timestamp without time zone |
| input         | text                        |
| line_number   | bigint                      |
| colname       | character (127)             |
| err_code      | integer                     |
| reason        | text                        |

## neuron_user.user_city_prediction

| column_name   | data_type                   |
|:--------------|:----------------------------|
| user_id       | bigint                      |
| role_code     | character varying (256)     |
| is_vip        | bigint                      |
| country       | character varying (256)     |
| city          | character varying (256)     |
| status        | character varying (256)     |
| updated_at    | timestamp without time zone |
| created_at    | timestamp without time zone |

## neuron_weather.brisbane

| column_name         | data_type                   |
|:--------------------|:----------------------------|
| visibility          | bigint                      |
| dt                  | timestamp without time zone |
| city_id             | bigint                      |
| city_name           | character varying (256)     |
| temp                | double precision            |
| temp_feels_like     | double precision            |
| temp_min            | double precision            |
| temp_max            | double precision            |
| pressure            | bigint                      |
| humidity            | bigint                      |
| wind_speed          | double precision            |
| wind_dir_deg        | bigint                      |
| clouds_all          | bigint                      |
| city_country        | character varying (256)     |
| city_sunrise        | timestamp without time zone |
| city_sunset         | timestamp without time zone |
| rain_1h             | character varying (256)     |
| rain_3h             | character varying (256)     |
| snow_1h             | character varying (256)     |
| snow_3h             | character varying (256)     |
| weather_id          | bigint                      |
| weather_name        | character varying (256)     |
| weather_description | character varying (256)     |

## scooter_history_adelaide

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_ansan

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_anyang

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_auckland

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_brisbane

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_busan

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_canberra

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_daegu

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_gimhae

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_goyang

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_gwangju

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_hobart

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_incheon

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_jeonju

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_launceston

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_logan

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_namyangju

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_nowon

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_pohang

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_port_douglas

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_pyeongtaek

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_santa_monica

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_seoul

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_suwon

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_sydney

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_townsville

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_washington_dc_gbfs

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## scooter_history_yongin

| column_name       | data_type                   |
|:------------------|:----------------------------|
| deck_id           | character varying (50)      |
| vehicle_type      | character varying (25)      |
| remaining_battery | double precision            |
| latitude          | double precision            |
| longitude         | double precision            |
| updated_at        | timestamp without time zone |
| last_scan         | timestamp without time zone |
| operator          | character varying (20)      |
| city              | character varying (20)      |
| code              | character varying (15)      |
| scooter_id        | character varying (64)      |

## test_file

| column_name   | data_type               |
|:--------------|:------------------------|
| id            | character varying (256) |
| h             | character varying (256) |

## user_city_pred

| column_name   | data_type                   |
|:--------------|:----------------------------|
| id            | bigint                      |
| user_id       | bigint                      |
| role_code     | character varying (256)     |
| is_vip        | bigint                      |
| country       | character varying (256)     |
| city          | character varying (256)     |
| status        | character varying (256)     |
| updated_at    | character varying (256)     |
| created_at    | timestamp without time zone |

## user_city_prediction

| column_name   | data_type                   |
|:--------------|:----------------------------|
| city          | character varying (256)     |
| created_at    | timestamp without time zone |
| is_vip        | integer                     |
| role_code     | character varying (256)     |
| status        | character varying (256)     |
| updated_at    | character varying (256)     |
| user_id       | integer                     |