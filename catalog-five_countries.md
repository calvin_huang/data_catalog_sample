[TOC]

# Schema Comparison Table

|                                  | neuron_australia   | neuron_canada   | neuron_gb   | neuron_korea   | neuron_newzealand   |
|:---------------------------------|:-------------------|:----------------|:------------|:---------------|:--------------------|
| adl_cbd_status_count             | 1                  |                 |             |                |                     |
| adl_wa_status_count              | 1                  |                 |             |                |                     |
| akl_status_count                 |                    |                 |             |                | 1                   |
| battery_history                  | 1                  | 1               | 1           | 1              | 1                   |
| bne_status_count                 | 1                  |                 |             |                |                     |
| city_status_count                | 1                  | 1               | 1           | 1              | 1                   |
| command_log_history              | 1                  |                 |             |                | 1                   |
| command_response_history         | 1                  |                 |             |                | 1                   |
| dft_user_post_ride_surveys       |                    |                 | 1           |                |                     |
| dft_user_trips                   |                    |                 | 1           |                |                     |
| dft_users                        |                    |                 | 1           |                |                     |
| dft_vehicle_status_logs          |                    |                 | 1           |                |                     |
| dft_vehicle_trips                |                    |                 | 1           |                |                     |
| dft_vehicles                     |                    |                 | 1           |                |                     |
| drw_status_count                 | 1                  |                 |             |                |                     |
| feedback_phrases                 | 1                  |                 |             |                |                     |
| geofence_wkb                     | 1                  | 1               | 1           | 1              | 1                   |
| gps_history                      | 1                  | 1               | 1           | 1              | 1                   |
| gross_revenue_history            | 1                  | 1               | 1           | 1              | 1                   |
| incident_report                  | 1                  |                 |             |                | 1                   |
| n3_status_history                | 1                  |                 |             |                | 1                   |
| rtk_station_history              |                    | 1               |             |                |                     |
| scooter_history                  | 1                  | 1               | 1           | 1              | 1                   |
| staging_battery_history          | 1                  | 1               | 1           | 1              | 1                   |
| staging_command_log_history      | 1                  |                 |             |                | 1                   |
| staging_command_response_history | 1                  |                 |             |                | 1                   |
| staging_gps_history              | 1                  | 1               | 1           | 1              | 1                   |
| staging_n3_status_history        | 1                  |                 |             |                | 1                   |
| staging_scooter_history          | 1                  | 1               | 1           | 1              | 1                   |
| status_count                     | 1                  |                 |             |                |                     |
| trip_detail                      | 1                  | 1               | 1           |                | 1                   |
| trip_start_end_locations         | 1                  |                 |             |                | 1                   |
| trip_start_end_wkb               | 1                  | 1               | 1           | 1              | 1                   |
| trip_status                      | 1                  |                 | 1           |                | 1                   |
| trip_waypoints                   | 1                  |                 | 1           |                | 1                   |
| trip_waypoints_detailed          | 1                  | 1               | 1           | 1              | 1                   |



## adl_cbd_status_count

| column_name            | data_type   | related_keys   |
|:-----------------------|:------------|:---------------|
| date                   | date        |                |
| hour                   | bigint      |                |
| minute                 | bigint      |                |
| in_trip                | bigint      |                |
| in_station_parking     | bigint      |                |
| in_station_out_station | bigint      |                |
| in_station_warehouse   | bigint      |                |
| in_station_pickup      | bigint      |                |
| in_station_staging     | bigint      |                |
| in_stock               | bigint      |                |
| missing                | bigint      |                |
| rebalancing            | bigint      |                |

## adl_wa_status_count

| column_name            | data_type   | related_keys   |
|:-----------------------|:------------|:---------------|
| date                   | date        |                |
| hour                   | bigint      |                |
| minute                 | bigint      |                |
| in_trip                | bigint      |                |
| in_station_parking     | bigint      |                |
| in_station_out_station | bigint      |                |
| in_station_warehouse   | bigint      |                |
| in_station_pickup      | bigint      |                |
| in_station_staging     | bigint      |                |
| in_stock               | bigint      |                |
| missing                | bigint      |                |
| rebalancing            | bigint      |                |

## battery_history

| column_name        | data_type                   | related_keys   |
|:-------------------|:----------------------------|:---------------|
| id                 | bigint                      |                |
| scooter_id         | bigint                      |                |
| created_at         | timestamp without time zone |                |
| updated_at         | timestamp without time zone |                |
| battery_number     | character varying (256)     |                |
| remaining_capacity | smallint                    |                |
| full_capacity      | smallint                    |                |
| work_cycle         | smallint                    |                |
| electricity        | double precision            |                |
| city               | character varying (256)     |                |
| status             | character varying (30)      |                |

## bne_status_count

| column_name            | data_type   | related_keys   |
|:-----------------------|:------------|:---------------|
| date                   | date        |                |
| hour                   | bigint      |                |
| minute                 | bigint      |                |
| in_trip                | bigint      |                |
| in_station_parking     | bigint      |                |
| in_station_out_station | bigint      |                |
| in_station_warehouse   | bigint      |                |
| in_station_pickup      | bigint      |                |
| in_station_staging     | bigint      |                |
| in_stock               | bigint      |                |
| missing                | bigint      |                |
| rebalancing            | bigint      |                |

## city_status_count

| column_name                   | data_type                   | related_keys   |
|:------------------------------|:----------------------------|:---------------|
| city                          | character varying (32)      |                |
| created_at                    | timestamp without time zone |                |
| in_station_parking            | bigint                      |                |
| in_station_out_station        | bigint                      |                |
| in_station_pickup             | bigint                      |                |
| in_station_staging            | bigint                      |                |
| in_reservation                | bigint                      |                |
| no_riding                     | bigint                      |                |
| outside_service               | bigint                      |                |
| admin_lock                    | bigint                      |                |
| in_trip                       | bigint                      |                |
| in_stock                      | bigint                      |                |
| in_repair                     | bigint                      |                |
| in_warehouse                  | bigint                      |                |
| missing                       | bigint                      |                |
| beyond_repair                 | bigint                      |                |
| rebalancing                   | bigint                      |                |
| rebalancing_and_available     | bigint                      |                |
| in_trip_and_available         | bigint                      |                |
| no_riding_and_available       | bigint                      |                |
| outside_service_and_available | bigint                      |                |
| admin_lock_and_available      | bigint                      |                |
| in_reservation_and_available  | bigint                      |                |
| in_station_and_available      | bigint                      |                |
| in_station_ops_only           | bigint                      |                |
| vehicle_type                  | character varying (30)      |                |
| zone                          | character varying (32)      |                |

## command_log_history

| column_name   | data_type                   | related_keys   |
|:--------------|:----------------------------|:---------------|
| id            | bigint                      |                |
| command       | character varying (1024)    |                |
| succeed       | smallint                    |                |
| device_id     | bigint                      |                |
| created_at    | timestamp without time zone |                |

## command_response_history

| column_name   | data_type                   | related_keys   |
|:--------------|:----------------------------|:---------------|
| id            | bigint                      |                |
| message       | character varying (255)     |                |
| device_id     | bigint                      |                |
| created_at    | timestamp without time zone |                |

## drw_status_count

| column_name            | data_type   | related_keys   |
|:-----------------------|:------------|:---------------|
| date                   | date        |                |
| hour                   | bigint      |                |
| minute                 | bigint      |                |
| in_trip                | bigint      |                |
| in_station_parking     | bigint      |                |
| in_station_out_station | bigint      |                |
| in_station_warehouse   | bigint      |                |
| in_station_pickup      | bigint      |                |
| in_station_staging     | bigint      |                |
| in_stock               | bigint      |                |
| missing                | bigint      |                |
| rebalancing            | bigint      |                |

## feedback_phrases

| column_name   | data_type               | related_keys   |
|:--------------|:------------------------|:---------------|
| phrases       | character varying (256) |                |
| count         | bigint                  |                |
| rating        | character varying (256) |                |

## geofence_wkb

| column_name    | data_type                   | related_keys   |
|:---------------|:----------------------------|:---------------|
| fence_id       | bigint                      |                |
| name           | character varying (150)     |                |
| geometry       | geometry                    |                |
| city           | character varying (20)      |                |
| max_speed      | real                        |                |
| status         | character varying (20)      |                |
| hidden         | smallint                    |                |
| start_time     | character varying (10)      |                |
| end_time       | character varying (10)      |                |
| weekdays       | character varying (32)      |                |
| timed_geofence | character varying (128)     |                |
| updated_at     | timestamp without time zone |                |
| created_at     | timestamp without time zone |                |
| type           | character varying (64)      |                |
| vehicle_type   | character varying (15)      |                |

## gps_history

| column_name          | data_type                   | related_keys   |
|:---------------------|:----------------------------|:---------------|
| imei                 | character varying (50)      |                |
| updated_at           | timestamp without time zone |                |
| helmet_attached      | smallint                    |                |
| dashboard_version    | character varying (256)     |                |
| motor_version        | character varying (256)     |                |
| bluetooth_version    | character varying (256)     |                |
| bt_mac               | character varying (32)      |                |
| is_connected         | smallint                    |                |
| topple               | smallint                    |                |
| remaining_range      | numeric                     |                |
| blt_password         | character varying (128)     |                |
| remaining_battery    | double precision            |                |
| latitude             | double precision            |                |
| longitude            | double precision            |                |
| engine_off           | smallint                    |                |
| gps_external_power   | smallint                    |                |
| id                   | bigint                      |                |
| created_at           | timestamp without time zone |                |
| alarm_on             | smallint                    |                |
| gps_battery_level    | smallint                    |                |
| status               | character varying (30)      |                |
| location_source      | character varying (50)      |                |
| position_active_time | timestamp without time zone |                |
| voltage              | double precision            |                |
| iot_version          | character varying (50)      |                |
| dbu_hw               | character varying (10)      |                |
| protocol             | character varying (50)      |                |
| local_geofence       | smallint                    |                |
| version_list         | super                       |                |
| white_noise          | smallint                    |                |
| mode                 | smallint                    |                |
| local_geo_status     | character varying (256)     |                |
| hdop                 | double precision            |                |
| vin                  | double precision            |                |
| ride_mile            | bigint                      |                |

## gross_revenue_history

| column_name             | data_type                | related_keys   |
|:------------------------|:-------------------------|:---------------|
| order_id                | bigint                   |                |
| order_updated_utc       | timestamp with time zone |                |
| pass_amount             | double precision         |                |
| gross_rev               | double precision         |                |
| cap_off_fee             | double precision         |                |
| waived_trip_fare        | double precision         |                |
| engine_off_fee          | double precision         |                |
| transfer_ride_unlocking | double precision         |                |
| pass_unlock_fee         | double precision         |                |
| coupon                  | double precision         |                |
| adjust_off              | double precision         |                |
| vip_fee                 | double precision         |                |
| short_trip_fee          | double precision         |                |
| free_morning_fee        | double precision         |                |
| payable_amount          | double precision         |                |
| created_at              | timestamp with time zone |                |
| city                    | character varying (10)   |                |
| city_base_fee           | double precision         |                |
| city_unit_fee           | double precision         |                |
| base_fee                | double precision         |                |
| unit_fee                | double precision         |                |
| is_vip                  | smallint                 |                |
| is_short_trip           | smallint                 |                |
| transfer_ride           | smallint                 |                |
| pass_id                 | bigint                   |                |
| total_minutes           | bigint                   |                |
| engine_off_minutes      | bigint                   |                |
| vehicle_type            | character varying (15)   |                |
| order_status            | character varying (15)   |                |
| order_created_utc       | timestamp with time zone |                |
| currency                | character varying (10)   |                |
| deposit_captured        | double precision         |                |
| load_time               | timestamp with time zone |                |
| unit_fee_discount       | double precision         |                |
| base_fee_discount       | double precision         |                |
| partial_pass_unlock_fee | double precision         |                |
| helmet_replacement_fee  | double precision         |                |
| convenience_fee         | double precision         |                |
| gross_trip_fare         | double precision         |                |

## incident_report

| column_name             | data_type                 | related_keys                                                                                                                                                    |
|:------------------------|:--------------------------|:----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| incident_id             | bigint                    |                                                                                                                                                                 |
| city                    | character varying (8)     |                                                                                                                                                                 |
| business_risk_level     | character varying (16)    |                                                                                                                                                                 |
| risk_level              | character varying (16)    |                                                                                                                                                                 |
| status                  | character varying (32)    |                                                                                                                                                                 |
| type                    | character varying (32)    |                                                                                                                                                                 |
| incident_time           | timestamp with time zone  |                                                                                                                                                                 |
| aware_time              | timestamp with time zone  |                                                                                                                                                                 |
| police                  | boolean                   |                                                                                                                                                                 |
| ticket_url              | character varying (256)   |                                                                                                                                                                 |
| short_description       | character varying (65535) |                                                                                                                                                                 |
| incident_description    | character varying (65535) |                                                                                                                                                                 |
| remarks                 | character varying (65535) |                                                                                                                                                                 |
| user_involved           | character varying (32)    |                                                                                                                                                                 |
| third_party_involved    | character varying (32)    |                                                                                                                                                                 |
| location                | character varying (65535) |                                                                                                                                                                 |
| created_user            | character varying (16)    |                                                                                                                                                                 |
| created_user_role       | character varying (32)    |                                                                                                                                                                 |
| updated_user            | character varying (16)    |                                                                                                                                                                 |
| updated_user_role       | character varying (32)    |                                                                                                                                                                 |
| platform                | character varying (16)    |                                                                                                                                                                 |
| version                 | double precision          |                                                                                                                                                                 |
| created_at              | timestamp with time zone  |                                                                                                                                                                 |
| updated_at              | timestamp with time zone  |                                                                                                                                                                 |
| updated_by              | bigint                    |                                                                                                                                                                 |
| created_by              | bigint                    |                                                                                                                                                                 |
| latitude                | double precision          |                                                                                                                                                                 |
| longitude               | double precision          |                                                                                                                                                                 |
| category                | character varying (16)    |                                                                                                                                                                 |
| reason                  | character varying (65535) |                                                                                                                                                                 |
| source                  | character varying (32)    |                                                                                                                                                                 |
| edm_sent                | boolean                   |                                                                                                                                                                 |
| user_id                 | bigint                    | trip_detail.user_id                                                                                                                                             |
| injury                  | character varying (16)    |                                                                                                                                                                 |
| injury_severity         | character varying (16)    |                                                                                                                                                                 |
| medical                 | character varying (16)    |                                                                                                                                                                 |
| email                   | character varying (64)    |                                                                                                                                                                 |
| paramedic               | character varying (16)    |                                                                                                                                                                 |
| user_description        | character varying (65535) |                                                                                                                                                                 |
| trip_id                 | bigint                    | trip_waypoints.trip_id, trip_waypoints_detailed.trip_id, trip_start_end_wkb.trip_id, trip_status.trip_id, trip_start_end_locations.trip_id, trip_detail.trip_id |
| start_time              | timestamp with time zone  |                                                                                                                                                                 |
| end_time                | timestamp with time zone  |                                                                                                                                                                 |
| trip_status             | character varying (16)    |                                                                                                                                                                 |
| start_station_id        | integer                   |                                                                                                                                                                 |
| start_station_name      | character varying (32)    |                                                                                                                                                                 |
| end_station_id          | integer                   |                                                                                                                                                                 |
| end_station_name        | character varying (32)    |                                                                                                                                                                 |
| start_zone_code         | character varying (16)    |                                                                                                                                                                 |
| start_zone_name         | character varying (32)    |                                                                                                                                                                 |
| end_zone_code           | character varying (16)    |                                                                                                                                                                 |
| end_zone_name           | character varying (32)    |                                                                                                                                                                 |
| deck_id                 | character varying (32)    |                                                                                                                                                                 |
| generation              | character varying (8)     |                                                                                                                                                                 |
| device_id               | bigint                    |                                                                                                                                                                 |
| imei                    | character varying (32)    |                                                                                                                                                                 |
| qr_code                 | character varying (16)    |                                                                                                                                                                 |
| sticker_id              | character varying (16)    |                                                                                                                                                                 |
| party                   | character varying (16)    |                                                                                                                                                                 |
| third_party_category    | character varying (16)    |                                                                                                                                                                 |
| third_party_description | character varying (65535) |                                                                                                                                                                 |

## n3_status_history

| column_name             | data_type                   | related_keys   |
|:------------------------|:----------------------------|:---------------|
| id                      | bigint                      |                |
| electric_hall           | smallint                    |                |
| a_phase_current         | smallint                    |                |
| b_phase_current         | smallint                    |                |
| c_phase_current         | smallint                    |                |
| dc_current              | smallint                    |                |
| accelerator             | smallint                    |                |
| dashboard_communication | smallint                    |                |
| bms_communication       | smallint                    |                |
| brake                   | smallint                    |                |
| iot_communication       | smallint                    |                |
| ble_communication       | smallint                    |                |
| lowe_battery            | smallint                    |                |
| low_battery_protection  | smallint                    |                |
| topple                  | smallint                    |                |
| lock_status             | character varying (32)      |                |
| battery_percentage      | double precision            |                |
| device_id               | bigint                      |                |
| created_at              | timestamp without time zone |                |
| single_range            | double precision            |                |
| speed                   | double precision            |                |
| voltage                 | double precision            |                |
| electricity             | double precision            |                |
| helmet_attached         | smallint                    |                |

## scooter_history

| column_name        | data_type                   | related_keys   |
|:-------------------|:----------------------------|:---------------|
| id                 | bigint                      |                |
| imei               | character varying (50)      |                |
| station_id         | double precision            |                |
| qr_code            | character varying (50)      |                |
| deck_id            | character varying (50)      |                |
| mac                | character varying (70)      |                |
| status             | character varying (30)      |                |
| city               | character varying (20)      |                |
| created_at         | timestamp without time zone |                |
| updated_at         | timestamp without time zone |                |
| deleted            | smallint                    |                |
| last_sanitize_time | timestamp without time zone |                |
| zone               | character varying (20)      |                |
| type               | character varying (32)      |                |
| generation         | character varying (16)      |                |
| sticker            | character varying (15)      |                |

## staging_battery_history

| column_name        | data_type                   | related_keys   |
|:-------------------|:----------------------------|:---------------|
| op                 | character varying (10)      |                |
| id                 | bigint                      |                |
| scooter_id         | bigint                      |                |
| created_at         | timestamp without time zone |                |
| updated_at         | timestamp without time zone |                |
| battery_number     | character varying (256)     |                |
| remaining_capacity | smallint                    |                |
| full_capacity      | smallint                    |                |
| work_cycle         | smallint                    |                |
| electricity        | double precision            |                |
| city               | character varying (256)     |                |
| status             | character varying (30)      |                |

## staging_command_log_history

| column_name   | data_type                   | related_keys   |
|:--------------|:----------------------------|:---------------|
| id            | bigint                      |                |
| command       | character varying (1024)    |                |
| succeed       | smallint                    |                |
| device_id     | bigint                      |                |
| created_at    | timestamp without time zone |                |

## staging_command_response_history

| column_name   | data_type                   | related_keys   |
|:--------------|:----------------------------|:---------------|
| id            | bigint                      |                |
| message       | character varying (255)     |                |
| device_id     | bigint                      |                |
| created_at    | timestamp without time zone |                |

## staging_gps_history

| column_name          | data_type                   | related_keys   |
|:---------------------|:----------------------------|:---------------|
| op                   | character varying (10)      |                |
| id                   | bigint                      |                |
| imei                 | character varying (50)      |                |
| created_at           | timestamp without time zone |                |
| updated_at           | timestamp without time zone |                |
| remaining_battery    | double precision            |                |
| latitude             | double precision            |                |
| longitude            | double precision            |                |
| engine_off           | smallint                    |                |
| gps_battery_level    | smallint                    |                |
| gps_external_power   | smallint                    |                |
| status               | character varying (30)      |                |
| alarm_on             | smallint                    |                |
| version              | character varying (256)     |                |
| voltage              | double precision            |                |
| apn                  | character varying (256)     |                |
| iccid                | character varying (256)     |                |
| location_source      | character varying (50)      |                |
| position_active_time | timestamp without time zone |                |
| protocol             | character varying (50)      |                |
| normal_speed         | smallint                    |                |
| sport_speed          | smallint                    |                |
| dashboard_version    | character varying (256)     |                |
| motor_version        | character varying (256)     |                |
| bluetooth_version    | character varying (256)     |                |
| helmet_attached      | smallint                    |                |
| no_parking_light     | smallint                    |                |
| wrench_light         | smallint                    |                |
| bt_mac               | character varying (32)      |                |
| is_connected         | smallint                    |                |
| topple               | smallint                    |                |
| remaining_range      | numeric                     |                |
| blt_password         | character varying (128)     |                |
| iot_version          | character varying (50)      |                |
| dbu_hw               | character varying (10)      |                |
| local_geofence       | smallint                    |                |
| version_list         | super                       |                |
| white_noise          | smallint                    |                |
| mode                 | smallint                    |                |
| local_geo_status     | character varying (256)     |                |
| hdop                 | double precision            |                |
| vin                  | double precision            |                |
| ride_mile            | bigint                      |                |

## staging_n3_status_history

| column_name             | data_type                   | related_keys   |
|:------------------------|:----------------------------|:---------------|
| id                      | bigint                      |                |
| electric_hall           | smallint                    |                |
| a_phase_current         | smallint                    |                |
| b_phase_current         | smallint                    |                |
| c_phase_current         | smallint                    |                |
| dc_current              | smallint                    |                |
| accelerator             | smallint                    |                |
| dashboard_communication | smallint                    |                |
| bms_communication       | smallint                    |                |
| brake                   | smallint                    |                |
| iot_communication       | smallint                    |                |
| ble_communication       | smallint                    |                |
| lowe_battery            | smallint                    |                |
| low_battery_protection  | smallint                    |                |
| topple                  | smallint                    |                |
| lock_status             | character varying (32)      |                |
| battery_percentage      | double precision            |                |
| device_id               | bigint                      |                |
| created_at              | timestamp without time zone |                |
| single_range            | double precision            |                |
| speed                   | double precision            |                |
| voltage                 | double precision            |                |
| electricity             | double precision            |                |
| helmet_attached         | smallint                    |                |

## staging_scooter_history

| column_name        | data_type                   | related_keys   |
|:-------------------|:----------------------------|:---------------|
| op                 | character varying (10)      |                |
| id                 | bigint                      |                |
| imei               | character varying (50)      |                |
| station_id         | double precision            |                |
| qr_code            | character varying (50)      |                |
| deck_id            | character varying (50)      |                |
| mac                | character varying (70)      |                |
| ic_card            | double precision            |                |
| status             | character varying (30)      |                |
| city               | character varying (20)      |                |
| created_at         | timestamp without time zone |                |
| updated_at         | timestamp without time zone |                |
| version            | integer                     |                |
| deleted            | smallint                    |                |
| image_id           | character varying (256)     |                |
| generation         | character varying (16)      |                |
| zone               | character varying (256)     |                |
| licence            | character varying (256)     |                |
| type               | character varying (256)     |                |
| last_test_time     | timestamp without time zone |                |
| helmet_lock        | smallint                    |                |
| last_sanitize_time | timestamp without time zone |                |
| sticker            | character varying (15)      |                |

## status_count

| column_name            | data_type   | related_keys   |
|:-----------------------|:------------|:---------------|
| date                   | date        |                |
| hour                   | bigint      |                |
| minute                 | bigint      |                |
| in_trip                | bigint      |                |
| in_station_parking     | bigint      |                |
| in_station_out_station | bigint      |                |
| in_station_warehouse   | bigint      |                |
| in_station_pickup      | bigint      |                |
| in_station_staging     | bigint      |                |
| in_stock               | bigint      |                |
| missing                | bigint      |                |
| rebalancing            | bigint      |                |

## trip_detail

| column_name         | data_type                   | related_keys                                                                                                                                                        |
|:--------------------|:----------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| start_time          | timestamp without time zone |                                                                                                                                                                     |
| trip_id             | bigint                      | trip_waypoints.trip_id, trip_waypoints_detailed.trip_id, trip_start_end_wkb.trip_id, trip_status.trip_id, trip_start_end_locations.trip_id, incident_report.trip_id |
| end_time            | timestamp without time zone |                                                                                                                                                                     |
| user_id             | bigint                      | incident_report.user_id                                                                                                                                             |
| city                | character varying (20)      |                                                                                                                                                                     |
| device_id           | bigint                      |                                                                                                                                                                     |
| trip_status         | character varying (16)      |                                                                                                                                                                     |
| total_minutes       | integer                     |                                                                                                                                                                     |
| total_mileage       | double precision            |                                                                                                                                                                     |
| start_station_id    | bigint                      |                                                                                                                                                                     |
| returned_station_id | bigint                      |                                                                                                                                                                     |
| force_end           | smallint                    |                                                                                                                                                                     |
| bt_unlock           | smallint                    |                                                                                                                                                                     |
| auto_end            | smallint                    |                                                                                                                                                                     |
| temp_lock           | smallint                    |                                                                                                                                                                     |
| is_short_trip       | smallint                    |                                                                                                                                                                     |
| is_vip              | smallint                    |                                                                                                                                                                     |
| zone                | character varying (20)      |                                                                                                                                                                     |
| group_id            | character varying (64)      |                                                                                                                                                                     |
| engine_off_minutes  | bigint                      |                                                                                                                                                                     |
| user_name           | character varying (100)     |                                                                                                                                                                     |
| email               | character varying (100)     |                                                                                                                                                                     |
| gps_id              | bigint                      |                                                                                                                                                                     |
| transfer_ride       | smallint                    |                                                                                                                                                                     |
| helmet_returned     | smallint                    |                                                                                                                                                                     |
| app_version         | character varying (20)      |                                                                                                                                                                     |
| start_latitude      | double precision            |                                                                                                                                                                     |
| start_longitude     | double precision            |                                                                                                                                                                     |
| end_latitude        | double precision            |                                                                                                                                                                     |
| end_longitude       | double precision            |                                                                                                                                                                     |
| start_no_riding     | smallint                    |                                                                                                                                                                     |
| end_no_riding       | smallint                    |                                                                                                                                                                     |
| imei                | character varying (32)      |                                                                                                                                                                     |
| battery_no          | character varying (64)      |                                                                                                                                                                     |
| vehicle_type        | character varying (32)      |                                                                                                                                                                     |
| start_zone          | character varying (20)      |                                                                                                                                                                     |
| end_zone            | character varying (20)      |                                                                                                                                                                     |
| campaign_id         | bigint                      |                                                                                                                                                                     |
| order_amount        | real                        |                                                                                                                                                                     |
| coupon_amount       | real                        |                                                                                                                                                                     |
| trip_type           | character varying (100)     |                                                                                                                                                                     |
| protocol            | character varying (20)      |                                                                                                                                                                     |
| helmet_selfie       | smallint                    |                                                                                                                                                                     |
| selfie_checked      | smallint                    |                                                                                                                                                                     |
| parking_photo       | smallint                    |                                                                                                                                                                     |
| rating              | integer                     |                                                                                                                                                                     |
| is_avas             | smallint                    |                                                                                                                                                                     |
| is_rtk              | smallint                    |                                                                                                                                                                     |
| local_geofence      | smallint                    |                                                                                                                                                                     |
| topple              | smallint                    |                                                                                                                                                                     |
| swerving            | smallint                    |                                                                                                                                                                     |
| helmet_unlock       | smallint                    |                                                                                                                                                                     |
| helmet_available    | smallint                    |                                                                                                                                                                     |

## trip_start_end_locations

| column_name   | data_type               | related_keys                                                                                                                                           |
|:--------------|:------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------|
| device_id     | integer                 |                                                                                                                                                        |
| el            | character varying (256) |                                                                                                                                                        |
| end_lat       | double precision        |                                                                                                                                                        |
| end_lon       | double precision        |                                                                                                                                                        |
| end_time      | character varying (256) |                                                                                                                                                        |
| sl            | character varying (256) |                                                                                                                                                        |
| start_lat     | double precision        |                                                                                                                                                        |
| start_lon     | double precision        |                                                                                                                                                        |
| start_time    | character varying (256) |                                                                                                                                                        |
| city          | character varying (256) |                                                                                                                                                        |
| trip_id       | integer                 | trip_waypoints.trip_id, trip_waypoints_detailed.trip_id, trip_start_end_wkb.trip_id, trip_status.trip_id, trip_detail.trip_id, incident_report.trip_id |

## trip_start_end_wkb

| column_name   | data_type                   | related_keys                                                                                                                                                 |
|:--------------|:----------------------------|:-------------------------------------------------------------------------------------------------------------------------------------------------------------|
| start_time    | timestamp without time zone |                                                                                                                                                              |
| trip_id       | integer                     | trip_waypoints.trip_id, trip_waypoints_detailed.trip_id, trip_status.trip_id, trip_start_end_locations.trip_id, trip_detail.trip_id, incident_report.trip_id |
| scooter_id    | integer                     |                                                                                                                                                              |
| gps_id        | integer                     |                                                                                                                                                              |
| start_lat     | double precision            |                                                                                                                                                              |
| start_lon     | double precision            |                                                                                                                                                              |
| start_point   | geometry                    |                                                                                                                                                              |
| end_time      | timestamp without time zone |                                                                                                                                                              |
| end_lat       | double precision            |                                                                                                                                                              |
| end_lon       | double precision            |                                                                                                                                                              |
| end_point     | geometry                    |                                                                                                                                                              |
| city          | character varying (256)     |                                                                                                                                                              |
| vehicle_type  | character varying (32)      |                                                                                                                                                              |
| imei          | character varying (32)      |                                                                                                                                                              |

## trip_status

| column_name        | data_type                   | related_keys                                                                                                                                                        |
|:-------------------|:----------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| created_at         | timestamp without time zone |                                                                                                                                                                     |
| trip_id            | bigint                      | trip_waypoints.trip_id, trip_waypoints_detailed.trip_id, trip_start_end_wkb.trip_id, trip_start_end_locations.trip_id, trip_detail.trip_id, incident_report.trip_id |
| city               | character varying (20)      |                                                                                                                                                                     |
| voltage            | double precision            |                                                                                                                                                                     |
| speed              | double precision            |                                                                                                                                                                     |
| electricity        | double precision            |                                                                                                                                                                     |
| battery_percentage | double precision            |                                                                                                                                                                     |
| imei               | character varying (32)      |                                                                                                                                                                     |
| protocol           | character varying (10)      |                                                                                                                                                                     |

## trip_waypoints

| column_name   | data_type                   | related_keys                                                                                                                                                     |
|:--------------|:----------------------------|:-----------------------------------------------------------------------------------------------------------------------------------------------------------------|
| created_at    | timestamp without time zone |                                                                                                                                                                  |
| trip_id       | bigint                      | trip_waypoints_detailed.trip_id, trip_start_end_wkb.trip_id, trip_status.trip_id, trip_start_end_locations.trip_id, trip_detail.trip_id, incident_report.trip_id |
| city          | character varying (20)      |                                                                                                                                                                  |
| device_id     | bigint                      |                                                                                                                                                                  |
| latitude      | double precision            |                                                                                                                                                                  |
| longitude     | double precision            |                                                                                                                                                                  |
| imei          | character varying (32)      |                                                                                                                                                                  |

## trip_waypoints_detailed

| column_name    | data_type                   | related_keys                                                                                                                                            |
|:---------------|:----------------------------|:--------------------------------------------------------------------------------------------------------------------------------------------------------|
| created_at     | timestamp without time zone |                                                                                                                                                         |
| trip_id        | bigint                      | trip_waypoints.trip_id, trip_start_end_wkb.trip_id, trip_status.trip_id, trip_start_end_locations.trip_id, trip_detail.trip_id, incident_report.trip_id |
| city           | character varying (20)      |                                                                                                                                                         |
| device_id      | bigint                      |                                                                                                                                                         |
| latitude       | double precision            |                                                                                                                                                         |
| longitude      | double precision            |                                                                                                                                                         |
| position_speed | double precision            |                                                                                                                                                         |
| imei           | character varying (32)      |                                                                                                                                                         |